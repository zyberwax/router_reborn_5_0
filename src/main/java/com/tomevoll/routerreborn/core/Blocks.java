package com.tomevoll.routerreborn.core;

import com.tomevoll.routerreborn.core.blocks.BlockAirLight;
import com.tomevoll.routerreborn.core.blocks.BlockLamp;
import com.tomevoll.routerreborn.core.blocks.BlockRouter;
import com.tomevoll.routerreborn.core.tileentity.TileEntityRouter;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Blocks {

    public  BlockRouter ROUTER;
    public  BlockAirLight LIGHT_BLOCK;
    public  BlockLamp LAMP_BLOCK;


    public void Init() {
        ROUTER = (BlockRouter) new BlockRouter(Material.GROUND).setHardness(1.0f).setUnlocalizedName("router").setRegistryName(RouterReborn.MODID, "router").setCreativeTab(RouterReborn.TAB);
        LIGHT_BLOCK = (BlockAirLight) new BlockAirLight().setHardness(1.0f).setUnlocalizedName("router_air").setRegistryName(RouterReborn.MODID, "router_air").setCreativeTab(RouterReborn.TAB);
        LAMP_BLOCK = (BlockLamp) new BlockLamp(Material.GROUND).setHardness(1.0f).setUnlocalizedName("router_lampblock").setRegistryName(RouterReborn.MODID, "router_lampblock").setCreativeTab(RouterReborn.TAB);
    }

    @SideOnly(Side.CLIENT)
    public void registerRender() {

        ROUTER.RegisterRenderer();
        LAMP_BLOCK.RegisterRenderer("router_lampblock");
    }

    public void Register() {
        RegisterBlocks();
        RegisterTiles();
        RegisterItemBlock();

        //ForgeRegistries.RECIPES.register(value);
    }

    private void RegisterBlocks() {
        ForgeRegistries.BLOCKS.register(ROUTER);
        ForgeRegistries.BLOCKS.register(LIGHT_BLOCK);
        ForgeRegistries.BLOCKS.register(LAMP_BLOCK);
    }

    private void RegisterTiles() {
        GameRegistry.registerTileEntity(TileEntityRouter.class, "rr_routerTile");

    }

    private void RegisterItemBlock() {
        ForgeRegistries.ITEMS.register(new ItemBlock(ROUTER).setUnlocalizedName(ROUTER.getUnlocalizedName()).setRegistryName(ROUTER.getRegistryName()).setCreativeTab(ROUTER.getCreativeTabToDisplayOn()));
        ForgeRegistries.ITEMS.register(new ItemBlock(LAMP_BLOCK).setUnlocalizedName(LAMP_BLOCK.getUnlocalizedName()).setRegistryName(LAMP_BLOCK.getRegistryName()).setCreativeTab(LAMP_BLOCK.getCreativeTabToDisplayOn()));
    }

}

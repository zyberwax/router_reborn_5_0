package com.tomevoll.routerreborn.core.tools;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.tomevoll.routerreborn.core.RouterReborn;
import com.tomevoll.routerreborn.lib.util.I18n;
import com.tomevoll.routerreborn.lib.util.Raytrace;
import net.minecraft.block.Block;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.network.play.server.SPacketBlockChange;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@SuppressWarnings({"deprecation"})
public class ItemPickaxe extends ItemTool {

    private static final Set<Block> EFFECTIVE_ON = Sets.newHashSet(Blocks.ACTIVATOR_RAIL, Blocks.COAL_ORE, Blocks.COBBLESTONE, Blocks.DETECTOR_RAIL, Blocks.DIAMOND_BLOCK, Blocks.DIAMOND_ORE, Blocks.DOUBLE_STONE_SLAB, Blocks.GOLDEN_RAIL, Blocks.GOLD_BLOCK, Blocks.GOLD_ORE, Blocks.ICE, Blocks.IRON_BLOCK, Blocks.IRON_ORE, Blocks.LAPIS_BLOCK, Blocks.LAPIS_ORE, Blocks.LIT_REDSTONE_ORE, Blocks.MOSSY_COBBLESTONE, Blocks.NETHERRACK, Blocks.PACKED_ICE, Blocks.RAIL, Blocks.REDSTONE_ORE, Blocks.SANDSTONE, Blocks.RED_SANDSTONE, Blocks.STONE, Blocks.STONE_SLAB, Blocks.STONE_BUTTON, Blocks.STONE_PRESSURE_PLATE,
            Blocks.CLAY, Blocks.DIRT, Blocks.FARMLAND, Blocks.GRASS, Blocks.GRAVEL, Blocks.MYCELIUM, Blocks.SAND, Blocks.SNOW, Blocks.SNOW_LAYER, Blocks.SOUL_SAND, Blocks.GRASS_PATH, Blocks.CONCRETE_POWDER,
            Blocks.PLANKS, Blocks.BOOKSHELF, Blocks.LOG, Blocks.LOG2, Blocks.CHEST, Blocks.PUMPKIN, Blocks.LIT_PUMPKIN, Blocks.MELON_BLOCK, Blocks.LADDER, Blocks.WOODEN_BUTTON, Blocks.WOODEN_PRESSURE_PLATE, Blocks.HARDENED_CLAY, Blocks.STAINED_HARDENED_CLAY);
    private int PICKAXE_MODE = 0;


    public ItemPickaxe() {
        super(1.0F, -2.8F, ToolMaterial.DIAMOND, EFFECTIVE_ON);
    }

    public static float breakExtraBlock(World world, BlockPos pos, int sidehit, EntityPlayer playerEntity, int refX, int refY, int refZ) {

        if (world.isAirBlock(pos))
            return 0;

        if (!(playerEntity instanceof EntityPlayerMP))
            return 0;

        EntityPlayerMP player = (EntityPlayerMP) playerEntity;
        IBlockState block = world.getBlockState(pos);

        int meta = block.getBlock().getMetaFromState(block);

        ItemStack itemstack = player.getHeldItemMainhand();

        if (!itemstack.canHarvestBlock(block))
            return 0;

        IBlockState refBlock = world.getBlockState(new BlockPos(refX, refY, refZ));
        float refStrength = ForgeHooks.blockStrength(refBlock, player, world, new BlockPos(refX, refY, refZ));
        float strength = ForgeHooks.blockStrength(block, player, world, pos);

        if (!ForgeHooks.canHarvestBlock(block.getBlock(), player, world, pos) || refStrength / strength > 10f)
            return 0;

        int event = ForgeHooks.onBlockBreakEvent(world, player.getEntityWorld().getMinecraftServer().getGameType(), player, pos);
        if (event == -1)
            return 0;

        if (player.capabilities.isCreativeMode) {
            block.getBlock().onBlockHarvested(world, pos, block, player);
            if (block.getBlock().removedByPlayer(block, world, pos, player, false))
                block.getBlock().onBlockDestroyedByPlayer(world, pos, block);

            // send update to client
            if (!world.isRemote) {
                player.connection.sendPacket(new SPacketBlockChange(world, pos));
            }
            return 0;
        }

        // server sided handling
        if (!world.isRemote) {
            block.getBlock().onBlockHarvested(world, pos, block, player);

            if (block.getBlock().removedByPlayer(block, world, pos, player, true)) // boolean
            {
                block.getBlock().onBlockDestroyedByPlayer(world, pos, block);

                block.getBlock().harvestBlock(world, player, pos, block, world.getTileEntity(pos), player.getHeldItem(player.getActiveHand()));
                int amount = block.getBlock().getExpDrop(block, world, pos, 0);
                if (amount > 0)
                    block.getBlock().dropXpOnBlockBreak(world, pos, amount);
            }
            player.connection.sendPacket(new SPacketBlockChange(world, pos));
        }
        // client sided handling
        else {

            world.playEvent(2001, pos, Block.getIdFromBlock(block.getBlock()) + (0 << 12));
            if (block.getBlock().removedByPlayer(block, world, pos, player, true)) {
                block.getBlock().onBlockDestroyedByPlayer(world, pos, block);
            }

            Minecraft.getMinecraft().getConnection().sendPacket(new CPacketPlayerDigging(CPacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, Minecraft.getMinecraft().objectMouseOver.sideHit));
        }
        return strength;
    }

    @Override
    public boolean canHarvestBlock(IBlockState state, ItemStack stack) {

        String tool = state.getBlock().getHarvestTool(state);
        if (tool == null) return true;
        for (String t : getToolClasses(stack)) {
            if (t.equalsIgnoreCase(tool))
                return true;
        }

        return false;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {

        if (!player.isSneaking() && hand == EnumHand.MAIN_HAND) {
            Block block = player.getEntityWorld().getBlockState(pos).getBlock();
            TileEntity tile = player.getEntityWorld().getTileEntity(pos);
            if (!hasInteraction(player, worldIn, pos, hand, facing, hitX, hitY, hitZ)) {

                if (player.inventory.hasItemStack(new ItemStack(Blocks.TORCH, 1))) {

                    if (placeTorch(new ItemStack(Blocks.TORCH, 1), player, worldIn, pos, facing, hitX, hitY, hitZ)) {
                        player.inventory.clearMatchingItems(new ItemStack(Blocks.TORCH, 1).getItem(), -1, 1, null);
                        return worldIn.isRemote ? super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ) : EnumActionResult.SUCCESS;
                    }
                }
            } else
                return EnumActionResult.SUCCESS; //has interaction so skip placing torches and allow vanilla handling
        }


        return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
    }

    public boolean placeTorch(ItemStack item, EntityPlayer player, World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ) {


        IBlockState state = world.getBlockState(pos);
        if (Blocks.TORCH.canPlaceBlockAt(world, pos.offset(facing)) && state.getBlock().canPlaceBlockOnSide(world, pos.offset(facing), facing)) {
            boolean placed = false;
            placed = world.setBlockState(pos.offset(facing), Block.getBlockFromName("TORCH").getBlockState().getBaseState().withProperty(BlockTorch.FACING, facing), 3);

            if (placed) {
                // Play sound on torch placed
                SoundType soundType = Block.getBlockFromName("TORCH").getBlockState().getBlock().getSoundType(Block.getBlockFromName("TORCH").getBlockState().getBaseState(), world, pos, player);
                world.playSound(null, pos, soundType.getPlaceSound(), SoundCategory.BLOCKS, 1.0f, 0.8F);
                return true;
            }
        }
        return false;
    }

    public boolean hasInteraction(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        double reachDist = player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue();
        net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock event = net.minecraftforge.common.ForgeHooks
                .onRightClickBlock(player, hand, pos, facing, net.minecraftforge.common.ForgeHooks.rayTraceEyeHitVec(player, reachDist + 1));
        if (event.isCanceled()) return false;


        if (!player.isSneaking() || event.getUseBlock() == net.minecraftforge.fml.common.eventhandler.Event.Result.ALLOW) {
            IBlockState iblockstate = worldIn.getBlockState(pos);
            if (event.getUseBlock() != net.minecraftforge.fml.common.eventhandler.Event.Result.DENY)
                if (iblockstate.getBlock().onBlockActivated(worldIn, pos, iblockstate, player, hand, facing, hitX, hitY, hitZ)) {
                    return true;
                }
        }
        return false;
    }

    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
        NBTTagCompound tag = itemstack.getTagCompound();
        PICKAXE_MODE = (tag != null && tag.hasKey("rr_pickaxemode")) ? tag.getInteger("rr_pickaxemode") : 0;

        if (tag == null) {
            tag = new NBTTagCompound();
            tag.setInteger("rr_pickaxemode", PICKAXE_MODE);
            itemstack.setTagCompound(tag);
        }

        //dont do AOE when sneaking
        if (player.isSneaking()) {
            return super.onBlockStartBreak(itemstack, pos, player);
        }

        //get side of block hit to know what axis to do AOE on
        Raytrace.RayTraceResult mop = Raytrace.collisionRayTrace(player.getEntityWorld(), pos, player, new AxisAlignedBB(0, 0, 0, 1, 1, 1), 0, null);
        if (mop == null)
            return super.onBlockStartBreak(itemstack, pos, player);

        EnumFacing sideHit = mop.hit.sideHit;

        int BREAK_RADIUS = PICKAXE_MODE;
        int BREAK_DEPTH = 0;

        int xRange = BREAK_RADIUS;
        int yRange = BREAK_RADIUS;

        int zRange = BREAK_DEPTH;
        switch (sideHit) {
            case DOWN:
            case UP:
                yRange = BREAK_DEPTH;
                zRange = BREAK_RADIUS;
                break;
            case NORTH:
            case SOUTH:
                xRange = BREAK_RADIUS;
                zRange = BREAK_DEPTH;
                break;
            case WEST:
            case EAST:
                xRange = BREAK_DEPTH;
                zRange = BREAK_RADIUS;
                break;
        }

        int x = pos.getX();
        int y = pos.getY();
        int z = pos.getZ();

        for (int xPos = x - xRange; xPos <= x + xRange; xPos++)
            for (int yPos = y - yRange; yPos <= y + yRange; yPos++)
                for (int zPos = z - zRange; zPos <= z + zRange; zPos++) {
                    // don't break the originally already broken block
                    if (xPos == x && yPos == y && zPos == z)
                        continue;

                    if (!super.onBlockStartBreak(itemstack, new BlockPos(xPos, yPos, zPos), player)) {

                        breakExtraBlock(player.getEntityWorld(), new BlockPos(xPos, yPos, zPos), sideHit.getIndex(), player, x, y, z);
                    }
                }
        return super.onBlockStartBreak(itemstack, pos, player);
    }

    //todo: add damage for all blocks broken
    @Override
    public boolean onBlockDestroyed(ItemStack stack, World worldIn, IBlockState state, BlockPos pos, EntityLivingBase entityLiving) {
        if (!worldIn.isRemote && (double) state.getBlockHardness(worldIn, pos) != 0.0D) {
            stack.damageItem(1, entityLiving);
        }

        return true;
    }

    @Override
    public int getHarvestLevel(ItemStack stack, String toolClass, @Nullable EntityPlayer player, @Nullable IBlockState blockState) {

        int level = super.getHarvestLevel(stack, toolClass, player, blockState);
        if (level == -1) {
            for (String t : getToolClasses(stack)) {
                if (t.equalsIgnoreCase(toolClass))
                    return this.toolMaterial.getHarvestLevel();
            }
        } else {
            return level;
        }


        return super.getHarvestLevel(stack, toolClass, player, blockState);
    }

    @Override
    public Set<String> getToolClasses(ItemStack stack) {
        return ImmutableSet.of("pickaxe", "axe", "shovel");
    }


    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        if (!world.isRemote)
            if (player.isSneaking()) {
                ItemStack itemstack = player.getHeldItem(hand);
                NBTTagCompound tag = itemstack.getTagCompound();
                PICKAXE_MODE = (tag != null && tag.hasKey("rr_pickaxemode")) ? tag.getInteger("rr_pickaxemode") : 1;

                if (tag == null) {
                    tag = new NBTTagCompound();
                    tag.setInteger("rr_pickaxemode", PICKAXE_MODE);
                    itemstack.setTagCompound(tag);
                }

                PICKAXE_MODE++;
                if (PICKAXE_MODE > 2)
                    PICKAXE_MODE = 0;
                String message = "";
                switch (PICKAXE_MODE) {
                    case 0: // Single BlockMode
                        message = "\u00a77Mode: \u00A7asingle block";

                        break;
                    case 1: // 3x3 Mode
                        message = "\u00a77Mode: \u00A7a3x3";

                        break;
                    case 2: // 5x5 Mode
                        message = "\u00a77Mode: \u00A7a5x5";

                        break;
                    default:
                        break;
                }

                player.sendStatusMessage(new TextComponentTranslation(message), true);

                tag.setInteger("rr_pickaxemode", PICKAXE_MODE);
                itemstack.setTagCompound(tag);
                player.setHeldItem(hand, itemstack);

                return ActionResult.newResult(EnumActionResult.SUCCESS, itemstack);
            }

        return super.onItemRightClick(world, player, hand);
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return false;
    }
    @SuppressWarnings({"deprecation"})
    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {

        try {

            tooltip.set(0, "\u00A73" + I18n.translateToLocal(RouterReborn.MODID + ":" +  this.getUnlocalizedName() + ".name") + "\u00A7r");

            NBTTagCompound tag = stack.getTagCompound();
            if (tag != null)
                if (tag.hasKey("pickaxemode")) {
                    this.PICKAXE_MODE = tag.getInteger("rr_pickaxemode");
                }
            tooltip.add("");
            switch (PICKAXE_MODE) {
                case 0: // Single BlockMode
                    tooltip.add("\u00a77Mode: \u00A7asingle block");
                    break;
                case 1: // 3x3 Mode
                    tooltip.add("\u00a77Mode: \u00A7a3x3");
                    break;
                case 2: // 5x5 Mode
                    tooltip.add("\u00a77Mode: \u00A7a5x5");
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot) {
        Multimap<String, AttributeModifier> multimap = super.getItemAttributeModifiers(equipmentSlot);

        if (equipmentSlot == EntityEquipmentSlot.MAINHAND) {
            multimap.put(SharedMonsterAttributes.MOVEMENT_SPEED.getName(), new AttributeModifier(UUID.fromString("CB3F55D3-645C-4F38-A497-9C13A33DB5CF"), "Tool modifier", (double) 0.04, 0));
        }

        return multimap;
    }

}

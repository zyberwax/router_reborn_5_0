package com.tomevoll.routerreborn.core.proxy;

import com.tomevoll.routerreborn.core.Items;
import com.tomevoll.routerreborn.core.RouterReborn;

public class ClientProxy extends CommonProxy {

    @Override
    public void RegisterRenders() {


        RouterReborn.BLOCKS.registerRender();
        Items.registerRender();
    }

    @Override
    public void RegisterEvents() {
        // TODO Auto-generated method stub

    }

    @Override
    public void RegisterTileEntitySpecialRenderer() {
        // TODO Auto-generated method stub

    }

}

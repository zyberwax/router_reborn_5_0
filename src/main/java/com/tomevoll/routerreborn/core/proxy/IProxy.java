package com.tomevoll.routerreborn.core.proxy;

public interface IProxy {

    void RegisterRenders();

    void RegisterEvents();

    void RegisterTileEntitySpecialRenderer();

}

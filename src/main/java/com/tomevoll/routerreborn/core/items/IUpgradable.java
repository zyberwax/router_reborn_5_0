package com.tomevoll.routerreborn.core.items;

import net.minecraft.item.Item;

public interface IUpgradable {

    UPGRADEINSTALLSTATE onUpgradeUsed(Item item);

    enum UPGRADEINSTALLSTATE {
        INSTALL_OK,
        INSTALL_ALREADY_INSTALLED,
        NOT_VALID

    }
}

package com.tomevoll.routerreborn.core.items;

import com.tomevoll.routerreborn.core.items.IUpgradable.UPGRADEINSTALLSTATE;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class ItemUpgrade extends Item {

    String upgName = "";

    public ItemUpgrade(String upgname) {
        super();
        this.upgName = upgname;
    }


    @Override
    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX,
                                           float hitY, float hitZ, EnumHand hand) {

        if (world.isRemote)
            return super.onItemUseFirst(player, world, pos, side, hitX, hitY, hitZ, hand);


        if (player.isSneaking()) {
            TileEntity tile = world.getTileEntity(pos);
            EnumActionResult res = installUpgrade(this, tile, player);

            if (res == EnumActionResult.SUCCESS)
                player.getHeldItem(hand).shrink(1);

            return res;

            //return super.onItemUseFirst(player, world, pos, side, hitX, hitY, hitZ, hand);
        } else
            return super.onItemUseFirst(player, world, pos, side, hitX, hitY, hitZ, hand);


    }

    @Override
    public boolean canItemEditBlocks() {
        return true;
    }

    @Override
    public boolean doesSneakBypassUse(ItemStack stack, IBlockAccess world, BlockPos pos, EntityPlayer player) {
        return world.getTileEntity(pos) instanceof IUpgradable;
    }

    EnumActionResult installUpgrade(Item item, TileEntity tile, EntityPlayer player) {
        if (tile instanceof IUpgradable) {
            UPGRADEINSTALLSTATE status = ((IUpgradable) tile).onUpgradeUsed(this);
            switch (status) {
                case INSTALL_OK:
                    player.sendStatusMessage(new TextComponentString(upgName + " installed."), false);
                    return EnumActionResult.SUCCESS;
                case INSTALL_ALREADY_INSTALLED:
                    player.sendStatusMessage(new TextComponentString(upgName + " is already installed."), false);

                    return EnumActionResult.FAIL;
                case NOT_VALID:
                    player.sendStatusMessage(new TextComponentString(upgName + " upgrade not valid for this machine."), false);

                    return EnumActionResult.FAIL;
                default:
                    player.sendStatusMessage(new TextComponentString("Upgrade install failed."), false);

                    return EnumActionResult.FAIL;
            }


        } else
            return EnumActionResult.FAIL;
    }

}

package com.tomevoll.routerreborn.core.blocks;

import com.tomevoll.routerreborn.core.tileentity.TileEntityRouter;
import com.tomevoll.routerreborn.lib.block.GuiBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.world.World;

public class BlockRouter extends GuiBlock {

    public BlockRouter(Material materialIn) {
        super(materialIn);
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public TileEntity createGuiTileEntity(World worldIn, int meta) {
        return new TileEntityRouter();
    }

}

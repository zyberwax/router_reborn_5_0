package com.tomevoll.routerreborn.core.blocks;

import com.tomevoll.routerreborn.core.Blocks;
import com.tomevoll.routerreborn.core.RouterReborn;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;

import javax.annotation.Nullable;
import java.util.Random;

public class BlockAirLight extends BlockAir {

    public BlockAirLight() {
        super();
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState blockState, World worldIn, BlockPos pos) {
        return null;
    }

    @Override
    public boolean canCollideCheck(IBlockState state, boolean hitIfLiquid) {
        return false;
    }

    @Nullable
    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return null;
    }

    @Override
    public boolean isNormalCube(IBlockState state, IBlockAccess world, BlockPos pos) {
        return false;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public boolean isSideSolid(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isAir(IBlockState state, IBlockAccess world, BlockPos pos) {
        if (world.getBlockState(pos).getBlock().equals(this))
            return true;

        return super.isAir(state, world, pos);
    }

    @Override
    public boolean isCollidable() {
        return false;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos) {
        Block block = world.getBlockState(pos).getBlock();
        if (block != this) {
            return block.getLightValue(state, world, pos);
        }
        return getLightValue(state);
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public int getLightOpacity(IBlockState state) {
        return 0;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public int getLightValue(IBlockState state) {
        return 15;
    }


    @Override
    public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos) {
        return true;
    }

    @Override
    public boolean isBurning(IBlockAccess world, BlockPos pos) {
        return false;
    }

    @Override
    public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player) {
        return false;
    }

    @Override
    public int getFlammability(IBlockAccess world, BlockPos pos, EnumFacing face) {
        return 0;
    }

    @Override
    public boolean isFlammable(IBlockAccess world, BlockPos pos, EnumFacing face) {
        return false;
    }

    @Override
    public boolean canBeReplacedByLeaves(IBlockState state, IBlockAccess world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable) {
        return false;
    }

    @Override
    public boolean canEntityDestroy(IBlockState state, IBlockAccess world, BlockPos pos, Entity entity) {
        return false;
    }

    private boolean isValidBlockAbove(Block block) {
        return block instanceof BlockAirLight || block instanceof BlockLamp;
    }


    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        Block bl = worldIn.getBlockState(pos.up()).getBlock();
        if (!isValidBlockAbove(bl))
            worldIn.setBlockToAir(pos);
        else {
            bl = worldIn.getBlockState(pos.down()).getBlock();
            if (bl instanceof BlockAir)
                worldIn.setBlockState(pos.down(), RouterReborn.BLOCKS.LIGHT_BLOCK.getBlockState().getBaseState(), 3);
        }
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
    }


    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
        Block bl = worldIn.getBlockState(pos.down()).getBlock();
        if (bl instanceof BlockAir)
            worldIn.setBlockState(pos.down(), RouterReborn.BLOCKS.LIGHT_BLOCK.getBlockState().getBaseState(), 3);
        super.onBlockAdded(worldIn, pos, state);
    }
}

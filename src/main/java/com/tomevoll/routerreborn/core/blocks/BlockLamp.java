package com.tomevoll.routerreborn.core.blocks;


import com.tomevoll.routerreborn.core.Blocks;
import com.tomevoll.routerreborn.core.RouterReborn;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public class BlockLamp extends Block {

    public BlockLamp(Material p_i45394_1_) {
        super(p_i45394_1_);
    }

    @Override
    public boolean canConnectRedstone(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side) {
        return true;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public boolean canProvidePower(IBlockState state) {
        return false;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public int getLightValue(IBlockState state) {
        return this.lightValue;
    }

    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
        int rstone = worldIn.getStrongPower(pos);

        if (rstone > 0) {
            Block bl = worldIn.getBlockState(pos.down()).getBlock();
            if ((bl instanceof BlockAirLight))
                worldIn.setBlockToAir(pos.down());

        } else {
            Block bl = worldIn.getBlockState(pos.down()).getBlock();
            if (bl instanceof BlockAir)
                worldIn.setBlockState(pos.down(), RouterReborn.BLOCKS.LIGHT_BLOCK.getBlockState().getBaseState(), 3);
        }
        super.onBlockAdded(worldIn, pos, state);
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        int rstone = worldIn.getStrongPower(pos);

        if (rstone > 0) {
            Block bl = worldIn.getBlockState(pos.down()).getBlock();
            if ((bl instanceof BlockAirLight))
                worldIn.setBlockToAir(pos.down());

        } else {
            Block bl = worldIn.getBlockState(pos.down()).getBlock();
            if (bl instanceof BlockAir)
                worldIn.setBlockState(pos.down(), RouterReborn.BLOCKS.LIGHT_BLOCK.getBlockState().getBaseState(), 3);
        }
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
    }

    @SideOnly(Side.CLIENT)
    public void RegisterRenderer(String name) {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(this), 0, new net.minecraft.client.renderer.block.model.ModelResourceLocation(RouterReborn.MODID + ":" + name, "inventory"));
    }


}

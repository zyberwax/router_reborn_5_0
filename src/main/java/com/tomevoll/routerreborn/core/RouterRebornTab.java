package com.tomevoll.routerreborn.core;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RouterRebornTab extends CreativeTabs {
    private String name;

    public RouterRebornTab(int par1, String par2Str) {
        super(par1, par2Str);
        name = par2Str;

    }

    @Override
    @SideOnly(Side.CLIENT)
    public ItemStack getIconItemStack() {
        return new ItemStack(RouterReborn.BLOCKS.ROUTER, 1, 0);
    }

    public String getTranslatedTabLabel() {
        return name;

    }

    @Override
    @SideOnly(Side.CLIENT)
    public ItemStack getTabIconItem() {
        return new ItemStack(Item.getItemFromBlock(RouterReborn.BLOCKS.ROUTER));
    }


}

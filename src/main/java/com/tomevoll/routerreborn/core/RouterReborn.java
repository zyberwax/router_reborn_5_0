package com.tomevoll.routerreborn.core;

import com.tomevoll.routerreborn.core.proxy.IProxy;
import com.tomevoll.routerreborn.lib.gui.GuiRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

import java.io.File;

@Mod(modid = RouterReborn.MODID, version = RouterReborn.VERSION, name = "RouterReborn", acceptedMinecraftVersions = "1.12")
public class RouterReborn {
    public static final String MODID = "routerreborn";
    public static final String VERSION = "@VERSION@";
    public static SimpleNetworkWrapper network;
    public static File config;

    public static Blocks BLOCKS;
    public static Items ITEMS;
    public static RouterRebornTab TAB;


    @Instance(MODID)
    public static RouterReborn instance;


    @SidedProxy(clientSide = "com.tomevoll.routerreborn.core.proxy.ClientProxy", serverSide = "com.tomevoll.routerreborn.core.proxy.ServerProxy")
    public static IProxy proxy;


    public RouterReborn() {
        BLOCKS = new Blocks();
        ITEMS = new Items();
        TAB = new RouterRebornTab(CreativeTabs.CREATIVE_TAB_ARRAY.length, "Router Reborn");
    }


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        BLOCKS.Init();
        BLOCKS.Register();
        ITEMS.Init();
        ITEMS.Register();

        proxy.RegisterEvents();

        config = event.getModConfigurationDirectory();

        network = NetworkRegistry.INSTANCE.newSimpleChannel("RouterReborn");
        new GuiRegistry(network);


    }

    @EventHandler
    public void Init(FMLInitializationEvent event) {
        //register blocks + items

        proxy.RegisterRenders();
        proxy.RegisterTileEntitySpecialRenderer();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }


}

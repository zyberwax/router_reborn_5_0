package com.tomevoll.routerreborn.core;

import com.tomevoll.routerreborn.core.items.ItemUpgrade;
import com.tomevoll.routerreborn.core.tools.ItemPickaxe;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Items {

    public static ItemUpgrade MACHINEFILTER;
    public static ItemUpgrade ITEMFILTER;
    public static ItemUpgrade SPEED;
    public static ItemUpgrade EJECT;
    public static ItemUpgrade BANDWIDTH;
    public static ItemUpgrade THOROUGH;

    public static ItemPickaxe RR_PICKAXE;

    @SideOnly(Side.CLIENT)
    public static void registerRender() {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(MACHINEFILTER, 0, new ModelResourceLocation(MACHINEFILTER.getRegistryName(), "inventory"));
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(ITEMFILTER, 0, new ModelResourceLocation(ITEMFILTER.getRegistryName(), "inventory"));
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(EJECT, 0, new ModelResourceLocation(EJECT.getRegistryName(), "inventory"));
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(SPEED, 0, new ModelResourceLocation(SPEED.getRegistryName(), "inventory"));
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(BANDWIDTH, 0, new ModelResourceLocation(BANDWIDTH.getRegistryName(), "inventory"));
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(THOROUGH, 0, new ModelResourceLocation(THOROUGH.getRegistryName(), "inventory"));

        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(RR_PICKAXE, 0, new ModelResourceLocation(RR_PICKAXE.getRegistryName(), "inventory"));
    }

    public void Init() {
        MACHINEFILTER = (ItemUpgrade) new ItemUpgrade("Machinefilter").setRegistryName(RouterReborn.MODID, "machinefilter").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("machinefilter");
        ITEMFILTER = (ItemUpgrade) new ItemUpgrade("Itemfilter").setRegistryName(RouterReborn.MODID, "itemfilter").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("itemfilter");
        EJECT = (ItemUpgrade) new ItemUpgrade("Eject").setRegistryName(RouterReborn.MODID, "eject").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("eject");
        SPEED = (ItemUpgrade) new ItemUpgrade("Speed").setRegistryName(RouterReborn.MODID, "speed").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("speed");
        BANDWIDTH = (ItemUpgrade) new ItemUpgrade("Bandwidth").setRegistryName(RouterReborn.MODID, "bandwidth").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("bandwidth");
        THOROUGH = (ItemUpgrade) new ItemUpgrade("Thorough").setRegistryName(RouterReborn.MODID, "thorough").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("thorough");

        RR_PICKAXE = (ItemPickaxe) new ItemPickaxe().setRegistryName(RouterReborn.MODID, "pickaxe").setCreativeTab(RouterReborn.TAB).setUnlocalizedName("pickaxe");
        //thorough

    }

    public void Register() {
        //ForgeRegistries.ITEMS.register(new ItemBlock(ROUTER).setUnlocalizedName("router").setRegistryName(RouterReborn.MODID ,"router").setCreativeTab(RouterReborn.TAB));
        ForgeRegistries.ITEMS.register(MACHINEFILTER);
        ForgeRegistries.ITEMS.register(ITEMFILTER);
        ForgeRegistries.ITEMS.register(EJECT);
        ForgeRegistries.ITEMS.register(SPEED);
        ForgeRegistries.ITEMS.register(BANDWIDTH);
        ForgeRegistries.ITEMS.register(THOROUGH);

        ForgeRegistries.ITEMS.register(RR_PICKAXE);
    }
}
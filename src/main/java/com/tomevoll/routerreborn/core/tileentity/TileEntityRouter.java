package com.tomevoll.routerreborn.core.tileentity;

import com.tomevoll.routerreborn.core.Items;
import com.tomevoll.routerreborn.core.RouterReborn;
import com.tomevoll.routerreborn.core.items.IUpgradable;
import com.tomevoll.routerreborn.lib.gui.ModuleRegistry;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.modules.eject.EjectClientModule;
import com.tomevoll.routerreborn.lib.gui.modules.eject.EjectServerModule;
import com.tomevoll.routerreborn.lib.gui.modules.eject.IEjectTile;
import com.tomevoll.routerreborn.lib.gui.modules.inoutside.IInOutSideTile;
import com.tomevoll.routerreborn.lib.gui.modules.inoutside.InOutSideClientModule;
import com.tomevoll.routerreborn.lib.gui.modules.inoutside.InOutSideServerModule;
import com.tomevoll.routerreborn.lib.gui.modules.itemfilter.IGuiItemFilterTile;
import com.tomevoll.routerreborn.lib.gui.modules.itemfilter.ItemFilterClientModule;
import com.tomevoll.routerreborn.lib.gui.modules.itemfilter.ItemFilterServerModule;
import com.tomevoll.routerreborn.lib.gui.modules.machinefilter.ITileFilterTile;
import com.tomevoll.routerreborn.lib.gui.modules.machinefilter.MachineFilterClientModule;
import com.tomevoll.routerreborn.lib.gui.modules.machinefilter.MachineFilterServerModule;
import com.tomevoll.routerreborn.lib.gui.modules.redstone.IGuiRedstoneTile;
import com.tomevoll.routerreborn.lib.gui.modules.redstone.RedstoneClientModule;
import com.tomevoll.routerreborn.lib.gui.modules.redstone.RedstoneServerModule;
import com.tomevoll.routerreborn.lib.gui.modules.textinfo.TextInfoClient;
import com.tomevoll.routerreborn.lib.gui.modules.textinfo.TextInfoServer;
import com.tomevoll.routerreborn.lib.inventory.*;
import com.tomevoll.routerreborn.lib.tile.GuiTileBase;
import com.tomevoll.routerreborn.lib.util.BlockUtil;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TileEntityRouter extends GuiTileBase implements ITickable, IGuiRedstoneTile, ITileFilterTile,
        IGuiItemFilterTile, IEjectTile, IInOutSideTile, IUpgradable, IItemHandler {

    CapabilityScanner SCANNER;
    int REDSTONE_MODE = 0;
    List<InfoTile> MACHINELIST = new ArrayList<InfoTile>();

    Class<?>[] PASS_THREW = {IInventoryCable.class, TileEntityRouter.class};
    TileFilter MACHINE_FILTER = new TileFilter(new ArrayList<String>(), Arrays.asList(PASS_THREW), false);

    ItemFilter ITEM_FILTER = new ItemFilter(true, true, true);
    int EJECT_SIDE = -1;

    ItemStack SLOT = ItemStack.EMPTY;
    boolean EXTRACT = false;
    int SIDE = 0;

    boolean SLOTMODE = false;
    int SELECTEDSLOT = 0;

    boolean UPGRADE_SPEED = false;
    boolean UPGRADE_STACK = false;
    boolean UPGRADE_REDSTONE = true;
    boolean UPGRADE_ITEMFILTER = false;
    boolean UPGRADE_MACHINEFILTER = false;
    boolean UPGRADE_SORT = false;
    boolean UPGRADE_EJECT = false;

    int CURRENT = 0;

    boolean isClogged = false;
    boolean hasScanned = false;
    int lastTransferFail = -1;
    boolean lastRedstone = false;

    public TileEntityRouter() {
        super();
    }

    @Override
    public void update() {
        if (world.isRemote)
            tickClient();
        else
            tickServer();
    }

    @Override
    public void readSyncFromNBT(NBTTagCompound tagCompound) {
        super.readSyncFromNBT(tagCompound);
        SLOT = new ItemStack(tagCompound.getCompoundTag("slotstack"));
        if (SLOT == null)
            SLOT = ItemStack.EMPTY;

        UPGRADE_SPEED = tagCompound.getBoolean("upgrade_speed");
        UPGRADE_STACK = tagCompound.getBoolean("upgrade_stack");
        UPGRADE_REDSTONE = tagCompound.getBoolean("upgrade_redstone");
        UPGRADE_ITEMFILTER = tagCompound.getBoolean("upgrade_itemfilter");
        UPGRADE_MACHINEFILTER = tagCompound.getBoolean("upgrade_machinefilter");
        UPGRADE_SORT = tagCompound.getBoolean("upgrade_sort");
        UPGRADE_EJECT = tagCompound.getBoolean("upgrade_eject");
    }


    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {

        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
            return true;
        return super.hasCapability(capability, facing);
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {

        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return (T) this;
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public NBTTagCompound writeSyncToNBT(NBTTagCompound tagCompound) {
        NBTTagCompound stack = new NBTTagCompound();
        stack = SLOT.writeToNBT(stack);
        tagCompound.setTag("slotstack", stack);

        tagCompound.setBoolean("upgrade_speed", UPGRADE_SPEED);
        tagCompound.setBoolean("upgrade_stack", UPGRADE_STACK);
        tagCompound.setBoolean("upgrade_redstone", UPGRADE_REDSTONE);
        tagCompound.setBoolean("upgrade_itemfilter", UPGRADE_ITEMFILTER);
        tagCompound.setBoolean("upgrade_machinefilter", UPGRADE_MACHINEFILTER);
        tagCompound.setBoolean("upgrade_sort", UPGRADE_SORT);
        tagCompound.setBoolean("upgrade_eject", UPGRADE_EJECT);

        return super.writeSyncToNBT(tagCompound);
    }

    @Override
    public void readFromNBT(NBTTagCompound tagCompound) {
        super.readFromNBT(tagCompound);
        MACHINE_FILTER.readFromNBT(tagCompound);
        ITEM_FILTER.readFromNBT(tagCompound);
        EJECT_SIDE = tagCompound.getInteger("ejectside");
        REDSTONE_MODE = tagCompound.getInteger("redstonemode");
        EXTRACT = tagCompound.getBoolean("extract");
        SLOTMODE = tagCompound.getBoolean("slotmode");
        SIDE = tagCompound.getInteger("side");
        SELECTEDSLOT = tagCompound.getInteger("selectedslot");

        SLOT = new ItemStack(tagCompound.getCompoundTag("slotstack"));
        if (SLOT == null)
            SLOT = ItemStack.EMPTY;

        UPGRADE_SPEED = tagCompound.getBoolean("upgrade_speed");
        UPGRADE_STACK = tagCompound.getBoolean("upgrade_stack");
        UPGRADE_REDSTONE = tagCompound.getBoolean("upgrade_redstone");
        UPGRADE_ITEMFILTER = tagCompound.getBoolean("upgrade_itemfilter");
        UPGRADE_MACHINEFILTER = tagCompound.getBoolean("upgrade_machinefilter");
        UPGRADE_SORT = tagCompound.getBoolean("upgrade_sort");
        UPGRADE_EJECT = tagCompound.getBoolean("upgrade_eject");

    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tagCompound) {
        tagCompound = MACHINE_FILTER.writeToNBT(tagCompound);
        tagCompound = ITEM_FILTER.writeToNBT(tagCompound);
        tagCompound.setInteger("ejectside", EJECT_SIDE);
        tagCompound.setInteger("redstonemode", REDSTONE_MODE);

        tagCompound.setBoolean("extract", EXTRACT);
        tagCompound.setBoolean("slotmode", SLOTMODE);
        tagCompound.setInteger("side", SIDE);
        tagCompound.setInteger("selectedslot", SELECTEDSLOT);

        NBTTagCompound stack = new NBTTagCompound();
        stack = SLOT.writeToNBT(stack);
        tagCompound.setTag("slotstack", stack);

        tagCompound.setBoolean("upgrade_speed", UPGRADE_SPEED);
        tagCompound.setBoolean("upgrade_stack", UPGRADE_STACK);
        tagCompound.setBoolean("upgrade_redstone", UPGRADE_REDSTONE);
        tagCompound.setBoolean("upgrade_itemfilter", UPGRADE_ITEMFILTER);
        tagCompound.setBoolean("upgrade_machinefilter", UPGRADE_MACHINEFILTER);
        tagCompound.setBoolean("upgrade_sort", UPGRADE_SORT);
        tagCompound.setBoolean("upgrade_eject", UPGRADE_EJECT);

        return super.writeToNBT(tagCompound);
    }

    private void tickServer() {

        if (SCANNER != null) {
            if (!SCANNER.isDone()) {
                SCANNER.Tick();
                // check if done now
            }

            if (SCANNER.isDone()) {
                MACHINELIST = SCANNER.getResult();
                SCANNER = null;
            }
        }

        //Auto rescan on world load or first placement
        if (world.getTotalWorldTime() % 5 == 0) {
            if (!hasScanned) {
                doRescan();
                hasScanned = true;
                return;
            }

            if (!checkRedstone()) return;


            if (UPGRADE_EJECT) {
                if (EXTRACT) {
                    doEject();
                }
            }

			/*
             * if(MACHINELIST.isEmpty() && SCANNER == null) { doRescan(); return; }
			 */
            if (!MACHINELIST.isEmpty()) {
                if (CURRENT >= MACHINELIST.size())
                    CURRENT = 0;

                if (SLOT.isEmpty()) {
                    if (isClogged) {
                        isClogged = false;
                        world.notifyNeighborsOfStateChange(pos, this.getBlockType(), false);
                        BlockUtil.markBlockForUpdate(world, pos);
                    }
                }

                if (!EXTRACT && SLOT.isEmpty())
                    return;

                InfoTile iTile = MACHINELIST.get(CURRENT);

                if (iTile.tile.isInvalid()) {
                    doRescan();
                    CURRENT = 0;
                    return;
                }

                boolean didTransfer = false;

                if (!EXTRACT)
                    didTransfer = UpdateOutput(iTile);
                else
                    didTransfer = updateExtract(iTile);

                if (didTransfer)
                    lastTransferFail = -1;


                //Handle overflow eject
                if (!didTransfer && !EXTRACT) {
                    if (lastTransferFail == -1)
                        lastTransferFail = CURRENT;
                    else {
                        if (lastTransferFail == CURRENT) {
                            if (UPGRADE_EJECT)
                                doEject();
                            isClogged = true;
                            world.notifyNeighborsOfStateChange(pos, this.getBlockType(), false);
                            BlockUtil.markBlockForUpdate(world, pos);
                        }
                    }
                }

                if (didTransfer) {
                    if (isClogged) {
                        isClogged = false;
                        world.notifyNeighborsOfStateChange(pos, this.getBlockType(), false);
                        BlockUtil.markBlockForUpdate(world, pos);
                    }
                }

                if (!UPGRADE_SORT || (UPGRADE_SORT && !didTransfer))
                    CURRENT++;
            }
        }
    }

    private void doEject() {

        if (EJECT_SIDE > -1) {
            TileEntity tile = world.getTileEntity(pos.add(EnumFacing.VALUES[EJECT_SIDE].getDirectionVec()));
            if (tile != null && tile.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.VALUES[EJECT_SIDE].getOpposite())) {
                IItemHandler hand = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.VALUES[EJECT_SIDE].getOpposite());
                SLOT = TransferHelper.insertStackToAny(hand, SLOT, 64);
            }
        }

    }

    boolean updateExtract(InfoTile tile) {
        IItemHandler handler = null;
        boolean didTransfer = false;

        ItemStack extracted = ItemStack.EMPTY;

        handler = tile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                SLOTMODE ? null : EnumFacing.VALUES[SIDE]);
        boolean currentCanExtract = TransferHelper.CanExtractFrom(handler, SLOT, SLOTMODE ? SELECTEDSLOT : -1,
                UPGRADE_ITEMFILTER ? ITEM_FILTER : null);

        if (currentCanExtract) {
            extracted = TransferHelper.ExtractFrom(handler, SLOT, SLOTMODE ? SELECTEDSLOT : -1,
                    UPGRADE_ITEMFILTER ? ITEM_FILTER : null, UPGRADE_STACK ? 64 : 1);
            if (!extracted.isEmpty()) {
                if (!SLOT.isEmpty())
                    SLOT.grow(extracted.getCount());
                else
                    SLOT = extracted.copy();

                return true;
            }
        } else {
            if (UPGRADE_SPEED) {
                int r = TransferHelper.GetFirstCanExtractFrom(MACHINELIST, SLOT, SLOTMODE ? SELECTEDSLOT : -1,
                        SLOTMODE ? null : EnumFacing.VALUES[SIDE], UPGRADE_ITEMFILTER ? ITEM_FILTER : null, CURRENT);
                if (r == -1)
                    return false;
                CURRENT = r;

                handler = MACHINELIST.get(CURRENT).tile.getCapability(
                        CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, SLOTMODE ? null : EnumFacing.VALUES[SIDE]);

                extracted = TransferHelper.ExtractFrom(handler, SLOT, SLOTMODE ? SELECTEDSLOT : -1,
                        UPGRADE_ITEMFILTER ? ITEM_FILTER : null, UPGRADE_STACK ? 64 : 1);
                if (!extracted.isEmpty()) {
                    if (!SLOT.isEmpty())
                        SLOT.grow(extracted.getCount());
                    else
                        SLOT = extracted.copy();

                    return true;
                }
            } else
                return false;
        }

        return !extracted.isEmpty();
    }

    boolean UpdateOutput(InfoTile iTile) {
        IItemHandler handler = null;
        boolean didTransfer = false;

        // No point looping threw machines if item dont pass the filter
        if (!UPGRADE_ITEMFILTER || ITEM_FILTER.CheckFilter(SLOT)) {
            ItemStack ret = ItemStack.EMPTY;

            // SORTING and SPEED, jump to next machine that has the item already, and can
            // accept more, or just move on if none
            if (UPGRADE_SORT && UPGRADE_SPEED) {
                if (!SLOTMODE) {
                    if (!TransferHelper.HasTypeAndCanAcceptStack(iTile, EnumFacing.VALUES[SIDE], SLOT)) {
                        int r = TransferHelper.GetFirstHasAndCanAcceptStack(MACHINELIST, SLOT, EnumFacing.VALUES[SIDE]);
                        if (r > -1) {
                            CURRENT = r;

                        }
                    }
                    handler = MACHINELIST.get(CURRENT).tile.getCapability(
                            CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.VALUES[SIDE]);
                } else {
                    if (!TransferHelper.SlotHasTypeAndCanAcceptStack(iTile, null, SLOT, SELECTEDSLOT)) {
                        int r = TransferHelper.GetFirstSlotHasAndCanAcceptStack(MACHINELIST, SLOT, SELECTEDSLOT);
                        if (r > -1) {
                            CURRENT = r;

                        }
                    }
                    handler = MACHINELIST.get(CURRENT).tile
                            .getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

                }
            }
            // NO SORTING; SPEED UPGRADE (jump to next machine that can accept the item)
            else if (!UPGRADE_SORT && UPGRADE_SPEED) {
                int r = -1;
                if (!SLOTMODE)
                    r = TransferHelper.GetFirstCanAcceptStack(MACHINELIST, SLOT, EnumFacing.VALUES[SIDE], CURRENT);
                else
                    r = TransferHelper.GetFirstSlotCanAcceptStack(MACHINELIST, SLOT, SELECTEDSLOT, CURRENT);

                if (r != CURRENT && r > -1) {
                    CURRENT = r;
                }

                if (!SLOTMODE)
                    handler = MACHINELIST.get(CURRENT).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                            EnumFacing.VALUES[SIDE]);
                else
                    handler = MACHINELIST.get(CURRENT).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                            null);
            }
            // SORTING AND NO SPEED (return if the first machine that accept the item that
            // already has this item is not the current index)
            else if (UPGRADE_SORT && !UPGRADE_SPEED) {
                int r = -1;
                if (!SLOTMODE)
                    r = TransferHelper.GetFirstHasAndCanAcceptStack(MACHINELIST, SLOT, EnumFacing.VALUES[SIDE]);
                else
                    r = TransferHelper.GetFirstSlotHasAndCanAcceptStack(MACHINELIST, SLOT, SELECTEDSLOT);

                if (r > -1 && r != CURRENT)
                    return false;

                if (!SLOTMODE)
                    handler = iTile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                            EnumFacing.VALUES[SIDE]);
                else
                    handler = iTile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            }
            // DEFAULT NO SORTING AND NO SPEED UPGRADE (Gets next machine nothing else)
            else {
                if (!SLOTMODE)
                    handler = iTile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                            EnumFacing.VALUES[SIDE]);
                else
                    handler = iTile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            }

            if (handler == null)
                return false;

            // Do the transfer
            if (!SLOTMODE)
                ret = TransferHelper.insertStackToAny(handler, SLOT, UPGRADE_STACK ? 64 : 1);
            else
                ret = TransferHelper.insertStackToSlot(handler, SLOT, UPGRADE_STACK ? 64 : 1, SELECTEDSLOT);

            // Check if anything was transfered
            if (ret.getCount() != SLOT.getCount())
                didTransfer = true;

            SLOT = ret;
        }

        return didTransfer;
    }

    private void tickClient() {

    }

    @Override
    public String getGuiDisplayName() {
        return "Router";
    }

    @Override
    public void registerClientGuiModule(ModuleRegistry registry) {
        registry.registerClientModule(new InOutSideClientModule(new ItemStack(RouterReborn.BLOCKS.ROUTER), this));

        if (UPGRADE_MACHINEFILTER)
            registry.registerClientModule(
                    new MachineFilterClientModule(new ItemStack(Items.MACHINEFILTER)).setCanUninstall(true));
        if (UPGRADE_ITEMFILTER)
            registry.registerClientModule(
                    new ItemFilterClientModule(new ItemStack(Items.ITEMFILTER), this, ITEM_FILTER, false).setCanUninstall(true));
        if (UPGRADE_SPEED)
            registry.registerClientModule(new TextInfoClient(new ItemStack(Items.SPEED),
                    "Will increse speed by jumping to the next available machine that can accept the item, or can be extracted from",
                    "speedupgrade").setCanUninstall(true));
        if (UPGRADE_STACK)
            registry.registerClientModule(new TextInfoClient(new ItemStack(Items.BANDWIDTH),
                    "Will transfer a stack at the time.", "stackupgrade").setCanUninstall(true));
        if (UPGRADE_SORT)
            registry.registerClientModule(new TextInfoClient(new ItemStack(Items.THOROUGH),
                    "Will fill one machine before moving on.\nCan be used to sort items in to barrels.",
                    "sortupgrade").setCanUninstall(true));

        if (UPGRADE_EJECT)
            registry.registerClientModule(new EjectClientModule(new ItemStack(Items.EJECT), this).setCanUninstall(true));
        if (UPGRADE_REDSTONE)
            registry.registerClientModule(new RedstoneClientModule(this));

    }

    @Override
    public void registerServerGuiModule(ModuleRegistry registry) {
        registry.registerServerModule(new InOutSideServerModule(this));
        if (UPGRADE_REDSTONE)
            registry.registerServerModule(new RedstoneServerModule());
        if (UPGRADE_MACHINEFILTER)
            registry.registerServerModule(new MachineFilterServerModule(this, MACHINE_FILTER));
        if (UPGRADE_ITEMFILTER)
            registry.registerServerModule(new ItemFilterServerModule(this, ITEM_FILTER));
        if (UPGRADE_SPEED)
            registry.registerServerModule(new TextInfoServer(this, "speedupgrade"));
        if (UPGRADE_STACK)
            registry.registerServerModule(new TextInfoServer(this, "stackupgrade"));
        if (UPGRADE_SORT)
            registry.registerServerModule(new TextInfoServer(this, "sortupgrade"));

        if (UPGRADE_EJECT)
            registry.registerServerModule(new EjectServerModule(this));
    }

    @Override
    public int getRedstoneMode() {
        return REDSTONE_MODE;
    }

    @Override
    public void setRedstoneMode(int mode) {
        REDSTONE_MODE = mode;
        this.markDirty();
    }

    @Override
    public void RegisterRedstoneModes(RedstoneClientModule handler) {
        handler.AddRedstoneMode("Ignore", 0);
        handler.AddRedstoneMode("Active High", 1);
        handler.AddRedstoneMode("Active Low", 2);
        handler.AddRedstoneMode("On Pulse", 3);
        handler.AddRedstoneMode("Emit When Clogged", 4);
        handler.AddRedstoneMode("Rescan on Pulse", 5);
    }

    @Override
    public int getRedstoneOutput() {
        return isClogged ? 15 : 0;
    }

    @Override
    public boolean canProvideRedstone() {
        return REDSTONE_MODE == 4;
    }

    private boolean checkRedstone() {
        switch (REDSTONE_MODE) {
            case 1:
                return isBlockIndirectlyGettingPowered(pos) > 0;
            case 2:
                return isBlockIndirectlyGettingPowered(pos) <= 0;
            case 3:
                if (!lastRedstone && isBlockIndirectlyGettingPowered(pos) > 0) {
                    lastRedstone = true;
                    return true;
                }
                lastRedstone = isBlockIndirectlyGettingPowered(pos) > 0;
                return false;
            case 5:
                if (!lastRedstone && isBlockIndirectlyGettingPowered(pos) > 0) {
                    lastRedstone = true;
                    doRescan(); //rescan on pulse
                }
                lastRedstone = isBlockIndirectlyGettingPowered(pos) > 0;
                return true;

            default:
                return true;
        }
    }


    @Override
    public void doRescan() {
        SCANNER = new CapabilityScanner();
        if (UPGRADE_MACHINEFILTER)
            SCANNER.scanForCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, world, pos, 4, MACHINE_FILTER);
        else {
            TileFilter filter = new TileFilter(new ArrayList<String>(), Arrays.asList(PASS_THREW), true);
            SCANNER.scanForCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, world, pos, 4, filter);
        }

    }

    @Override
    public List<InfoTile> getUnfilteredList() {
        SCANNER = new CapabilityScanner();
        List<InfoTile> lst = SCANNER.scanForConnectedCapabilitySTA(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, world,
                pos, Arrays.asList(PASS_THREW));
        SCANNER = null;
        return lst;

    }

    @Override
    public EnumFacing getEjectVal() {
        return EJECT_SIDE == -1 ? null : EnumFacing.VALUES[EJECT_SIDE];
    }

    @Override
    public void setEjectValue(EnumFacing val) {
        if (val == null)
            EJECT_SIDE = -1;
        else
            EJECT_SIDE = val.ordinal();

        this.markDirty();
    }

    @Override
    public ItemStack getInOutSlotStack() {
        if (SLOT == null)
            SLOT = ItemStack.EMPTY;
        return SLOT;
    }

    @Override
    public void setInOutSlotStack(ItemStack stack) {
        SLOT = stack;
    }

    @Override
    public int getInOutSide() {
        return SIDE;
    }

    @Override
    public void setInOutSide(int side) {
        SIDE = side;
    }

    @Override
    public int getInOutMode() {
        return EXTRACT ? 1 : 0;
    }

    @Override
    public void setInOutMode(int mode) {
        EXTRACT = mode == 1;
    }

    @Override
    public int getInOutSlot() {
        return SELECTEDSLOT;
    }

    @Override
    public void setInOutSlot(int s) {
        SELECTEDSLOT = s;
    }

    @Override
    public boolean getIsSlotMode() {
        return SLOTMODE;
    }

    @Override
    public void setIsSlotMode(boolean slotMode) {
        SLOTMODE = slotMode;
    }

    @Override
    public UPGRADEINSTALLSTATE onUpgradeUsed(Item item) {

        if (item.equals(Items.MACHINEFILTER)) {
            if (UPGRADE_MACHINEFILTER)
                return UPGRADEINSTALLSTATE.INSTALL_ALREADY_INSTALLED;
            else {
                UPGRADE_MACHINEFILTER = true;
                return UpgradeInstalled();
            }
        }

        if (item.equals(Items.ITEMFILTER)) {
            if (UPGRADE_ITEMFILTER)
                return UPGRADEINSTALLSTATE.INSTALL_ALREADY_INSTALLED;
            else {
                UPGRADE_ITEMFILTER = true;
                return UpgradeInstalled();
            }
        }

        if (item.equals(Items.EJECT)) {
            if (UPGRADE_EJECT)
                return UPGRADEINSTALLSTATE.INSTALL_ALREADY_INSTALLED;
            else {
                UPGRADE_EJECT = true;
                return UpgradeInstalled();
            }
        }

        if (item.equals(Items.SPEED)) {
            if (UPGRADE_SPEED)
                return UPGRADEINSTALLSTATE.INSTALL_ALREADY_INSTALLED;
            else {
                UPGRADE_SPEED = true;
                return UpgradeInstalled();
            }
        }
        if (item.equals(Items.BANDWIDTH)) {
            if (UPGRADE_STACK)
                return UPGRADEINSTALLSTATE.INSTALL_ALREADY_INSTALLED;
            else {
                UPGRADE_STACK = true;
                return UpgradeInstalled();

            }
        }
        if (item.equals(Items.THOROUGH)) {
            if (UPGRADE_SORT)
                return UPGRADEINSTALLSTATE.INSTALL_ALREADY_INSTALLED;
            else {
                UPGRADE_SORT = true;
                return UpgradeInstalled();
            }
        }

        return UPGRADEINSTALLSTATE.NOT_VALID;
    }

    private  UPGRADEINSTALLSTATE UpgradeInstalled()
    {
        this.markDirty();
        BlockUtil.markBlockForUpdate(world, pos);
        return UPGRADEINSTALLSTATE.INSTALL_OK;
    }

    @Override
    public void uninstallGuiTab(ModuleServerBase module) {
        ItemStack toDrop = ItemStack.EMPTY;

        if (module instanceof EjectServerModule) {
            toDrop = new ItemStack(Items.EJECT);
            UPGRADE_EJECT = false;
        }

        if (module instanceof ItemFilterServerModule) {
            toDrop = new ItemStack(Items.ITEMFILTER);
            UPGRADE_ITEMFILTER = false;
        }
        if (module instanceof MachineFilterServerModule) {
            toDrop = new ItemStack(Items.MACHINEFILTER);
            UPGRADE_MACHINEFILTER = false;
        }
        if (module instanceof TextInfoServer) {
            if (module.getModuleID().equals("speedupgrade")) {
                toDrop = new ItemStack(Items.SPEED);
                UPGRADE_SPEED = false;
            }

            if (module.getModuleID().equals("stackupgrade")) {
                toDrop = new ItemStack(Items.BANDWIDTH);
                UPGRADE_STACK = false;
            }

            if (module.getModuleID().equals("sortupgrade")) {
                toDrop = new ItemStack(Items.THOROUGH);
                UPGRADE_SORT = false;
            }

        }


        if (!toDrop.isEmpty())
            Block.spawnAsEntity(world, pos, toDrop); //world.spawnEntity(new EntityItem(world, pos.getX(), pos.getY(), pos.getY(), toDrop));


        this.markDirty();
        BlockUtil.markBlockForUpdate(world, pos);
    }

    @Override
    public int getSlots() {
        return 1;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        return SLOT;
    }

    @Override
    public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {

        if (SLOT.isEmpty()) {
            if (!simulate)
                SLOT = stack.copy();

            return ItemStack.EMPTY;
        } else {
            if (ItemStack.areItemsEqual(stack, SLOT)) {
                int max = SLOT.getMaxStackSize() - SLOT.getCount();
                int toTR = Math.min(max, stack.getCount());

                if (!simulate)
                    SLOT.grow(toTR);

                ItemStack ret = stack.copy();
                ret.setCount(stack.getCount() - toTR);
                return ret;
            }
        }
        return stack;
    }

    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
        int max = Math.min(amount, SLOT.getCount());
        if (SLOT.isEmpty())
            return ItemStack.EMPTY;

        if (!simulate)
            return SLOT.splitStack(max);
        else
            return SLOT.copy().splitStack(max);
    }

    @Override
    public int getSlotLimit(int slot) {
        return 64;
    }

}

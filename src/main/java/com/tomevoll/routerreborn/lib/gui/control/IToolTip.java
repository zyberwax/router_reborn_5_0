package com.tomevoll.routerreborn.lib.gui.control;

import net.minecraft.client.Minecraft;

public interface IToolTip {
    void DrawTooltips(Minecraft mc);
}

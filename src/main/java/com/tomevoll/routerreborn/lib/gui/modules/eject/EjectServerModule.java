package com.tomevoll.routerreborn.lib.gui.modules.eject;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import net.minecraft.util.EnumFacing;

public class EjectServerModule extends ModuleServerBase {
    IEjectTile tile;
    EnumFacing ejectSide;

    public EjectServerModule(IEjectTile tile) {
        this.tile = tile;
    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        if (cmd == 1) {
            if (val2 == -1) {
                tile.setEjectValue(null);
            } else {
                tile.setEjectValue(EnumFacing.VALUES[val2]);
            }
        }

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
        EnumFacing f = tile.getEjectVal();

        if (f != ejectSide) {
            if (f == null) {
                container.sendToClient(getModuleID(), 1, -1);
            } else {
                container.sendToClient(getModuleID(), 1, f.getIndex());
            }
            ejectSide = f;
        }
    }

    @Override
    public String getModuleID() {
        // TODO Auto-generated method stub
        return "eject";
    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }

}

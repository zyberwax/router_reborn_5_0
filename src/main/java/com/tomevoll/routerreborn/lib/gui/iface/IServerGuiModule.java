package com.tomevoll.routerreborn.lib.gui.iface;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;

public interface IServerGuiModule extends IGuiModule {
    void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2);

    void detectAndSendChanges(ContainerServer container, IGuiTile t);
}

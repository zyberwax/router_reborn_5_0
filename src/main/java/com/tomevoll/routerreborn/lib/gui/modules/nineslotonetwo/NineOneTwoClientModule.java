package com.tomevoll.routerreborn.lib.gui.modules.nineslotonetwo;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

import java.util.Arrays;

public class NineOneTwoClientModule extends ModuleClientBase {
    INineOneTwoTile tile;
    private int storedEnergy = 0;
    private int maxEnergy = 0;
    private boolean useEnergy = false;

    public NineOneTwoClientModule(INineOneTwoTile tile, boolean useEnergy, ItemStack icon) {
        super(icon);
        this.tile = tile;
        this.useEnergy = useEnergy;
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        switch (cmd) {
            case 1:
                storedEnergy = val2;
                break;
            case 2:
                maxEnergy = val2;
                break;
    /*
        case 3:
			burnTime = val2;
			break;
		case 4:
			totalBurnTime = val2;
			break;
			*/
            case 5:
                useEnergy = val2 == 1;
                break;
        /*
		case 6:
			processTime = val2;
			break;
		case 7:
			processTimeTotal = val2;
			break;
			*/
            default:
                break;
        }
    }

    @Override
    public void DrawBG() {
        super.DrawBG();

        if (useEnergy) gui.drawEnergyBar(10, 28, 10, 52, maxEnergy, storedEnergy);

        //gui.drawProgressBar(75, 47, processTimeTotal, processTime, false);

//		if (!useEnergy)	gui.drawBurnBar(52, 47, totalBurnTime, burnTime, false);
    }


    @Override
    public String getModuleID() {
        return "nineonetwo";
    }


    @Override
    protected void AddControls(IGuiController gui) {

    }

    @Override
    public void Draw() {
        // TODO Auto-generated method stub
        super.Draw();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        //super.drawScreen(mouseX, mouseY, partialTicks);

        int mx = mouseX - Left;
        int my = mouseY - Top - 17;

        if (mx > 10 && mx < 20 && my > 11 && my < 10 + 52) {
            String text = storedEnergy + "/" + maxEnergy + " RF"; //+ "\nInput: " + this.tileFurnace.getField(11) + " RF/t";
            this.gui.drawTooltipText(Arrays.asList(text.split("\n")), mouseX, mouseY, Minecraft.getMinecraft().fontRenderer);
        }
        if (mx > 115 + 36 && mx < 133 + 36 && my > 10 && my < 10 + (18 * 2)) {

            String text = "Allowed upgrades:\n* Router Speed upgrade\n* Chest Advanced Eject";
            this.gui.drawTooltipText(Arrays.asList(text.split("\n")), mouseX, mouseY, Minecraft.getMinecraft().fontRenderer);
        }
    }


    @Override
    protected void registerSlots() {

        int x = 62;
        int y = 28;
        int index = 0;
        for (int line = 0; line < 3; line++) {
            for (int column = 0; column < 3; column++) {
                addSlot(new GuiSlot(this, index, ((18) * column) + x, ((18) * line) + y));
                index++;
            }
        }

        addSlot(new GuiSlot(this, index, x - 30, y));
        index++;

        x = 115;
        for (int line = 0; line < 2; line++) {
            for (int column = 2; column < 3; column++) {
                addSlot(new GuiSlot(this, index, ((18) * column) + x, ((18) * line) + y));
                index++;
            }
        }

        //addSlot(new GuiSlot(this, 0, Left + 50, Top + (!useEnergy ? 28 : 45)));

        if (!useEnergy)
            addSlot(new GuiSlot(this, 1, Left + 50, Top + 62));
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        return tile.getGuiSlot(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        tile.setGuiSlot(slotIndex, stack);
    }

    @Override
    public float getSmeltingExperience(ItemStack stack) {
        return 0;
    }
}


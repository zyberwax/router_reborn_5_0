package com.tomevoll.routerreborn.lib.gui;

import com.tomevoll.routerreborn.lib.gui.iface.IClientGuiModule;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiModule;
import com.tomevoll.routerreborn.lib.gui.iface.IServerGuiModule;

import java.util.ArrayList;
import java.util.List;

public class ModuleRegistry {

    List<IGuiModule> modules = new ArrayList<IGuiModule>();

    public void registerServerModule(IServerGuiModule guiModule) {
        if (!modules.contains(guiModule)) {
            modules.add(guiModule);
        }
    }

    public void registerClientModule(IClientGuiModule guiModule) {
        if (!modules.contains(guiModule)) {
            modules.add(guiModule);
        }
    }


    public <T extends IGuiModule> T getModuleByID(String id) {
        for (int i = 0; i < modules.size(); i++) {
            if (modules.get(i).getModuleID().equals(id))
                return (T) modules.get(i);
        }
        return null;
    }

    public <T extends IGuiModule> T getModuleByIndex(int index) {
        if (index >= modules.size() || index < 0 || modules.size() == 0)
            return null;

        return (T) modules.get(index);
    }

    public int getCount() {
        return modules.size();
    }


    public IGuiModule[] getModules() {
        IGuiModule[] l = new IGuiModule[modules.size()];
        int i = 0;
        for (IGuiModule t : modules) {
            l[i] = t;
            i++;
        }

        return l;
    }

}

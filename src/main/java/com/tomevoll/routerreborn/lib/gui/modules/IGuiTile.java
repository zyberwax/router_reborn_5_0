package com.tomevoll.routerreborn.lib.gui.modules;

import com.tomevoll.routerreborn.lib.gui.ModuleRegistry;

public interface IGuiTile {
    String getGuiDisplayName();

    void registerClientGuiModule(ModuleRegistry registry);

    void registerServerGuiModule(ModuleRegistry registry);

    void uninstallGuiTab(ModuleServerBase moduleServerBase);
}

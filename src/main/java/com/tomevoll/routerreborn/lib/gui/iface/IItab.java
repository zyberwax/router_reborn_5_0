package com.tomevoll.routerreborn.lib.gui.iface;


import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;


public interface IItab {

    void MouseMove(int x, int y);

    void MouseClick(int x, int y, int button);

    boolean KeyPress(char c, int keyCode);

    void HideContent(ContainerServer c);

    void Show(ContainerServer c);

    void Draw();

    void DrawTooltip();

    void DrawBG();

    void ActionPerformed(GuiButton btn);

    int registerButtons(int startIndx, IGuiController gui);

    ItemStack GetItem();

    void initGui(IGuiController gui);

    String GetTooltip();

    void updateScreen();
}

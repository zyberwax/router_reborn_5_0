package com.tomevoll.routerreborn.lib.gui.modules.textinfo;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

public class TextInfoClient extends ModuleClientBase {

    String info = "";
    String ID = "textinfo";

    public TextInfoClient(ItemStack icon, String text, String ID) {
        super(icon);
        info = text;
        this.ID = ID;
        // TODO Auto-generated constructor stub
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub
        Minecraft.getMinecraft().fontRenderer.drawSplitString(info, Left + 8, Top + 28, 160, 0);
    }

    @Override
    public String getModuleID() {
        // TODO Auto-generated method stub
        return ID;
    }

    @Override
    protected void AddControls(IGuiController gui) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }

}

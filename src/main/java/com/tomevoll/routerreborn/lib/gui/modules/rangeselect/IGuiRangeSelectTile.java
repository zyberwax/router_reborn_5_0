package com.tomevoll.routerreborn.lib.gui.modules.rangeselect;

import net.minecraft.util.EnumFacing;

public interface IGuiRangeSelectTile {

    int getMaxRange();

    int getRange(EnumFacing facing);

    void setRange(EnumFacing facing, int range);

    int getOffsetY();

    void setOffsetY(int val);

    boolean getShowRange();

    void setShowRange(boolean val);


}

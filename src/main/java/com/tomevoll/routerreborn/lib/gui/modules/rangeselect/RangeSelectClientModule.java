package com.tomevoll.routerreborn.lib.gui.modules.rangeselect;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.GuiButtonFlat;
import com.tomevoll.routerreborn.lib.gui.control.GuiCheckBox;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;

public class RangeSelectClientModule extends ModuleClientBase {

    private GuiButtonFlat btnupN;
    private GuiButtonFlat btndownN;

    private GuiButtonFlat btnupS;
    private GuiButtonFlat btndownS;

    private GuiButtonFlat btnupW;
    private GuiButtonFlat btndownW;

    private GuiButtonFlat btnupE;
    private GuiButtonFlat btndownE;

    private GuiButtonFlat btnupY;
    private GuiButtonFlat btndownY;

    private GuiCheckBox checkShowRange;

    private int maxRange = 0;


    private int rangeN = 0;
    private int rangeS = 0;
    private int rangeW = 0;
    private int rangeE = 0;
    private int rangeO = 0;

    private int showRange = 0;

    private IGuiRangeSelectTile tile;

    public RangeSelectClientModule(IGuiRangeSelectTile tile, ItemStack icon) {
        super(icon);
        this.tile = tile;
        // TODO Auto-generated constructor stub
    }

    @Override
    public void ActionPerformed(GuiButton btn) {
        if (btn.id == btnupN.id) {
            if (rangeN < maxRange) {
                rangeN++;
                gui.getContainer().sendToServer(getModuleID(), 1, rangeN);
            }
            return;
        }
        if (btn.id == btndownN.id) {
            if (rangeN > -2) {
                rangeN--;
                gui.getContainer().sendToServer(getModuleID(), 1, rangeN);
            }
            return;
        }


        if (btn.id == btnupS.id) {
            if (rangeS < maxRange) {
                rangeS++;
                gui.getContainer().sendToServer(getModuleID(), 2, rangeS);
            }
            return;
        }
        if (btn.id == btndownS.id) {
            if (rangeS > -2) {
                rangeS--;
                gui.getContainer().sendToServer(getModuleID(), 2, rangeS);
            }
            return;
        }


        if (btn.id == btnupW.id) {
            if (rangeW < maxRange) {
                rangeW++;
                gui.getContainer().sendToServer(getModuleID(), 3, rangeW);
            }
            return;
        }
        if (btn.id == btndownW.id) {
            if (rangeW > -2) {
                rangeW--;
                gui.getContainer().sendToServer(getModuleID(), 3, rangeW);
            }
            return;
        }


        if (btn.id == btnupE.id) {
            if (rangeE < maxRange) {
                rangeE++;
                gui.getContainer().sendToServer(getModuleID(), 4, rangeE);
            }
            return;
        }
        if (btn.id == btndownE.id) {
            if (rangeE > -2) {
                rangeE--;
                gui.getContainer().sendToServer(getModuleID(), 4, rangeE);
            }
            return;
        }


        if (btn.id == btnupY.id) {
            if (rangeO < maxRange) {
                rangeO++;
                gui.getContainer().sendToServer(getModuleID(), 5, rangeO);
            }
            return;
        }
        if (btn.id == btndownY.id) {
            if (rangeO > 0 - maxRange) {
                rangeO--;
                gui.getContainer().sendToServer(getModuleID(), 5, rangeO);
            }
            return;
        }


        if (btn.id == checkShowRange.id) {
            if (showRange == 0) showRange = 1;
            else showRange = 0;
            checkShowRange.selected = showRange == 1;
            gui.getContainer().sendToServer(getModuleID(), 6, showRange);
        }


        //super.ActionPerformed(btn);
    }


    @Override
    public void handleMessageFromServer(ContainerServer containerServer,
                                        IGuiTile t, int cmd, String val1, int val2) {
        // TODO Auto-generated method stub
        switch (cmd) {
            case 0:
                maxRange = val2;

                break;
            case 1:
                rangeN = val2;
                tile.setRange(EnumFacing.NORTH, val2);
                break;
            case 2:
                rangeS = val2;
                tile.setRange(EnumFacing.SOUTH, val2);
                break;
            case 3:
                rangeW = val2;
                tile.setRange(EnumFacing.WEST, val2);
                break;
            case 4:
                rangeE = val2;
                tile.setRange(EnumFacing.EAST, val2);
                break;
            case 5:
                rangeO = val2;
                tile.setOffsetY(val2);
                break;
            case 6:
                showRange = val2;
                break;
            default:
                break;
        }
    }

    @Override
    public String getModuleID() {
        // TODO Auto-generated method stub
        return "rangeselect";
    }

    @Override
    protected void AddControls(IGuiController gui) {
        btnupN = new GuiButtonFlat(2, Left + 11, Top + 42, 10, 10, 4, 0);
        btndownN = new GuiButtonFlat(3, Left + 11, Top + 62, 10, 10, 5, 0);

        btnupS = new GuiButtonFlat(4, Left + 31, Top + 42, 10, 10, 4, 0);
        btndownS = new GuiButtonFlat(5, Left + 31, Top + 62, 10, 10, 5, 0);

        btnupW = new GuiButtonFlat(6, Left + 51, Top + 42, 10, 10, 4, 0);
        btndownW = new GuiButtonFlat(7, Left + 51, Top + 62, 10, 10, 5, 0);

        btnupE = new GuiButtonFlat(8, Left + 71, Top + 42, 10, 10, 4, 0);
        btndownE = new GuiButtonFlat(9, Left + 71, Top + 62, 10, 10, 5, 0);

        btnupY = new GuiButtonFlat(10, Left + 158, Top + 42, 10, 10, 4, 0);
        btndownY = new GuiButtonFlat(11, Left + 158, Top + 62, 10, 10, 5, 0);

        checkShowRange = new GuiCheckBox(12, Left + 10, Top + 76, 10, 10, 2, 1);

        AddButton(btnupN);
        AddButton(btndownN);
        AddButton(btnupE);
        AddButton(btndownE);
        AddButton(btnupS);
        AddButton(btndownS);
        AddButton(btnupW);
        AddButton(btndownW);
        AddButton(btnupY);
        AddButton(btndownY);
        AddButton(checkShowRange);
    }

    @Override
    public void DrawBG() {
        super.DrawBG();

        checkShowRange.displayString = "Show range";
        checkShowRange.selected = showRange == 1;

        tile.setShowRange(showRange == 1);
        gui.getMC().fontRenderer.drawString("Range Select", Left + 11, Top + 22, 4210752);

        gui.getMC().fontRenderer.drawString("N", Left + 13, Top + 33, 4210752);
        gui.getMC().fontRenderer.drawString("" + rangeN, Left + 11, Top + 53, 4210752);

        gui.getMC().fontRenderer.drawString("S", Left + 33, Top + 33, 4210752);
        gui.getMC().fontRenderer.drawString("" + rangeS, Left + 31, Top + 53, 4210752);

        gui.getMC().fontRenderer.drawString("W", Left + 53, Top + 33, 4210752);
        gui.getMC().fontRenderer.drawString("" + rangeW, Left + 51, Top + 53, 4210752);

        gui.getMC().fontRenderer.drawString("E", Left + 73, Top + 33, 4210752);
        gui.getMC().fontRenderer.drawString("" + rangeE, Left + 71, Top + 53, 4210752);


        gui.getMC().fontRenderer.drawString("Level offset", Left + 108, Top + 33, 4210752);
        gui.getMC().fontRenderer.drawString("" + rangeO, Left + 158, Top + 53, 4210752);
    }


    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }

}

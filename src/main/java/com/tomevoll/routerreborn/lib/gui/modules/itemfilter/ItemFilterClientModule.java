package com.tomevoll.routerreborn.lib.gui.modules.itemfilter;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.GuiCheckBox;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotGhostMulti;
import com.tomevoll.routerreborn.lib.inventory.ItemFilter;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ItemFilterClientModule extends ModuleClientBase {
    private IGuiItemFilterTile tile;
    private boolean isKeepStock = false;
    private boolean useMeta = false;
    private boolean useOreDict = false;
    private boolean whiteList = false;
    private boolean showKeepInStock = false;
    private ItemFilter filter;
    private GuiCheckBox btnwhitelist;
    private GuiCheckBox btnMetadata;
    private GuiCheckBox btnOreDict;
    private GuiCheckBox btnKeepStock;
    private GuiCheckBox btnItemFilter;

    public ItemFilterClientModule(ItemStack icon, IGuiItemFilterTile tile, ItemFilter filter, boolean showKeepInStock) {
        super(icon);
        this.tile = tile;
        this.filter = filter;
        this.showKeepInStock = showKeepInStock;
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        switch (cmd) {
            case 1:
                isKeepStock = val2 == 1;
                break;

            case 2:
                useMeta = val2 == 1;
                break;

            case 3:
                useOreDict = val2 == 1;
                break;

            case 4:
                whiteList = val2 == 1;
                break;
            default:
                break;
        }
    }

    @Override
    public void ActionPerformed(GuiButton btn) {

        if (btn.id == btnItemFilter.id) {
            container.sendToServer(getModuleID(), 1, btnItemFilter.selected ? 1 : 0);
            btnItemFilter.selected = !btnItemFilter.selected;
        }
        if (btn.id == btnKeepStock.id) {
            container.sendToServer(getModuleID(), 1, btnKeepStock.selected ? 0 : 1);
            btnKeepStock.selected = !btnKeepStock.selected;
        }
        if (btn.id == btnMetadata.id) {
            container.sendToServer(getModuleID(), 2, btnMetadata.selected ? 0 : 1);
            btnMetadata.selected = !btnMetadata.selected;
        }
        if (btn.id == btnOreDict.id) {
            container.sendToServer(getModuleID(), 3, btnOreDict.selected ? 0 : 1);
            btnOreDict.selected = !btnOreDict.selected;
        }
        if (btn.id == btnwhitelist.id) {
            container.sendToServer(getModuleID(), 4, btnwhitelist.selected ? 0 : 1);
            btnwhitelist.selected = !btnwhitelist.selected;
        }

    }

    @Override
    public String getModuleID() {
        return "itemfilter";
    }

    @Override
    public String GetTooltip() {
        return "Item Filter";
    }

    @Override
    public void DrawBG() {

        btnwhitelist.selected = !whiteList; // "Blacklist" : "Whitelist";
        btnwhitelist.displayString = "Blacklist";

        btnMetadata.selected = useMeta; // "Ignore Metadata" : "Use Metadata";
        btnMetadata.displayString = "Use Meta";

        btnOreDict.selected = useOreDict; // "Ignore Metadata" : "Use Metadata";
        btnOreDict.displayString = "Use OreDictionary";

        btnItemFilter.selected = !isKeepStock;
        btnItemFilter.displayString = "Item Filter";

        btnKeepStock.displayString = "Keep in stock";
        btnKeepStock.selected = isKeepStock;

        if (showKeepInStock) {
            if (btnKeepStock.selected) {
                btnOreDict.enabled = false;
                btnwhitelist.enabled = false;
                // btnOreDict.selected = false;
                // btnwhitelist.selected = false;
                btnMetadata.enabled = false;
                // btnMetadata.selected = false;
            } else {
                btnOreDict.enabled = true;
                btnwhitelist.enabled = true;
                btnMetadata.enabled = true;
            }
        }
        if (btnMetadata.selected) {
            btnOreDict.enabled = false;
            // btnOreDict.selected = false;
        } else {
            if (!btnKeepStock.selected)
                btnOreDict.enabled = true;
        }
        if (btnOreDict.selected) {
            btnMetadata.enabled = false;
            // btnMetadata.selected = false;
        } else {
            if (!btnKeepStock.selected)
                btnMetadata.enabled = true;
        }

        if (!showKeepInStock) {
            btnItemFilter.visible = false;
            btnKeepStock.visible = false;
        }

        GlStateManager.pushMatrix();

        if (showKeepInStock) {
            gui.drawFilledRect(7, 26, 64, 22, 0xff7d8c9d, 0xff7d8c9d);
            gui.drawFilledRect(8, 27, 62, 20, 0xffa6a6a6, 0xffa6a6a6);
        } else {
            gui.drawFilledRect(82, 26, 87, 22, 0xff7d8c9d, 0xff7d8c9d);
            gui.drawFilledRect(83, 27, 85, 20, 0xffa6a6a6, 0xffa6a6a6);
        }
        GlStateManager.popMatrix();

        super.DrawBG();
    }

    @Override
    protected void AddControls(IGuiController gui) {
        int Topp = Top + 3;

        btnwhitelist = new GuiCheckBox(2, Left + 129, Topp + 26, 10, 10, 7, 0);
        btnMetadata = new GuiCheckBox(1, Left + 85, Topp + 26, 10, 10, 2, 1);
        btnOreDict = new GuiCheckBox(3, Left + 85, Topp + 36, 10, 10, 2, 1);

        btnItemFilter = new GuiCheckBox(4, Left + 10, Topp + 26, 10, 10, 2, 1);
        btnKeepStock = new GuiCheckBox(5, Left + 10, Topp + 36, 10, 10, 2, 1);

        AddButton(btnwhitelist);
        AddButton(btnMetadata);
        AddButton(btnOreDict);

        AddButton(btnItemFilter);
        AddButton(btnKeepStock);

    }


    @Override
    protected void registerSlots() {

        int X = 8;
        for (int i = 0; i < 9; i++) {
            addSlot(new GuiSlotGhostMulti(this, i, Left + X + (i * 18), Top + 55));
        }
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        return filter.getSlot(slotIndex); // tile.getItemFilterSlot(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        filter.setSlot(slotIndex, stack);
        // tile.SetItemFilterSlot(slotIndex, stack);
    }

    public boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        boolean flag = false;
        int i = startIndex;
        reverseDirection = false;
        if (reverseDirection) {
            i = slots.size() - 1;
        }
        if (stack.getCount() > 0) {
            for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > 0
                    : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
                Slot slot = slots.get(i).getSlot();
                ItemStack itemstack = slot.getStack();

                if (itemstack.isEmpty() && !stack.isEmpty()) // Forge:
                {
                    ItemStack st = stack.copy();
                    st.setCount(1);
                    slot.putStack(st);
                    slot.onSlotChanged();
                    // stack.stackSize = 0;
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }

}

package com.tomevoll.routerreborn.lib.gui.modules.nineslotonetwo;

import net.minecraft.item.ItemStack;

public interface INineOneTwoTile {

    void UpdateProcessing(NineOneTwoServerModule eightenSlotServerModule);

    ItemStack getGuiSlot(int slotIndex);

    void setGuiSlot(int slotIndex, ItemStack stack);

}

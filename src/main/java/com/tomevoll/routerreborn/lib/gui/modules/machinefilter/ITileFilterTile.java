package com.tomevoll.routerreborn.lib.gui.modules.machinefilter;

import com.tomevoll.routerreborn.lib.inventory.InfoTile;

import java.util.List;


public interface ITileFilterTile {

    void doRescan();

    List<InfoTile> getUnfilteredList();
}

package com.tomevoll.routerreborn.lib.gui.modules.processing;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotOutput;
import net.minecraft.item.ItemStack;

public class ProcessingServerModule extends ModuleServerBase {

    IGuiProcessingTile tile;
    private int storedEnergy = 0;
    private int maxEnergy = 0;
    private int burnTime = 0;
    private int totalBurnTime = 0;
    private boolean useEnergy = false;
    private int processTimeTotal = 0;
    private int processTime = 0;
    private int storedEnergy2 = 0;
    private int maxEnergy2 = 0;
    private int burnTime2 = 0;
    private int totalBurnTime2 = 0;
    private boolean useEnergy2 = false;
    private int processTimeTotal2 = 0;
    private int processTime2 = 0;

    public ProcessingServerModule(IGuiProcessingTile tile, boolean useEnergy) {
        super();
        this.tile = tile;
        this.useEnergy = useEnergy;
    }


    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
        tile.UpdateProcessing(this);

        if (storedEnergy != storedEnergy2)
            container.sendToClient(getModuleID(), 1, storedEnergy);
        if (maxEnergy != maxEnergy2)
            container.sendToClient(getModuleID(), 2, maxEnergy);
        if (burnTime != burnTime2)
            container.sendToClient(getModuleID(), 3, burnTime);
        if (totalBurnTime != totalBurnTime2)
            container.sendToClient(getModuleID(), 4, totalBurnTime);
        if (useEnergy != useEnergy2)
            container.sendToClient(getModuleID(), 5, useEnergy ? 1 : 0);
        if (processTime != processTime2)
            container.sendToClient(getModuleID(), 6, processTime);
        if (processTimeTotal != processTimeTotal2)
            container.sendToClient(getModuleID(), 7, processTimeTotal);

        storedEnergy2 = storedEnergy;
        maxEnergy2 = maxEnergy;
        burnTime2 = burnTime;
        totalBurnTime2 = totalBurnTime;
        useEnergy2 = useEnergy;
        processTime2 = processTime;
        processTimeTotal2 = processTimeTotal;
    }

    @Override
    public String getModuleID() {
        return "processing";
    }

    @Override
    protected void registerSlots() {
        addSlot(new GuiSlot(this, 0, 0, 0));
        addSlot(new GuiSlotOutput(container.player, this, 2, 0, 0));
        if (!useEnergy) addSlot(new GuiSlot(this, 1, 0, 0));
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        return tile.getProcessingSlot(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        tile.setProcessingSlot(slotIndex, stack);
    }

    @Override
    public float getSmeltingExperience(ItemStack stack) {
        return tile.getProcessingXP(stack);
    }


    public int getStoredEnergy() {
        return storedEnergy;
    }


    public void setStoredEnergy(int storedEnergy) {
        this.storedEnergy = storedEnergy;
    }


    public int getMaxEnergy() {
        return maxEnergy;
    }


    public void setMaxEnergy(int maxEnergy) {
        this.maxEnergy = maxEnergy;
    }


    public float getBurnTime() {
        return burnTime;
    }


    public void setBurnTime(int burnTime) {
        this.burnTime = burnTime;
    }


    public float getTotalBurnTime() {
        return totalBurnTime;
    }


    public void setTotalBurnTime(int totalBurnTime) {
        this.totalBurnTime = totalBurnTime;
    }


    public boolean isUseEnergy() {
        return useEnergy;
    }


    public int getProcessTimeTotal() {
        return processTimeTotal;
    }

    public void setProcessTimeTotal(int processTimeTotal) {
        this.processTimeTotal = processTimeTotal;
    }

    public int getProcessTime() {
        return processTime;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }
}

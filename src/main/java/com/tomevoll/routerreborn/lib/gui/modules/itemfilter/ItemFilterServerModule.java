package com.tomevoll.routerreborn.lib.gui.modules.itemfilter;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotGhostMulti;
import com.tomevoll.routerreborn.lib.inventory.ItemFilter;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public class ItemFilterServerModule extends ModuleServerBase {
    private IGuiItemFilterTile tile;
    private boolean isKeepStock = false;
    private boolean useMeta = false;
    private boolean useOreDict = false;
    private boolean whiteList = false;


    private ItemFilter filter;
    private boolean hasSentChanges = false;

    public ItemFilterServerModule(IGuiItemFilterTile tile, ItemFilter filter) {
        //super();
        this.tile = tile;
        this.filter = filter;


    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        switch (cmd) {
            case 1:
                filter.setKeepStock(val2 == 1);
                break;

            case 2:
                filter.setUseMeta(val2 == 1);
                break;

            case 3:
                filter.setUseOreDict(val2 == 1);
                break;

            case 4:
                filter.setWhiteList(val2 != 1);
                break;
            default:
                break;
        }

        ((TileEntity) tile).markDirty();

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {

        if (filter.getKeepStock() != isKeepStock || !hasSentChanges)
            container.sendToClient(getModuleID(), 1, filter.getKeepStock() ? 1 : 0);

        if (filter.getUseMeta() != useMeta || !hasSentChanges)
            container.sendToClient(getModuleID(), 2, filter.getUseMeta() ? 1 : 0);

        if (filter.getUseOreDict() != useOreDict || !hasSentChanges)
            container.sendToClient(getModuleID(), 3, filter.getUseOreDict() ? 1 : 0);

        if (filter.getWhiteList() != whiteList || !hasSentChanges)
            container.sendToClient(getModuleID(), 4, filter.getWhiteList() ? 1 : 0);


        isKeepStock = filter.getKeepStock();
        useMeta = filter.getUseMeta();
        useOreDict = filter.getUseOreDict();
        whiteList = filter.getWhiteList();
        hasSentChanges = true;
    }

    @Override
    public String getModuleID() {
        return "itemfilter";
    }


    @Override
    protected void registerSlots() {
        for (int i = 0; i < 9; i++) {
            addSlot(new GuiSlotGhostMulti(this, i, 0, 0));
        }
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        return filter.getSlot(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        filter.setSlot(slotIndex, stack);
        ((TileEntity) tile).markDirty();
    }

    public boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        boolean flag = false;
        int i = startIndex;
        reverseDirection = false;
        if (reverseDirection) {
            i = slots.size() - 1;
        }
        if (stack.getCount() > 0) {
            for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > 0
                    : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
                Slot slot = slots.get(i).getSlot();
                ItemStack itemstack = slot.getStack();

                if (itemstack.isEmpty() && !stack.isEmpty()) // Forge:
                {
                    ItemStack st = stack.copy();
                    st.setCount(1);
                    slot.putStack(st);
                    slot.onSlotChanged();
                    //stack.stackSize = 0;
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }
}

package com.tomevoll.routerreborn.lib.gui.modules.redstone;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;

public class RedstoneServerModule extends ModuleServerBase {

    int redStoneMode = -1;

    @Override
    public String getModuleID() {
        return "redstone";
    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        if (cmd == 1) {
            if (t instanceof IGuiRedstoneTile) {
                IGuiRedstoneTile r = (IGuiRedstoneTile) t;
                r.setRedstoneMode(val2);
            }
        }
    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {

        if (t instanceof IGuiRedstoneTile) {
            IGuiRedstoneTile r = (IGuiRedstoneTile) t;
            int val = r.getRedstoneMode();
            if (val != redStoneMode)
                container.sendToClient(getModuleID(), 1, val);

            this.redStoneMode = val;
        }
    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }


}

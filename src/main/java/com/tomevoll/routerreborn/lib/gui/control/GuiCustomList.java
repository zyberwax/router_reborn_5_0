package com.tomevoll.routerreborn.lib.gui.control;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GuiCustomList extends GuiButton implements IToolTip {

    public boolean canScollUp = false;
    public boolean canScrollDown = false;
    public ArrayList<ListItem> lst = new ArrayList<ListItem>();
    float scale = 1f;
    private int mx = 0, my = 0;
    private int selectedIndex = -1;
    private int startindex = 0;
    private boolean selected = false;
    private boolean hovering = false;

    public GuiCustomList(int id, int x, int y, int width, int height, String text) {
        super(id, x, y, width, height, text);
    }

    public static void drawRect(int left, int top, int right, int bottom, float f, float f1, float f2, float f3) {
        if (left < right) {
            int i = left;
            left = right;
            right = i;
        }

        if (top < bottom) {
            int j = top;
            top = bottom;
            bottom = j;
        }

        //  float f3 = (float)(color >> 24 & 255) / 255.0F;
        // float f = (float)(color >> 16 & 255) / 255.0F;
        //float f1 = (float)(color >> 8 & 255) / 255.0F;
        //float f2 = (float)(color & 255) / 255.0F;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();
        //GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        //GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(f, f1, f2, f3);
        GL11.glColor4f(f, f1, f2, f3);
        worldrenderer.begin(7, DefaultVertexFormats.POSITION);
        worldrenderer.pos((double) left, (double) bottom, 0.0D).endVertex();
        worldrenderer.pos((double) right, (double) bottom, 0.0D).endVertex();
        worldrenderer.pos((double) right, (double) top, 0.0D).endVertex();
        worldrenderer.pos((double) left, (double) top, 0.0D).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        //GlStateManager.disableBlend();
    }

    public void scrollDown(Minecraft mc) {
        int maxItm = this.height / mc.fontRenderer.FONT_HEIGHT;

        if (maxItm < lst.size()) {
            if (lst.size() - maxItm > 0) {
                if (startindex < lst.size() - maxItm)
                    startindex++;
            }
        }
    }

    public void scrollUp() {
        if (startindex > 0)
            startindex--;
    }

    public void selectFromText(String txt) {
        for (int i = 0; i < lst.size(); i++) {
            if (txt.equalsIgnoreCase(lst.get(i).realname)) {
                selectedIndex = i;
                break;
            }
        }
    }

    public void setselectedIndex(int index) {
        if (index < lst.size() && index >= 0) {
            this.selectedIndex = index;
        }
    }

    public int getselectedindex() {
        return this.selectedIndex;
    }

    public boolean Clicked(Minecraft mc) {

        //boolean kk = mx >= this.xPosition && my >= this.yPosition && mx < this.xPosition + this.width && my < this.yPosition + this.height;


        for (int i = startindex; i < lst.size(); i++) {
            FontRenderer fontrenderer = mc.fontRenderer;
            boolean k = mx >= this.y && my >= this.y + ((i - startindex) * (fontrenderer.FONT_HEIGHT * scale)) && mx < this.x + this.width && my < this.y + ((i - startindex) * (fontrenderer.FONT_HEIGHT * scale)) + (fontrenderer.FONT_HEIGHT * scale);
            if (k) {

                if (i != selectedIndex) {
                    selectedIndex = i;
                    return true;
                } else {
                    selectedIndex = i;
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public void drawButton(Minecraft mc, int x, int y, float t) {

        this.hovering = false;
        if (this.visible) {

            int xpos = this.x;
            int ypos = this.y;

            int width = this.width;
            int height = this.height;

            float scale = 0.5f;

            float multiplier = xpos * scale;
            multiplier = xpos / multiplier;
            multiplier = multiplier - 1;

            width = (int) (width + (width * multiplier));
            height = (int) (height + (height * multiplier));

            xpos = (int) (xpos + (xpos * multiplier));
            ypos = (int) (ypos + (ypos * multiplier));

            mx = x;
            my = y;
            FontRenderer fontrenderer = mc.fontRenderer;

            // mc.getTextureManager().bindTexture(CustomTextures);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

            this.mouseDragged(mc, x, y);
            GL11.glPushMatrix();
            // GL11.glPushMatrix();
            // GL11.glScalef(0.5f, 0.5f, 0.5f);
            drawRect(this.x, this.y, this.x + this.width, this.y + this.height, 0.0f, 0.0f, 0.0f, 1f);


            GL11.glScalef(0.5f, 0.5f, 0.5f);

            drawRect(xpos - 1, ypos - 1, xpos + 1 + width, ypos, 0.3f, 0.3f, 0.3f, 1f);

            drawRect(xpos - 1, ypos + height, xpos + 1 + width, ypos + height + 1, 1f, 1f, 1f, 1f);

            drawRect(xpos - 1, ypos - 1, xpos, ypos + height, 0.3f, 0.3f, 0.3f, 1f);

            drawRect(xpos + width, ypos - 1, xpos + width + 1, ypos + height, 1f, 1f, 1f, 1f);
            drawRect(xpos - 1, ypos + height, xpos, ypos + height + 1, 0.6f, 0.6f, 0.6f, 1f);
            drawRect(xpos + width, ypos - 1, xpos + 1 + width, ypos, 0.6f, 0.6f, 0.6f, 1f);

            GL11.glPopMatrix();
            canScollUp = startindex > 0;

            for (int i = startindex; i < lst.size(); i++) {
                //GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                GL11.glPushMatrix();
                if (i == selectedIndex)
                    drawRect(this.x, this.y + ((i - startindex) * (fontrenderer.FONT_HEIGHT)), this.x + (this.width), this.y + ((i - startindex) * fontrenderer.FONT_HEIGHT) + fontrenderer.FONT_HEIGHT, 0.3f, 0.4f, 0.6f, 1f);


                //GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                GL11.glPopMatrix();
                //GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                this.hovered = x >= this.x && y >= this.y + ((i - startindex) * fontrenderer.FONT_HEIGHT) && x < this.x + this.width && y < this.y + ((i - startindex) * fontrenderer.FONT_HEIGHT) + fontrenderer.FONT_HEIGHT;
                int k = this.getHoverState(this.hovered);

                int l = 14737632;

                //if (packedFGColour != 0) {
                //l = packedFGColour;

                //} else
                if (!this.enabled) {
                    l = 10526880;
                } else if (this.hovered) {
                    l = 16777120;
                }
                //l = 14737632;
                List txt = fontrenderer.listFormattedStringToWidth(lst.get(i).text, this.width - 2);

                fontrenderer.drawString((String) txt.get(0), this.x + 2, this.y + 1 + ((i - startindex) * fontrenderer.FONT_HEIGHT), l);

                //if (this.yPosition + (((i - startindex) + 1) * fontrenderer.FONT_HEIGHT) + fontrenderer.FONT_HEIGHT > this.yPosition + this.height) {
                int maxItm = this.height / mc.fontRenderer.FONT_HEIGHT;
                //break;
//}
                canScrollDown = startindex < lst.size() - maxItm;
                if (this.y + (((i - startindex) + 1) * fontrenderer.FONT_HEIGHT) + fontrenderer.FONT_HEIGHT > this.y + this.height)
                    break;

            }

            this.hovered = x >= this.x && y >= this.y && x < this.x + this.width && y < this.y + this.height;
            int k = this.getHoverState(this.hovered);

            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

            this.hovering = k == 2;
        }

    }

    public void DrawTooltips(Minecraft mc) {
        if (this.hovering) {
            if (this.displayString.length() > 0)
                this.drawHoveringText(Arrays.asList(this.displayString), this.mx, this.my, mc.fontRenderer);
        }
    }

    protected void drawHoveringText(List p_146283_1_, int p_146283_2_, int p_146283_3_, FontRenderer font) {
        if (!p_146283_1_.isEmpty()) {
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_DEPTH_TEST);

            int k = 0;
            Iterator iterator = p_146283_1_.iterator();

            while (iterator.hasNext()) {
                String s = (String) iterator.next();
                int l = font.getStringWidth(s);

                if (l > k) {
                    k = l;
                }
            }

            int j2 = p_146283_2_ + 12;
            int k2 = p_146283_3_ - 12;
            int i1 = 8;

            if (p_146283_1_.size() > 1) {
                i1 += 2 + (p_146283_1_.size() - 1) * 10;
            }

            this.zLevel = 200.0F;

            int j1 = -267386864;
            this.drawGradientRect(j2 - 3, k2 - 4, j2 + k + 3, k2 - 3, j1, j1);
            this.drawGradientRect(j2 - 3, k2 + i1 + 3, j2 + k + 3, k2 + i1 + 4, j1, j1);
            this.drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 + i1 + 3, j1, j1);
            this.drawGradientRect(j2 - 4, k2 - 3, j2 - 3, k2 + i1 + 3, j1, j1);
            this.drawGradientRect(j2 + k + 3, k2 - 3, j2 + k + 4, k2 + i1 + 3, j1, j1);
            int k1 = 1347420415;
            int l1 = (k1 & 16711422) >> 1 | k1 & -16777216;
            this.drawGradientRect(j2 - 3, k2 - 3 + 1, j2 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
            this.drawGradientRect(j2 + k + 2, k2 - 3 + 1, j2 + k + 3, k2 + i1 + 3 - 1, k1, l1);
            this.drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 - 3 + 1, k1, k1);
            this.drawGradientRect(j2 - 3, k2 + i1 + 2, j2 + k + 3, k2 + i1 + 3, l1, l1);

            for (int i2 = 0; i2 < p_146283_1_.size(); ++i2) {
                String s1 = (String) p_146283_1_.get(i2);
                font.drawStringWithShadow(s1, j2, k2, -1);

                if (i2 == 0) {
                    k2 += 2;
                }

                k2 += 10;
            }

            this.zLevel = 0.0F;
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            RenderHelper.enableStandardItemLighting();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        }
    }

    public class ListItem {
        public String realname;
        String text;

        public ListItem(String text, String RealName) {
            this.realname = RealName;
            this.text = text;
        }
    }

}

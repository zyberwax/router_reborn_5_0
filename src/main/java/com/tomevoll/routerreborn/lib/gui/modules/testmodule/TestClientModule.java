package com.tomevoll.routerreborn.lib.gui.modules.testmodule;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotGhost;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class TestClientModule extends ModuleClientBase {

    ItemStack stack = null;


    public TestClientModule() {
        super(new ItemStack(Blocks.FURNACE));
        // TODO Auto-generated constructor stub
    }

    @Override
    public String getModuleID() {
        // TODO Auto-generated method stub
        return "test";
    }

    @Override
    protected void AddControls(IGuiController gui) {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        // TODO Auto-generated method stub

    }

    @Override
    public ItemStack getStack(int slotIndex) {
        if (slotIndex == 0)
            return stack;
        return null;
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        if (slotIndex == 0)
            this.stack = stack;

    }

    @Override
    public int getStackLimit() {
        // TODO Auto-generated method stub
        return 64;
    }

    @Override
    protected void registerSlots() {
        addSlot(new GuiSlotGhost(this, 0, Left + 10, Top + 30));

    }


    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }


}

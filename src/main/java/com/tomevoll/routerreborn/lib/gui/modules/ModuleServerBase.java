package com.tomevoll.routerreborn.lib.gui.modules;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.iface.IServerGuiModule;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public abstract class ModuleServerBase implements IServerGuiModule {

    protected ContainerServer container;
    protected List<Slots> slots = new ArrayList<Slots>();

    @Override
    public ItemStack getStack(int slotIndex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getStackLimit() {
        // TODO Auto-generated method stub
        return 64;
    }

    public void addSlots(ContainerServer c) {
        this.container = c;
        registerSlots();
    }

    protected abstract void registerSlots();

    protected void addSlot(GuiSlot slot) {
        GuiSlot s = container.addSlot(slot);
        slots.add(new Slots(s.slotNumber, s.getSlotIndex(), s, s.xPos, s.yPos));
        s.yPos = -50000; // hide it, original position stored in the
        // list
    }

    protected Slots getSlotByContainerIndex(int index) {
        for (int i = 0; i < slots.size(); i++) {
            if (slots.get(i).containerIndex == index)
                return slots.get(i);
        }
        return null;
    }

    protected Slots getSlotByModuleIndex(int index) {
        for (int i = 0; i < slots.size(); i++) {
            if (slots.get(i).moduleIndex == index)
                return slots.get(i);
        }
        return null;
    }

    @Override
    public float getSmeltingExperience(ItemStack stack) {
        return 0;
    }

    // Only called for transfers to the module, never when transfer from the
    // module, start and end index should be ignored
    public boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        boolean flag = false;
        int i = startIndex;

        if (reverseDirection) {
            i = slots.size() - 1;
        }

        for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > -1
                : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
            Slot slot = slots.get(i).slot;
            ItemStack itemstack = slot.getStack();

            if (stack.isStackable()) {
                if (itemstack != null && ContainerServer.areItemStacksEqual(stack, itemstack) && slot.isItemValid(stack)) // slot
                {
                    int j = itemstack.getCount() + stack.getCount();
                    int maxSize = Math.min(slot.getSlotStackLimit(), stack.getMaxStackSize());

                    if (j <= maxSize) {
                        stack.setCount(0);
                        itemstack.setCount(j);
                        slot.onSlotChanged();
                        flag = true;
                    } else if (itemstack.getCount() < maxSize) {
                        stack.shrink(maxSize - itemstack.getCount());
                        itemstack.setCount(maxSize);
                        slot.onSlotChanged();
                        flag = true;
                    }
                }
            }
        }

        if (stack.getCount() > 0) {
            for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > -1
                    : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
                Slot slot = slots.get(i).slot;
                ItemStack itemstack = slot.getStack();

                if (itemstack == null && slot.isItemValid(stack)) // Forge:
                {
                    slot.putStack(stack.copy());
                    slot.onSlotChanged();
                    stack.setCount(0);
                    flag = true;
                    break;
                }
            }
        }

        return flag;
    }

    public void uninstallModule() {
        container.t.uninstallGuiTab(this);
    }

    public class Slots {
        private int containerIndex;
        private int moduleIndex;
        private GuiSlot slot;
        private int yPos;
        private int xPos;

        public Slots(int ContainerIndex, int ModuleIndex, GuiSlot s, int xPos, int yPos) {
            setContainerIndex(ContainerIndex);
            setModuleIndex(ModuleIndex);
            setSlot(s);
            setxPos(xPos);
            setyPos(yPos);
        }

        int getContainerIndex() {
            return containerIndex;

        }

        void setContainerIndex(int containerIndex) {
            this.containerIndex = containerIndex;
        }

        int getModuleIndex() {
            return moduleIndex;
        }

        void setModuleIndex(int moduleIndex) {
            this.moduleIndex = moduleIndex;
        }

        public GuiSlot getSlot() {
            return slot;
        }

        void setSlot(GuiSlot slot) {
            this.slot = slot;
        }

        private int getyPos() {
            return yPos;
        }

        private void setyPos(int yPos) {
            this.yPos = yPos;
        }

        private int getxPos() {
            return xPos;
        }

        private void setxPos(int xPos) {
            this.xPos = xPos;
        }
    }

}

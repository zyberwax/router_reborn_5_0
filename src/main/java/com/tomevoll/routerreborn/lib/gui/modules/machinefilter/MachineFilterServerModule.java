package com.tomevoll.routerreborn.lib.gui.modules.machinefilter;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.inventory.InfoTile;
import com.tomevoll.routerreborn.lib.inventory.TileFilter;
import net.minecraft.tileentity.TileEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Zyber on 17.12.2016.
 */
public class MachineFilterServerModule extends ModuleServerBase {

    ITileFilterTile tile;
    TileFilter filter;
    TileFilter _filter;

    Set<String> unfiltered;


    public MachineFilterServerModule(ITileFilterTile tile, TileFilter filter) {
        this.tile = tile;
        this.filter = filter;
    }


    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {

        if (cmd == 1) {
            filter.filterList.clear();
            String[] l = val1.split(":");
            for (String rName : l) {
                if (!rName.equalsIgnoreCase(""))
                    filter.filterList.add(rName);
            }
            tile.doRescan();
        }
        if (cmd == 2) {

            filter.BlackList = val2 == 1;
            tile.doRescan();
        }
        if (cmd == 3) {
            filter.VisitNear = val2 == 1;
            tile.doRescan();
        }


        ((TileEntity) tile).markDirty();
    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {

        if (unfiltered == null) {
            unfiltered = new HashSet<String>();

            for (InfoTile ti : tile.getUnfilteredList()) {
                unfiltered.add(ti.invName);
            }

            String fullList = "";
            for (String un : unfiltered) {
                fullList += un + ":";
            }

            container.sendToClient(getModuleID(), 1, fullList);

        }

        if (!isFilterEqual(filter, _filter)) {
            String fullList = "";
            fullList = "";
            for (String fil : filter.filterList) {
                fullList += fil + ":";
            }

//    		System.out.println("Change detected in TileFilter");
            container.sendToClient(getModuleID(), 2, fullList);

            container.sendToClient(getModuleID(), 4, filter.BlackList ? 1 : 0);
            container.sendToClient(getModuleID(), 3, filter.VisitNear ? 1 : 0);

            _filter = filter.Copy();
        }
    }

    private boolean isFilterEqual(TileFilter fil, TileFilter _fil) {

        if (_fil == null) return false;

        if (fil.BlackList != _fil.BlackList) return false;
        if (fil.VisitNear != _fil.VisitNear) return false;
        if (fil.filterList != null && _fil.filterList != null) {
            if (fil.filterList.size() != _fil.filterList.size()) return false;
            for (int i = 0; i < fil.filterList.size(); i++) {
                if (!fil.filterList.get(i).equalsIgnoreCase(_fil.filterList.get(i))) return false;
            }
        } else {
            if (fil.filterList == null && _fil.filterList != null) return false;
            if (_fil.filterList == null && fil.filterList != null) return false;

        }


        return true;
    }


    @Override
    public String getModuleID() {
        return "machinefilter";
    }

    @Override
    protected void registerSlots() {

    }
}

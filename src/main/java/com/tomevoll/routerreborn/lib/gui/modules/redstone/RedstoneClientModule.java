package com.tomevoll.routerreborn.lib.gui.modules.redstone;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.GuiButtonFlat;
import com.tomevoll.routerreborn.lib.gui.control.GuiCustomList;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class RedstoneClientModule extends ModuleClientBase {
    IGuiRedstoneTile tile;
    int redstoneMode = -1;
    private GuiCustomList lst;
    private GuiButtonFlat btnup;
    private GuiButtonFlat btndown;

    public RedstoneClientModule(IGuiRedstoneTile tile) {
        super(new ItemStack(Items.REDSTONE));
        this.tile = tile;
    }

    @Override
    public String getModuleID() {
        return "redstone";
    }

    @Override
    public String GetTooltip() {
        return "Redstone Mode";
    }


    public void AddRedstoneMode(String displayName, int id) {
        lst.lst.add(lst.new ListItem(displayName, "" + id));
    }


    @Override
    public void ActionPerformed(GuiButton btn) {
        if (btn.id == lst.id) {
            lst.Clicked(gui.getMC());
            gui.getContainer().sendToServer(getModuleID(), 1, lst.getselectedindex());
            return;
        }
        if (btn.id == btndown.id) {
            lst.scrollDown(gui.getMC());
            return;
        }
        if (btn.id == btnup.id) {
            lst.scrollUp();
            return;
        }

        super.ActionPerformed(btn);
    }


    @Override
    protected void AddControls(IGuiController gui) {

        this.lst = new GuiCustomList(1, Left + 10, Top + 27, 120, 50, "");

        tile.RegisterRedstoneModes(this);

        btnup = new GuiButtonFlat(2, Left + 131, Top + 27, 10, 10, 4, 0);
        btndown = new GuiButtonFlat(3, Left + 131, Top + 67, 10, 10, 5, 0);

        AddButton(lst);
        AddButton(btnup);
        AddButton(btndown);
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        if (cmd == 1) {
            redstoneMode = val2;
            //if(val2 != lst.getselectedindex())
            lst.setselectedIndex(redstoneMode);

        }

    }

    @Override
    public void DrawBG() {
        lst.setselectedIndex(redstoneMode);
        btnup.enabled = lst.canScollUp;
        btndown.enabled = lst.canScrollDown;
    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }
}

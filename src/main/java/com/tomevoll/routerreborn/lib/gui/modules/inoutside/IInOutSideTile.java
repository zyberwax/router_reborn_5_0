package com.tomevoll.routerreborn.lib.gui.modules.inoutside;

import net.minecraft.item.ItemStack;

public interface IInOutSideTile {
    ItemStack getInOutSlotStack();

    void setInOutSlotStack(ItemStack stack);

    int getInOutSide();

    void setInOutSide(int side);

    int getInOutMode();

    void setInOutMode(int mode);

    int getInOutSlot();

    void setInOutSlot(int s);

    boolean getIsSlotMode();

    void setIsSlotMode(boolean slotMode);
}

package com.tomevoll.routerreborn.lib.gui.modules.testmodule;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.modules.processing.IGuiProcessingTile;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotGhost;
import net.minecraft.item.ItemStack;

public class TestServerModule extends ModuleServerBase {

    ItemStack stack = null;

    @Override
    public String getModuleID() {
        return "test";
    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        System.out.println("RECEIVED COMMAND: " + cmd + " FROM CLIENT, VAR1='" + val1 + "' VAR2='" + val2 + "'");
    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        if (container.t instanceof IGuiProcessingTile) {
            IGuiProcessingTile tile = (IGuiProcessingTile) container.t;
            return tile.getProcessingSlot(slotIndex);
        }

        return null;
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        if (container.t instanceof IGuiProcessingTile) {
            IGuiProcessingTile tile = (IGuiProcessingTile) container.t;
            tile.setProcessingSlot(slotIndex, stack);
        }
    }

    @Override
    public int getStackLimit() {
        return 64;
    }

    @Override
    protected void registerSlots() {
        addSlot(new GuiSlotGhost(this, 0, 0, 0));
    }

}

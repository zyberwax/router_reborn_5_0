package com.tomevoll.routerreborn.lib.gui;

import com.tomevoll.routerreborn.lib.gui.iface.IClientGuiModule;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiModule;
import com.tomevoll.routerreborn.lib.gui.iface.IServerGuiModule;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.network.C01_ContainerToClient;
import com.tomevoll.routerreborn.lib.gui.network.S01_ContainerToServer;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotGhost;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

public class ContainerServer extends Container {

    public EntityPlayer player;
    public IGuiTile t;
    public boolean readyToUpdate = false;
    public IGuiModule activeModule = null;
    SimpleNetworkWrapper network;
    World world;
    ModuleRegistry modules;

    public ContainerServer(IGuiTile t, EntityPlayer player, World world, SimpleNetworkWrapper network) {
        // super();
        this.t = t;
        this.player = player;
        this.world = world;
        this.network = network;

        modules = new ModuleRegistry();
        if (world.isRemote) {
            t.registerClientGuiModule(modules);
        } else {
            t.registerServerGuiModule(modules);
        }

        int i = 87;

        for (int l = 0; l < 3; ++l) {
            for (int k = 0; k < 9; ++k) {
                this.addSlotToContainer(new Slot(player.inventory, k + l * 9 + 9, 8 + k * 18, l * 18 + i));
            }
        }

        i = 143;
        for (int i1 = 0; i1 < 9; ++i1) {
            this.addSlotToContainer(new Slot(player.inventory, i1, 8 + i1 * 18, i));
        }

        IGuiModule[] list = modules.getModules();
        int indx = 0;
        for (i = 0; i < list.length; i++) {
            list[i].addSlots(this);

        }

    }

    public static boolean areItemStacksEqual(ItemStack stackA, ItemStack stackB) {
        return stackB.getItem() == stackA.getItem()
                && (!stackA.getHasSubtypes() || stackA.getMetadata() == stackB.getMetadata())
                && ItemStack.areItemStackTagsEqual(stackA, stackB);
    }

    public GuiSlot addSlot(GuiSlot slot) {
        return (GuiSlot) addSlotToContainer(slot);
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {

        return true;
    }

    // @SideOnly(Side.SERVER)
    public void onMessageFromClient(String module, int cmd, String val1, int val2) {
        if (module.equals("container") && cmd == 1) {
            readyToUpdate = true;
            return;
        }
        if (module.equals("container") && cmd == 2) {
            activeModule = modules.getModuleByID(val1);
            return;
        }
        if (module.equals("container") && cmd == 3) {
            ModuleServerBase mod = modules.getModuleByID(val1);
            if (mod != null) {
                mod.uninstallModule();
                int ID = modules.modules.indexOf(mod);
                modules.modules.remove(mod);
                if (ID >= modules.modules.size())
                    ID = 0;

                activeModule = modules.getModuleByIndex(ID);
                return;
            }
        }

        IServerGuiModule mod;
        mod = modules.getModuleByID(module);
        if (mod != null) {
            mod.handleMessageFromClient(this, t, cmd, val1, val2);
        }

    }

    // @SideOnly(Side.CLIENT)
    public void sendToServer(String module, int cmd, int val) {
        network.sendToServer(new S01_ContainerToServer(module, cmd, "", val));
    }

    // @SideOnly(Side.CLIENT)
    public void sendToServer(String module, int cmd, String val) {
        network.sendToServer(new S01_ContainerToServer(module, cmd, val, -1));
    }

    public void onMessageFromServer(String module, int cmd, String val1, int val2) {
        IClientGuiModule mod;
        mod = modules.getModuleByID(module);
        if (mod != null) {
            mod.handleMessageFromServer(this, t, cmd, val1, val2);
        }

    }

    // @SideOnly(Side.CLIENT)
    public void sendToClient(String module, int cmd, int val) {
        network.sendTo(new C01_ContainerToClient(module, cmd, "", val), (EntityPlayerMP) player);
    }

    // @SideOnly(Side.CLIENT)
    public void sendToClient(String module, int cmd, String val) {
        network.sendTo(new C01_ContainerToClient(module, cmd, val, -1), (EntityPlayerMP) player);
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();
        // somehow this get called on client side randomly causing all slots to
        // desync and show up empty, JEI?
        if (readyToUpdate && !world.isRemote) {

            IServerGuiModule mod;

            IGuiModule[] list = modules.getModules();
            int indx = 0;
            for (int i = 0; i < list.length; i++) {
                ((IServerGuiModule) list[i]).detectAndSendChanges(this, t);

            }
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {

        // if(true) return super.transferStackInSlot(player, slot);

        ItemStack stack = ItemStack.EMPTY;
        Slot slotObject = inventorySlots.get(slot);
        int numSlots = inventorySlots.size() - 36;

        if (slotObject instanceof GuiSlotGhost)
            return ItemStack.EMPTY;

        if (slotObject != null && slotObject.getHasStack()) {
            ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();

            if (slot < 36) { // Transfer from inventory to module

                if (!activeModule.mergeItemStack(stackInSlot, inventorySlots.size() - numSlots, inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else {
                if (!mergeItemStack(stackInSlot, 27, 36, false)) {
                    if (!mergeItemStack(stackInSlot, 0, 27, false)) {
                        return ItemStack.EMPTY;
                    }
                }
            }

            if (stackInSlot.getCount() == 0) {
                slotObject.putStack(ItemStack.EMPTY);
            } else {
                slotObject.onSlotChanged();
            }

            if (stackInSlot.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slotObject.onTake(player, stackInSlot);
        }

        return stack;
    }

    public ItemStack slotClick(int slot, int dragType, ClickType clickTypeIn, EntityPlayer player) {

        Slot sl = null;
        if (slot > 0 && slot < this.inventorySlots.size())
            sl = this.inventorySlots.get(slot);

        if (sl instanceof GuiSlotGhost) {
            InventoryPlayer inventoryplayer = player.inventory;
            ItemStack itemstack = inventoryplayer.getItemStack();

            if (clickTypeIn == ClickType.PICKUP) {
                ItemStack s = this.inventorySlots.get(slot).getStack();

                if (s.isEmpty() && !itemstack.isEmpty()) {
                    if (dragType == 0)
                        this.inventorySlots.get(slot).putStack(itemstack.copy());
                    else
                        this.inventorySlots.get(slot).putStack(itemstack.copy().splitStack(1));

                    this.detectAndSendChanges();

                } else {
                    if (dragType == 0) {
                        this.inventorySlots.get(slot).putStack(ItemStack.EMPTY);
                        this.detectAndSendChanges();
                    } else if (dragType == 1) {

                        if (itemstack.isEmpty()) {
                            this.inventorySlots.get(slot).decrStackSize(1);
                        } else {
                            s.grow(1);
                            this.inventorySlots.get(slot).putStack(s);
                        }
                        this.detectAndSendChanges();
                    }

                }
                // this.inventorySlots.get(slot).putStack(null);
            }

            return ItemStack.EMPTY;
        }
        return super.slotClick(slot, dragType, clickTypeIn, player);
    }

    // Overridden to prevent shift clicking items into a furnace output slot
    protected boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        boolean flag = false;
        int i = startIndex;

        if (this.inventorySlots.get(i) instanceof GuiSlotGhost)
            reverseDirection = false;

        if (reverseDirection) {
            i = endIndex - 1;
        }

        if (stack.isStackable()) {
            while (stack.getCount() > 0 && (!reverseDirection && i < endIndex || reverseDirection && i >= startIndex)) {
                boolean isGhost = false;
                ItemStack ghostStack = stack.copy();
                ghostStack.setCount(1);
                Slot slot = this.inventorySlots.get(i);
                ItemStack itemstack = slot.getStack();

                if (slot instanceof GuiSlotGhost) {
                    isGhost = true;
                    break;
                } else if (!itemstack.isEmpty() && areItemStacksEqual(stack, itemstack) && slot.isItemValid(stack)) // prevent
                // shift
                // clicking
                // items
                // into
                // a
                // furnace
                // output
                // slot
                {
                    int j = itemstack.getCount() + stack.getCount();
                    int maxSize = Math.min(slot.getSlotStackLimit(), stack.getMaxStackSize());

                    if (j <= maxSize) {
                        stack.setCount(0);
                        itemstack.setCount(j);
                        slot.onSlotChanged();
                        flag = true;
                    } else if (itemstack.getCount() < maxSize) {
                        stack.shrink(maxSize - itemstack.getCount());
                        itemstack.setCount(maxSize);
                        slot.onSlotChanged();
                        flag = true;
                    }
                }

                if (reverseDirection) {
                    --i;
                } else {
                    ++i;
                }
            }
        }

        if (stack.getCount() > 0) {
            if (reverseDirection) {
                i = endIndex - 1;
            } else {
                i = startIndex;
            }

            while (!reverseDirection && i < endIndex || reverseDirection && i >= startIndex) {
                Slot slot1 = this.inventorySlots.get(i);
                ItemStack itemstack1 = slot1.getStack();
                boolean isGhost1 = false;
                ItemStack ghostStack1 = stack.copy();
                ghostStack1.setCount(1);

                if (slot1 instanceof GuiSlotGhost) {

                    if (itemstack1.isEmpty()) {
                        isGhost1 = true;
                        slot1.putStack(ghostStack1);
                        break;
                    }

                } else if (itemstack1.isEmpty() && slot1.isItemValid(stack)) // Forge:
                // Make
                // sure
                // to
                // respect
                // isItemValid
                // in
                // the
                // slot.
                {

                    slot1.putStack(stack.copy());
                    slot1.onSlotChanged();
                    stack.setCount(0);
                    flag = true;
                    break;

                }

                if (reverseDirection) {
                    --i;
                } else {
                    ++i;
                }
            }
        }

        return flag;
    }

    @Override
    public void updateProgressBar(int id, int data) {
        // TODO Auto-generated method stub
        super.updateProgressBar(id, data);
    }

    @Override
    public void addListener(IContainerListener listener) {
        // TODO Auto-generated method stub
        // super.addListener(listener);

        // TODO Auto-generated method stub
        if (this.listeners.contains(listener)) {
            throw new IllegalArgumentException("Listener already listening");
        } else {
            this.listeners.add(listener);
            listener.sendAllContents(this, this.getInventory());
            this.detectAndSendChanges();
        }
        // super.onCraftGuiOpened(listener);
    }

}

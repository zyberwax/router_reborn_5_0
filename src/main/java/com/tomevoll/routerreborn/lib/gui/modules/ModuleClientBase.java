package com.tomevoll.routerreborn.lib.gui.modules;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.IToolTip;
import com.tomevoll.routerreborn.lib.gui.iface.IClientGuiModule;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.iface.IItab;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class ModuleClientBase implements IClientGuiModule, IItab {


    //public abstract ItemStack GetTabIcon();
    public IGuiController gui;
    public boolean canUninstall = false;
    protected int Left = 0;
    protected int Top = 0;
    protected List<GuiButton> buttons;
    protected int startIndex = 0;
    protected ContainerServer container;
    protected ItemStack icon;
    protected List<Slots> slots = new ArrayList<Slots>();
    String lastTabID = "";


    public ModuleClientBase(ItemStack icon) {
        buttons = new ArrayList<GuiButton>();
        this.icon = icon;
    }

    public ModuleClientBase setCanUninstall(boolean val) {
        canUninstall = val;
        return this;
    }

    public abstract String getModuleID();

    @Override
    public ItemStack GetItem() {
        return icon;
    }

    @Override
    public void MouseMove(int x, int y) {

    }

    @Override
    public void MouseClick(int x, int y, int button) {

    }

    @Override
    public boolean KeyPress(char c, int keyCode) {
        return false;
    }

    @Override
    public void HideContent(ContainerServer c) {
        for (GuiButton guiButton : buttons) {
            guiButton.visible = false;

        }
        for (Slots s : slots) {
            //s.slot.xDisplayPosition = s.getxPos();
            s.slot.yPos = -50000;
        }

    }

    @Override
    public void Show(ContainerServer c) {
        if (!getModuleID().equals(lastTabID)) {
            c.sendToServer("container", 2, getModuleID());
            lastTabID = getModuleID();
        }

        for (GuiButton guiButton : buttons) {
            guiButton.visible = true;

        }

        for (Slots s : slots) {
            //s.slot.xDisplayPosition = s.getxPos();
            s.slot.yPos = s.getyPos();
        }

        c.activeModule = this;

    }

    protected abstract void AddControls(IGuiController gui);

    protected int AddButton(GuiButton button) {
        button.id = startIndex;
        startIndex++;

        buttons.add(button);
        return button.id;

    }

    @Override
    public void Draw() {

    }

    @Override
    public void DrawTooltip() {
        for (GuiButton guiButton : buttons) {
            if (guiButton instanceof IToolTip)

                ((IToolTip) guiButton).DrawTooltips(gui.getMC());
        }
    }

    @Override
    public void DrawBG() {


        int x1, x2, y1, y2;

        int darkBorder = new Color(80, 80, 80).getRGB();
        int lightBorder = new Color(255, 255, 255).getRGB();
        int cornerBorder = new Color(155, 155, 155).getRGB();
        int bk = new Color(150, 150, 150).getRGB();

        for (int i = 0; i < slots.size(); i++) {

            Slot s = slots.get(i).getSlot();
            x1 = (Left + s.xPos) * 2;
            y1 = (Top + s.yPos) * 2;
            x2 = x1 + 32;
            y2 = y1 + 32;


            // gui.itemRender.renderIcon(p_94149_1_, p_94149_2_, p_94149_3_, p_94149_4_, p_94149_5_);

            GL11.glPushMatrix();
            GL11.glScalef(0.5f, 0.5f, 1.0f);
            Gui.drawRect(x1, y1, x2, y2, bk);

            Gui.drawRect(x1, y1 - 1, x1 - 1, y2 + 1, darkBorder);
            Gui.drawRect(x1, y1, x2, y1 - 1, darkBorder);

            Gui.drawRect(x1, y2, x2, y2 + 1, lightBorder);
            Gui.drawRect(x2, y1 - 1, x2 + 1, y2 + 1, lightBorder);


            Gui.drawRect(x1, y2, x1 - 1, y2 + 1, cornerBorder);
            Gui.drawRect(x2, y1, x2 + 1, y1 - 1, cornerBorder);


            // gui.drawTexturedModalRect(width + (xpos - 2) + (space * i) + (((16 * 2)) * (i)), height + (ypos - 2), 0, 202, 36, 36);
            GL11.glPopMatrix();
        }
        // gui.drawTexturedModalRect(k + SL.xDisplayPosition - 1, l + SL.yDisplayPosition - 1, 0, 0, 16, 16);


    }

    @Override
    public void ActionPerformed(GuiButton btn) {
        gui.getContainer().sendToServer(getModuleID(), CMD_TYPE.BUTTON_CLICK.Value, btn.id);
    }

    @Override
    public int registerButtons(int startIndx, IGuiController gui) {
        buttons.clear();
        this.startIndex = startIndx;
        AddControls(gui);

        int i = startIndx;

        for (GuiButton guiButton : buttons) {
            guiButton.visible = false;
            gui.registerButton(guiButton);
            i++;
        }
        return i;
    }

    @Override
    public void initGui(IGuiController gui) {
        this.Left = (gui.getWidth() - gui.getXsize()) / 2;
        this.Top = (gui.getHeight() - gui.getYsize()) / 2;
        this.gui = gui;
    }

    @Override
    public String GetTooltip() {
        return null;
    }

    @Override
    public void updateScreen() {

    }

    public int getXSize() {
        // TODO Auto-generated method stub
        return gui.getXsize();
    }

    public int getYSize() {
        // TODO Auto-generated method stub
        return gui.getYsize();
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        // TODO Auto-generated method stub
        return ItemStack.EMPTY;
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getStackLimit() {
        // TODO Auto-generated method stub
        return 64;
    }

    public void addSlots(ContainerServer c) {
        this.container = c;
        slots.clear();
        registerSlots();
    }

    protected abstract void registerSlots();

    protected void addSlot(GuiSlot slot) {
        GuiSlot s = container.addSlot(slot);
        slots.add(new Slots(s.slotNumber, s.getSlotIndex(), s, s.xPos, s.yPos));
        s.yPos = -50000; //hide it, original position stored in the list
    }

    protected Slots getSlotByContainerIndex(int index) {
        for (int i = 0; i < slots.size(); i++) {
            if (slots.get(i).containerIndex == index)
                return slots.get(i);
        }
        return null;
    }

    protected Slots getSlotByModuleIndex(int index) {
        for (int i = 0; i < slots.size(); i++) {
            if (slots.get(i).moduleIndex == index)
                return slots.get(i);
        }
        return null;
    }

    @Override
    public float getSmeltingExperience(ItemStack stack) {
        return 0;
    }

    // Only called for transfers to the module, never when transfer from the
    // module, start and end index should be ignored
    public boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        boolean flag = false;
        int i = startIndex;

        if (reverseDirection) {
            i = slots.size() - 1;
        }

        for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > -1
                : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
            Slot slot = slots.get(i).slot;
            ItemStack itemstack = slot.getStack();

            if (stack.isStackable()) {
                if (!itemstack.isEmpty() && ContainerServer.areItemStacksEqual(stack, itemstack) && slot.isItemValid(stack)) // slot
                {
                    int j = itemstack.getCount() + stack.getCount();
                    int maxSize = Math.min(slot.getSlotStackLimit(), stack.getMaxStackSize());

                    if (j <= maxSize) {
                        stack.setCount(0);
                        itemstack.setCount(j);
                        slot.onSlotChanged();
                        flag = true;
                    } else if (itemstack.getCount() < maxSize) {
                        stack.shrink(maxSize - itemstack.getCount());
                        itemstack.setCount(maxSize);
                        slot.onSlotChanged();
                        flag = true;
                    }
                }
            }
        }

        if (stack.getCount() > 0) {
            for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > -1
                    : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
                Slot slot = slots.get(i).slot;
                ItemStack itemstack = slot.getStack();

                if (itemstack.isEmpty() && slot.isItemValid(stack)) // Forge:
                {
                    slot.putStack(stack.copy());
                    slot.onSlotChanged();
                    stack.setCount(0);
                    flag = true;
                    break;
                }
            }
        }

        return flag;
    }

    public class Slots {
        private int containerIndex;
        private int moduleIndex;
        private GuiSlot slot;
        private int yPos;
        private int xPos;


        public Slots(int ContainerIndex, int ModuleIndex, GuiSlot s, int xPos, int yPos) {
            setContainerIndex(ContainerIndex);
            setModuleIndex(ModuleIndex);
            setSlot(s);
            setxPos(xPos);
            setyPos(yPos);
        }

        int getContainerIndex() {
            return containerIndex;
        }

        void setContainerIndex(int containerIndex) {
            this.containerIndex = containerIndex;
        }

        int getModuleIndex() {
            return moduleIndex;
        }

        void setModuleIndex(int moduleIndex) {
            this.moduleIndex = moduleIndex;
        }

        public GuiSlot getSlot() {
            return slot;
        }

        void setSlot(GuiSlot slot) {
            this.slot = slot;
        }

        private int getyPos() {
            return yPos;
        }

        private void setyPos(int yPos) {
            this.yPos = yPos;
        }

        private int getxPos() {
            return xPos;
        }

        private void setxPos(int xPos) {
            this.xPos = xPos;
        }
    }


}

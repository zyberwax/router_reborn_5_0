package com.tomevoll.routerreborn.lib.gui.modules.rangeselect;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import net.minecraft.util.EnumFacing;

public class RangeSelectServerModule extends ModuleServerBase {

    IGuiRangeSelectTile tile;

    private int maxRange = -100;


    private int rangeN = -100;
    private int rangeS = -100;
    private int rangeW = -100;
    private int rangeE = -100;
    private int rangeO = -100;
    private boolean showRange = false;

    public RangeSelectServerModule(IGuiRangeSelectTile tile) {
        // TODO Auto-generated constructor stub
        super();
        this.tile = tile;
    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t,
                                        int cmd, String val1, int val2) {
        switch (cmd) {
            case 1:
                tile.setRange(EnumFacing.NORTH, val2);
                break;
            case 2:
                tile.setRange(EnumFacing.SOUTH, val2);
                break;
            case 3:
                tile.setRange(EnumFacing.WEST, val2);
                break;
            case 4:
                tile.setRange(EnumFacing.EAST, val2);
                break;
            case 5:
                tile.setOffsetY(val2);
                break;
            case 6:
                tile.setShowRange(val2 == 1);
                break;

            default:
                break;
        }

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
        // TODO Auto-generated method stub
        int N = tile.getRange(EnumFacing.NORTH);
        int S = tile.getRange(EnumFacing.SOUTH);
        int W = tile.getRange(EnumFacing.WEST);
        int E = tile.getRange(EnumFacing.EAST);
        int O = tile.getOffsetY();


        if (tile.getMaxRange() != maxRange)
            container.sendToClient(getModuleID(), 0, tile.getMaxRange());

        if (N != rangeN)
            container.sendToClient(getModuleID(), 1, N);

        if (S != rangeS)
            container.sendToClient(getModuleID(), 2, S);


        if (W != rangeW)
            container.sendToClient(getModuleID(), 3, W);

        if (E != rangeE)
            container.sendToClient(getModuleID(), 4, E);

        if (O != rangeO)
            container.sendToClient(getModuleID(), 5, O);

        if (showRange != tile.getShowRange())
            container.sendToClient(getModuleID(), 6, tile.getShowRange() == true ? 1 : 0);


        rangeN = N;
        rangeS = S;
        rangeW = W;
        rangeE = E;
        rangeO = O;
        maxRange = tile.getMaxRange();
        showRange = tile.getShowRange();
    }

    @Override
    public String getModuleID() {
        // TODO Auto-generated method stub
        return "rangeselect";
    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }

}

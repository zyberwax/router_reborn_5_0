package com.tomevoll.routerreborn.lib.gui.modules.textinfo;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import net.minecraft.tileentity.TileEntity;

public class TextInfoServer extends ModuleServerBase {

    String ID = "textinfo";

    public TextInfoServer(TileEntity tile, String ID) {
        super();
        this.ID = ID;
    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getModuleID() {
        // TODO Auto-generated method stub
        return ID;
    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }

}

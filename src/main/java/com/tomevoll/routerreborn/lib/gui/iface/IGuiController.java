/**
 *
 */
package com.tomevoll.routerreborn.lib.gui.iface;


import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;

import java.util.List;

/**
 * @author zyberwax
 */
public interface IGuiController {
    void removeTab(IItab tab);

    void registerButton(GuiButton but);

    void UnregisterButton(GuiButton but);

    ContainerServer getContainer();

    int getWidth();

    int getXsize();

    int getHeight();

    int getYsize();

    Minecraft getMC();

    void drawBurnBar(int x, int y, int totalBurnTime, int burnTime, boolean backOnly);

    void drawFilledRect(int x, int y, int width, int heigh, int bordercolor, int fillcolor);

    void draw3DBorder(int x, int y, int width, int height, boolean sunken, boolean filled);

    void drawProgressBar(int x, int y, int processTimeTotal, int processTime, boolean backOnly);

    void drawEnergyBar(int x, int y, int width, int height, int maxEnergy, int storedEnergy);

    void drawTooltipText(List<String> asList, int mouseX, int mouseY,
                         FontRenderer fontRendererObj);


}

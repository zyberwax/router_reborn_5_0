package com.tomevoll.routerreborn.lib.gui.control;


import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GuiButtonFlat extends GuiButton implements IToolTip {
    private static ResourceLocation CustomTextures = new ResourceLocation("routerrebornlib:textures/gui/buttons.png");
    public float br = 1f, bg = 1f, bb = 1f, ba = 1f;
    public boolean selected = false;
    public float ir = 1f, ig = 1f, ib = 1f, ia = 1f;
    private boolean doh = false;
    private long counter = 0;
    private float cmp = 0.0f;
    private int yIndex = 0;
    private int xIndex = 0;
    private boolean hovering = false;
    //protected static RenderItem		itemRender		= new RenderItem(null, null);
    private int mx = 0;
    private int my = 0;

    public GuiButtonFlat(int p_i1021_1_, int p_i1021_2_, int p_i1021_3_, int p_i1021_4_, int p_i1021_5_, int xIndex, int yIndex) {
        super(p_i1021_1_, p_i1021_2_, p_i1021_3_, p_i1021_4_, p_i1021_5_, "");
        this.yIndex = yIndex;
        this.xIndex = xIndex;

        //this.itemRender = new RenderItem( Minecraft.getMinecraft().getTextureManager(), Minecraft.getMinecraft().mo modelManager)
    }

    private static void drawRect(int p_73734_0_, int p_73734_1_, int p_73734_2_, int p_73734_3_, float r, float g, float b, float a) {
        int j1;

        if (p_73734_0_ < p_73734_2_) {
            j1 = p_73734_0_;
            p_73734_0_ = p_73734_2_;
            p_73734_2_ = j1;
        }

        if (p_73734_1_ < p_73734_3_) {
            j1 = p_73734_1_;
            p_73734_1_ = p_73734_3_;
            p_73734_3_ = j1;
        }

        Tessellator tessellator = Tessellator.getInstance();
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glColor4f(r, g, b, a);


        BufferBuilder worldrenderer = tessellator.getBuffer();

        worldrenderer.begin(7, DefaultVertexFormats.POSITION);

        worldrenderer.pos(p_73734_0_, p_73734_3_, 0.0D).endVertex();
        worldrenderer.pos(p_73734_2_, p_73734_3_, 0.0D).endVertex();
        worldrenderer.pos(p_73734_2_, p_73734_1_, 0.0D).endVertex();
        worldrenderer.pos(p_73734_0_, p_73734_1_, 0.0D).endVertex();

        tessellator.draw();


        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void drawRect(int left, int top, int right, int bottom, int color) {
        if (left < right) {
            int i = left;
            left = right;
            right = i;
        }

        if (top < bottom) {
            int j = top;
            top = bottom;
            bottom = j;
        }

        float f3 = (float) (color >> 24 & 255) / 255.0F;
        float f = (float) (color >> 16 & 255) / 255.0F;
        float f1 = (float) (color >> 8 & 255) / 255.0F;
        float f2 = (float) (color & 255) / 255.0F;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();
        //GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        //GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(f, f1, f2, f3);
        GL11.glColor4f(f, f1, f2, f3);
        worldrenderer.begin(7, DefaultVertexFormats.POSITION);
        worldrenderer.pos((double) left, (double) bottom, 0.0D).endVertex();
        worldrenderer.pos((double) right, (double) bottom, 0.0D).endVertex();
        worldrenderer.pos((double) right, (double) top, 0.0D).endVertex();
        worldrenderer.pos((double) left, (double) top, 0.0D).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        //GlStateManager.disableBlend();
    }

    /*
    // need to be in here to be compatible with 1.7.2, not needed on 1.7.10
    private static void func_152125_a(int x, int y, float u, float v, int uWidth, int vHeight, int width, int height, float tileWidth, float tileHeight) {
        float f4 = 1.0F / tileWidth;
        float f5 = 1.0F / tileHeight;
        Tessellator tessellator = Tessellator.getInstance();
        VertexBuffer worldrenderer = tessellator.getBuffer();
        worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX);

        worldrenderer.pos(x, y + height, 0.0D).tex(u * f4, (v + vHeight) * f5).endVertex();
        worldrenderer.pos(x + width, y + height, 0.0D).tex((u + uWidth) * f4, (v + vHeight) * f5).endVertex();
        worldrenderer.pos(x + width, y, 0.0D).tex((u + uWidth) * f4, v * f5).endVertex();
        worldrenderer.pos(x, y, 0.0D).tex(u * f4, v * f5).endVertex();

        tessellator.draw();

    }
    */

    public static void drawScaledCustomSizeModalRect(int x, int y, float u, float v, int uWidth, int vHeight, int width, int height, float tileWidth, float tileHeight) {
        float f = 1.0F / tileWidth;
        float f1 = 1.0F / tileHeight;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
        vertexbuffer.pos((double) x, (double) (y + height), 0.0D).tex((double) (u * f), (double) ((v + (float) vHeight) * f1)).endVertex();
        vertexbuffer.pos((double) (x + width), (double) (y + height), 0.0D).tex((double) ((u + (float) uWidth) * f), (double) ((v + (float) vHeight) * f1)).endVertex();
        vertexbuffer.pos((double) (x + width), (double) y, 0.0D).tex((double) ((u + (float) uWidth) * f), (double) (v * f1)).endVertex();
        vertexbuffer.pos((double) x, (double) y, 0.0D).tex((double) (u * f), (double) (v * f1)).endVertex();
        tessellator.draw();
    }

    public void setImageIndexes(int x, int y) {
        this.yIndex = y;
        this.xIndex = x;
    }

    @Override
    public void drawButton(Minecraft mc, int x, int y, float t) {
        if (this.visible) {
            GL11.glPushMatrix();
            FontRenderer fontrenderer = mc.fontRenderer;
            mc.getTextureManager().bindTexture(CustomTextures);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = x >= this.x && y >= this.y && x < this.x + this.width && y < this.y + this.height;
            int k = this.getHoverState(this.hovered);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

            int xpos = this.x;
            int ypos = this.y;

            int width = this.width;
            int height = this.height;

            float scale = 0.5f;
            counter++;
            if (counter + 100 < Minecraft.getSystemTime()) {
                counter = Minecraft.getSystemTime();
                if (doh)
                    cmp += 0.1f;
                else
                    cmp -= 0.1f;

                if (cmp <= 0)
                    doh = true;
                if (cmp >= 1f)
                    doh = false;
            }

            // selected = false;

            float multiplier = xpos * scale;
            multiplier = xpos / multiplier;
            multiplier = multiplier - 1;

            width = (int) (width + (width * multiplier));
            height = (int) (height + (height * multiplier));

            xpos = (int) (xpos + (xpos * multiplier));
            ypos = (int) (ypos + (ypos * multiplier));

            // if (selected)
            // this.drawRect(this.xPosition - 1, this.yPosition - 1, this.xPosition + (this.width + 1), this.yPosition + this.height + 1, br, bg, bb, ba);

            mc.getTextureManager().bindTexture(BUTTON_TEXTURES);

            // System.out.println(k);


            GL11.glScalef(scale, scale, scale);
            GL11.glDisable(GL11.GL_BLEND);
            // this.enabled = false;
            if (this.enabled) {
                if (selected) {

                    float resBR = 0.4f - (br);
                    resBR = resBR * cmp;
                    float resBG = 0.8f - (bg);
                    resBG = resBG * cmp;
                    float resBB = 1.0f - (bb);
                    resBB = resBB * cmp;

                    drawRect(xpos - 2, ypos - 2, xpos + (width + 2), ypos + height + 2, br + resBR, bg + resBG, bb + resBB, ba);
                }

                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                if (k > 1) {
                    drawRect(xpos, ypos, xpos + width, ypos + height, 0xff7c95c0);

                    drawRect(xpos, ypos + height, xpos + width, ypos + height - 1, 0xff111111);
                    drawRect(xpos + width, ypos, xpos + width - 1, ypos + height - 1, 0xff111111);

                    drawRect(xpos, ypos, xpos + width, ypos + 1, 0xff111111);
                    drawRect(xpos, ypos, xpos + 1, ypos + height - 1, 0xff111111);
                }
                if (k == 1) {
                    drawRect(xpos, ypos, xpos + width, ypos + height, 0xffadb6c1);

                    drawRect(xpos, ypos + height, xpos + width, ypos + height - 1, 0.2f, 0.2f, 0.2f, 1.0f);
                    drawRect(xpos + width, ypos, xpos + width - 1, ypos + height - 1, 0.2f, 0.2f, 0.2f, 1.0f);

                    drawRect(xpos, ypos, xpos + width, ypos + 1, 0.2f, 0.2f, 0.2f, 1.0f);
                    drawRect(xpos, ypos, xpos + 1, ypos + height - 1, 0.2f, 0.2f, 0.2f, 1.0f);
                }
            } else {

                drawRect(xpos, ypos, xpos + width, ypos + height, 0.3f, 0.3f, 0.3f, 1f);

                drawRect(xpos, ypos + height, xpos + width, ypos + height - 1, 0.1f, 0.1f, 0.1f, 1.0f);
                drawRect(xpos + width, ypos, xpos + width - 1, ypos + height - 1, 0.1f, 0.1f, 0.1f, 1.0f);

                drawRect(xpos, ypos, xpos + width, ypos + 1, 0.1f, 0.1f, 0.1f, 1.0f);
                drawRect(xpos, ypos, xpos + 1, ypos + height - 1, 0.1f, 0.1f, 0.1f, 1.0f);

            }

            // top left
            // this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height / 2);
            // top right
            // this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height / 2);
            // bottom left
            // this.drawTexturedModalRect(this.xPosition, this.yPosition + this.height / 2, 0, 46 + k * 20 + 20 - (this.height / 2), this.width / 2, this.height
            // / 2);
            // bottom right
            // this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition + this.height / 2, 200 - this.width / 2, 46 + k * 20 + 20 -
            // this.height / 2, this.width / 2, this.height / 2);

            mc.getTextureManager().bindTexture(CustomTextures);
            GL11.glColor4f(0.1F, 0.1F, 0.1F, 0.7F);

            int offset = 0;

            if (this.enabled) {
                if (k == 2 && this.enabled)
                    offset = -1;

                drawScaledCustomSizeModalRect(xpos + 3, ypos + 3, this.xIndex, this.yIndex, 1, 1, width - 4, height - 4, 10.66f, 10.66f);
                GL11.glColor4f(ir, ig, ib, ia);
                drawScaledCustomSizeModalRect(xpos + 2, ypos + 2, this.xIndex, this.yIndex, 1, 1, width - 4, height - 4, 10.66f, 10.66f);

            } else {
                drawScaledCustomSizeModalRect(xpos + 3 + offset, ypos + 3 + offset, this.xIndex, this.yIndex, 1, 1, width - 4, height - 4, 10.66f, 10.66f);
                GL11.glColor4f(0.4f, 0.4f, 0.4f, 0.6f);
                drawScaledCustomSizeModalRect(xpos + 2 + offset, ypos + 2 + offset, this.xIndex, this.yIndex, 1, 1, width - 4, height - 4, 10.66f, 10.66f);

            }
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glPopMatrix();
            this.mouseDragged(mc, x, y);
            int l = 14737632;

            if (packedFGColour != 0) {
                l = packedFGColour;
            } else if (!this.enabled) {
                l = 10526880;
            } else if (this.hovered) {
                l = 16777120;
            }

            this.hovering = k == 2;

            mx = x;
            my = y;
            //GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        }
    }

    public void DrawTooltips(Minecraft mc) {
        if (this.hovering) {
            if (this.displayString.length() > 0)
                this.drawHoveringText(Arrays.asList(this.displayString), this.mx, this.my, mc.fontRenderer);
        }
    }

    protected void drawHoveringText(List p_146283_1_, int p_146283_2_, int p_146283_3_, FontRenderer font) {
        if (!p_146283_1_.isEmpty()) {
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.disableStandardItemLighting();
            //GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_DEPTH_TEST);

            int k = 0;
            Iterator iterator = p_146283_1_.iterator();

            while (iterator.hasNext()) {
                String s = (String) iterator.next();
                int l = font.getStringWidth(s);

                if (l > k) {
                    k = l;
                }
            }

            int j2 = p_146283_2_ + 12;
            int k2 = p_146283_3_ - 12;
            int i1 = 8;

            if (p_146283_1_.size() > 1) {
                i1 += 2 + (p_146283_1_.size() - 1) * 10;
            }

            this.zLevel = 200.0F;
            //itemRender.zLevel = 200.0F;
            int j1 = -267386864;
            this.drawGradientRect(j2 - 3, k2 - 4, j2 + k + 3, k2 - 3, j1, j1);
            this.drawGradientRect(j2 - 3, k2 + i1 + 3, j2 + k + 3, k2 + i1 + 4, j1, j1);
            this.drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 + i1 + 3, j1, j1);
            this.drawGradientRect(j2 - 4, k2 - 3, j2 - 3, k2 + i1 + 3, j1, j1);
            this.drawGradientRect(j2 + k + 3, k2 - 3, j2 + k + 4, k2 + i1 + 3, j1, j1);
            int k1 = 1347420415;
            int l1 = (k1 & 16711422) >> 1 | k1 & -16777216;
            this.drawGradientRect(j2 - 3, k2 - 3 + 1, j2 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
            this.drawGradientRect(j2 + k + 2, k2 - 3 + 1, j2 + k + 3, k2 + i1 + 3 - 1, k1, l1);
            this.drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 - 3 + 1, k1, k1);
            this.drawGradientRect(j2 - 3, k2 + i1 + 2, j2 + k + 3, k2 + i1 + 3, l1, l1);

            for (int i2 = 0; i2 < p_146283_1_.size(); ++i2) {
                String s1 = (String) p_146283_1_.get(i2);
                font.drawStringWithShadow(s1, j2, k2, -1);

                if (i2 == 0) {
                    k2 += 2;
                }

                k2 += 10;
            }

            this.zLevel = 0.0F;
            //itemRender.zLevel = 0.0F;
            //GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            //RenderHelper.enableStandardItemLighting();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        }
    }
}

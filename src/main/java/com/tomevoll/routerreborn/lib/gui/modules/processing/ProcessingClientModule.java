package com.tomevoll.routerreborn.lib.gui.modules.processing;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlotOutput;
import net.minecraft.item.ItemStack;

public class ProcessingClientModule extends ModuleClientBase {
    IGuiProcessingTile tile;
    private int storedEnergy = 0;
    private int maxEnergy = 0;
    private int burnTime = 0;
    private int totalBurnTime = 0;
    private boolean useEnergy = false;
    private int processTimeTotal = 0;
    private int processTime = 0;

    public ProcessingClientModule(IGuiProcessingTile tile, boolean useEnergy, ItemStack icon) {
        super(icon);
        this.tile = tile;
        this.useEnergy = useEnergy;
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        switch (cmd) {
            case 1:
                storedEnergy = val2;
                break;
            case 2:
                maxEnergy = val2;
                break;
            case 3:
                burnTime = val2;
                break;
            case 4:
                totalBurnTime = val2;
                break;
            case 5:
                useEnergy = val2 == 1;
                break;
            case 6:
                processTime = val2;
                break;
            case 7:
                processTimeTotal = val2;
                break;
            default:
                break;
        }
    }

    @Override
    public void DrawBG() {
        super.DrawBG();

        if (useEnergy) gui.drawEnergyBar(10, 28, 10, 50, maxEnergy, storedEnergy);

        gui.drawProgressBar(75, 47, processTimeTotal, processTime, false);

        if (!useEnergy) gui.drawBurnBar(52, 47, totalBurnTime, burnTime, false);
    }


    @Override
    public String getModuleID() {
        return "processing";
    }


    @Override
    protected void AddControls(IGuiController gui) {

    }

    @Override
    protected void registerSlots() {
        addSlot(new GuiSlot(this, 0, Left + 50, Top + (!useEnergy ? 28 : 45)));
        addSlot(new GuiSlotOutput(container.player, this, 2, Left + 105, Top + 45));
        if (!useEnergy)
            addSlot(new GuiSlot(this, 1, Left + 50, Top + 62));
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        return tile.getProcessingSlot(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        tile.setProcessingSlot(slotIndex, stack);
    }

    @Override
    public float getSmeltingExperience(ItemStack stack) {
        return tile.getProcessingXP(stack);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }

}

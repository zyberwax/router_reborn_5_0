package com.tomevoll.routerreborn.lib.gui.modules.nineslotonetwo;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import net.minecraft.item.ItemStack;

public class NineOneTwoServerModule extends ModuleServerBase {

    INineOneTwoTile tile;
    private int storedEnergy = 0;
    private int maxEnergy = 0;
    private boolean useEnergy = false;
    private int storedEnergy2 = 0;
    private int maxEnergy2 = 0;
    private boolean useEnergy2 = false;

    public NineOneTwoServerModule(INineOneTwoTile tile, boolean useEnergy) {
        super();
        this.tile = tile;
        this.useEnergy = useEnergy;
    }


    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
        tile.UpdateProcessing(this);

        if (storedEnergy != storedEnergy2)
            container.sendToClient(getModuleID(), 1, storedEnergy);
        if (maxEnergy != maxEnergy2)
            container.sendToClient(getModuleID(), 2, maxEnergy);

        if (useEnergy != useEnergy2)
            container.sendToClient(getModuleID(), 5, useEnergy ? 1 : 0);

        storedEnergy2 = storedEnergy;
        maxEnergy2 = maxEnergy;
        useEnergy2 = useEnergy;
    }

    @Override
    public String getModuleID() {
        return "nineonetwo";
    }

    @Override
    protected void registerSlots() {

        for (int i = 0; i < 12; i++) {
            addSlot(new GuiSlot(this, i, 0, 0));
        }


        //addSlot(new GuiSlotOutput(container.player, this,2,0,0));


        if (!useEnergy) addSlot(new GuiSlot(this, 1, 0, 0));
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        return tile.getGuiSlot(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        tile.setGuiSlot(slotIndex, stack);
    }


    public int getStoredEnergy() {
        return storedEnergy;
    }


    public void setStoredEnergy(int storedEnergy) {
        this.storedEnergy = storedEnergy;
    }


    public int getMaxEnergy() {
        return maxEnergy;
    }


    public void setMaxEnergy(int maxEnergy) {
        this.maxEnergy = maxEnergy;
    }

    public boolean isUseEnergy() {
        return useEnergy;
    }

}

package com.tomevoll.routerreborn.lib.gui.slot;

import com.tomevoll.routerreborn.lib.gui.iface.IGuiModule;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class GuiSlotGhostMulti extends GuiSlotGhost {

    public GuiSlotGhostMulti(IGuiModule module, int index, int xPosition, int yPosition) {
        super(module, index, xPosition, yPosition);
    }

    /**
     * Helper method to put a stack in the slot.
     */
    public void putStack(ItemStack stack) {
        ItemStack itm;
        if (stack.isEmpty())
            itm = ItemStack.EMPTY;
        else
            itm = stack.copy();
        this.module.setSlotContents(this.getSlotIndex(), itm);
    }

    private void increaseStack(int amount) {
        ItemStack me = this.getStack();
        if (!me.isEmpty()) {
            me.setCount(me.getCount() + amount);
            putStack(me.copy());
        }
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack stack) {


        ItemStack its = this.getStack();

        if (!its.isEmpty()) {
            if (ItemStack.areItemsEqual(stack, its) && ItemStack.areItemStackTagsEqual(stack, its))
                if (!stack.isEmpty()) increaseStack(stack.getCount());
        }
        return its.isEmpty();

    }

    public boolean canTakeStack(EntityPlayer playerIn) {
        this.decrStackSize(1);
        return false;
    }


    public int getSlotStackLimit() {
        return 0;
    }
}

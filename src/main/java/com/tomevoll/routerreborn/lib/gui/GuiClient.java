package com.tomevoll.routerreborn.lib.gui;

import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiModule;
import com.tomevoll.routerreborn.lib.gui.iface.IItab;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.util.I18n;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.awt.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GuiClient extends GuiContainer implements IGuiController {
    public static final ResourceLocation GuiTextures = new ResourceLocation("routerrebornlib:textures/gui/libgui.png");
    private static final ResourceLocation GUI_ICONS = new ResourceLocation("routerrebornlib:textures/gui/icons.png");

    public ContainerServer container;
    public float scale = 1f;
    int activeTab = 0;
    int tim = 0;
    private int tabWidth = 21;
    private int tabHeight = 20;
    private int mx = 0;
    private int my = 0;

    public GuiClient(IGuiTile t, EntityPlayer player, World world, SimpleNetworkWrapper network) {

        super(new ContainerServer(t, player, world, network));
        container = (ContainerServer) inventorySlots;

    }

    public ModuleClientBase getActiveTab() {
        if (activeTab >= container.modules.getCount())
            activeTab = 0;
        if (container.modules.getCount() == 0)
            return null;

        return container.modules.getModuleByIndex(activeTab);
    }

    @Override
    public void initGui() {
        super.initGui();
        //buttonList.clear();
/*
        if (buttonList.size() > 0) {
			IGuiModule[] list = (IGuiModule[]) container.modules.getModules();
			for (int i = 0; i < list.length; i++) {
				((ModuleClientBase) list[i]).initGui(this);
			}
		} else {*/
        IGuiModule[] list = container.modules.getModules();
        int indx = 0;
        for (int i = 0; i < list.length; i++) {
            ((ModuleClientBase) list[i]).initGui(this);
            indx = ((ModuleClientBase) list[i]).registerButtons(indx, this);
        }
        //	}
    }

    @Override
    protected void keyTyped(char p_73869_1_, int keyId) throws IOException {
        ModuleClientBase tab = getActiveTab();
        if (tab == null) {
            super.keyTyped(p_73869_1_, keyId);
            return;
        }
        if (!tab.KeyPress(p_73869_1_, keyId)) {
            if (keyId == org.lwjgl.input.Keyboard.KEY_DELETE) {
                if (tab.canUninstall) {
                    container.sendToServer("container", 3, tab.getModuleID());
                    tab.HideContent(container);
                    container.modules.modules.remove(tab);
                    getActiveTab();
                    return;
                }
            }
            super.keyTyped(p_73869_1_, keyId);
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int par1, int par2) {
        super.drawGuiContainerForegroundLayer(par1, par2);

        GL11.glPushMatrix();
        GL11.glScalef(scale, scale, 1);
        int i = 0;
        for (IGuiModule tabs : container.modules.getModules()) {
            ModuleClientBase tab = (ModuleClientBase) tabs;

            DrawTabIcon(i);

            if (i != activeTab)
                tab.HideContent(container);
            else
                tab.Show(container);

            i++;
        }

        ModuleClientBase tab = getActiveTab();
        if (tab != null)
            tab.Draw();
        GL11.glPopMatrix();
    }

    public void drawProgressBar(int x, int y, int max, int val, boolean bkOnly) {
        int Left = (width - xSize) / 2;
        int Top = (height - ySize) / 2;
        mc.getTextureManager().bindTexture(GUI_ICONS);
        GlStateManager.color(1, 1, 1);
        GlStateManager.pushMatrix();
        GlStateManager.scale(0.25f, 0.25f, 1.0f);
        drawTexturedModalRect((Left + x) * 4, (Top + y) * 4, 96, 48, 96, 48);
        GlStateManager.popMatrix();

        if (val > 0 && !bkOnly) {

            float per = (float) val / (float) max;

            GlStateManager.pushMatrix();
            GlStateManager.scale(0.25f, 0.25f, 1.0f);
            drawTexturedModalRect((Left + x) * 4, (Top + y) * 4, 0, 48, (int) (96 * per), 48);
            GlStateManager.popMatrix();
        }
        GlStateManager.color(1, 1, 1);

    }

    public void drawBurnBar(int x, int y, int max, int val, boolean bkOnly) {
        int Left = (width - xSize) / 2;
        int Top = (height - ySize) / 2;
        mc.getTextureManager().bindTexture(GUI_ICONS);

        GlStateManager.pushMatrix();
        GlStateManager.scale(0.25f, 0.25f, 1.0f);
        drawTexturedModalRect((Left + x) * 4, (Top + y) * 4, 48, 0, 48, 48);
        GlStateManager.popMatrix();

        if (bkOnly)
            return;
        if (val <= 0 || max <= 0)
            return;

        float p = (float) val / (float) max;
        int w = (int) (48 * p);

        w = 48 - w;

        int l = Left + x;
        l = l * 4;
        int t = Top + y;
        t = t * 4;

        GlStateManager.pushMatrix();
        GlStateManager.scale(0.25f, 0.25f, 1.0f);
        drawTexturedModalRect(l, t + w, 0, w, 48, 48 - w);
        GlStateManager.popMatrix();
        GlStateManager.color(1, 1, 1);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        //super.drawBackground(0);
        super.drawDefaultBackground();
        if (!container.readyToUpdate) { // to not send custom packet until
            // container is available, messed up
            // sync on gui open

            container.sendToServer("container", 1, 0);
            container.readyToUpdate = true;

        }

        ModuleClientBase tab = getActiveTab();
        if (tab == null)
            return;
        GL11.glPushMatrix();
        GL11.glScalef(scale, scale, 1);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(GuiTextures);

        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;

        // k = k;

        for (int i = 0; i < container.modules.getCount(); i++) {
            if (activeTab != i)
                DrawInactiveTabBK(i);
        }

        int tabY = tab.getYSize();

        // top Draw
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, tabY - 5);

        this.drawTexturedModalRect(k, l + tabY - 5, 0, this.ySize - 5, this.xSize, 5);

        int darkBorder = new Color(80, 80, 80).getRGB();
        int lightBorder = new Color(255, 255, 255).getRGB();
        int cornerBorder = new Color(155, 155, 155).getRGB();
        int bk = new Color(150, 150, 150).getRGB();
        int x1, x2, y1, y2;
        for (int i = 0; i < container.inventorySlots.size(); i++) {
            if (i > 35)
                break; // only do player slots, tab will draw tab slots so it
            // wont if not visible,
            // we got no way to know what slot belong to a tab at this point.
            Slot s = container.inventorySlots.get(i);
            x1 = (k + s.xPos) * 2;
            y1 = (l + s.yPos) * 2;
            x2 = x1 + 32;
            y2 = y1 + 32;

            GL11.glPushMatrix();
            GL11.glScalef(scale / 2, scale / 2, scale);
            drawRect(x1, y1, x2, y2, bk);

            drawRect(x1, y1 - 1, x1 - 1, y2 + 1, darkBorder);
            drawRect(x1, y1, x2, y1 - 1, darkBorder);

            drawRect(x1, y2, x2, y2 + 1, lightBorder);
            drawRect(x2, y1 - 1, x2 + 1, y2 + 1, lightBorder);

            drawRect(x1, y2, x1 - 1, y2 + 1, cornerBorder);
            drawRect(x2, y1, x2 + 1, y1 - 1, cornerBorder);

            GL11.glPopMatrix();
        }

        GlStateManager.color(1, 1, 1);
        DrawActiveTabBK(activeTab);
        GlStateManager.color(1, 1, 1);
        tab.DrawBG();
        GlStateManager.color(1, 1, 1);
        GL11.glPopMatrix();
    }

    public void drawFilledRect(int x, int y, int width, int height, int borderColor, int fillColor) {

        int guiLeft = (this.width - this.xSize) / 2;
        int guiTop = (this.height - this.ySize) / 2;

        int x1 = (guiLeft + x) * 2;
        int y1 = (guiTop + y) * 2;
        int x2 = x1 + (width * 2);
        int y2 = y1 + (height * 2);

        GL11.glPushMatrix();
        GL11.glScalef(0.5f, 0.5f, 1.0f);
        drawRect(x1, y1, x2, y2, fillColor);

        drawRect(x1, y1 - 1, x1 - 1, y2 + 1, borderColor);
        drawRect(x1, y1, x2, y1 - 1, borderColor);

        drawRect(x1, y2, x2, y2 + 1, borderColor);
        drawRect(x2, y1 - 1, x2 + 1, y2 + 1, borderColor);

        // drawRect(x1, y2, x1 - 1, y2 + 1, borderColor);
        // drawRect(x2, y1, x2 + 1, y1 - 1, borderColor);

        GL11.glPopMatrix();
        GlStateManager.color(1, 1, 1);
    }

    public void draw3DBorder(int x, int y, int width, int height, boolean sunken, boolean filled) {

        int darkBorder = new Color(80, 80, 80).getRGB();
        int lightBorder = new Color(255, 255, 255).getRGB();
        int cornerBorder = new Color(155, 155, 155).getRGB();
        int bk = new Color(150, 150, 150).getRGB();

        int guiLeft = (this.width - this.xSize) / 2;
        int guiTop = (this.height - this.ySize) / 2;

        int x1 = (guiLeft + x) * 2;
        int y1 = (guiTop + y) * 2;
        int x2 = x1 + (width * 2);
        int y2 = y1 + (height * 2);

        GL11.glPushMatrix();
        GL11.glScalef(0.5f, 0.5f, 1.0f);
        if (filled)
            drawRect(x1, y1, x2, y2, bk);

        drawRect(x1, y1 - 1, x1 - 1, y2 + 1, sunken ? darkBorder : lightBorder);
        drawRect(x1, y1, x2, y1 - 1, sunken ? darkBorder : lightBorder);

        drawRect(x1, y2, x2, y2 + 1, sunken ? lightBorder : darkBorder);
        drawRect(x2, y1 - 1, x2 + 1, y2 + 1, sunken ? lightBorder : darkBorder);

        drawRect(x1, y2, x1 - 1, y2 + 1, cornerBorder);
        drawRect(x2, y1, x2 + 1, y1 - 1, cornerBorder);

        GL11.glPopMatrix();
        GlStateManager.color(1, 1, 1);
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);

        ModuleClientBase tab = getActiveTab();

        if (tab == null)
            return;

        if (tab != null)
            tab.drawScreen(mouseX, mouseY, partialTicks);
        GL11.glPushMatrix();
        GL11.glScalef(scale, scale, 1);

        tab.DrawTooltip();

        GL11.glPopMatrix();
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;

        int i = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int j = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;

        this.mx = i - k;
        this.my = j - l;

        int row = my < tabHeight && my > 0 ? 0 : my - this.ySize < tabHeight && my - this.ySize + 3 > 0 ? 1 : -1;
        int tindx = (mx - 1) / (tabWidth + 2);

        if (tindx > 6)
            return;
        if (row == 1)
            tindx += 7;

        if (tindx >= container.modules.getCount())
            return;
        if (row > -1) {
            if (i < k)
                return;
            this.mx = i;
            this.my = j;

            GL11.glPushMatrix();
            GL11.glScalef(scale, scale, 1);
            if (tindx > -1) {
                tab = container.modules.getModuleByIndex(tindx);
                if (tab != null && tab.GetTooltip() != null && tab.GetTooltip().length() > 0)
                    DrawTooltips(this.mc, I18n.translateToLocal(tab.GetTooltip()));
            }
            GL11.glPopMatrix();
        }


    }

    @Override
    protected void mouseClicked(int x, int y, int button) throws IOException {

        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;

        int mx = x - k;
        int my = y - l;

        int row = my < tabHeight && my > 0 ? 0 : my - this.ySize < tabHeight && my - this.ySize + 3 > 0 ? 1 : -1;

        if (row > -1 && mx > 0) {
            int tindx = (mx - 1) / (tabWidth + 2);

            if (tindx > 6)
                return;
            if (row == 1)
                tindx += 7;

            if (tindx != activeTab) {
                if (tindx < container.modules.getCount() && tindx >= 0) {

                    ModuleClientBase tab = getActiveTab();

                    if (tab != null)
                        tab.HideContent(container);
                    activeTab = tindx;
                    tab = getActiveTab();
                    if (tab != null)
                        tab.Show(container);

                    // this.Inventory.lastTab = activeTab;
                    this.mc.getSoundHandler()
                            .playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
                    // this.mc.getSoundHandler().playSound(PositionedSoundRecord.create(new
                    // ResourceLocation("gui.button.press"), 1.0F));
                }
            }
        } else if (my > tabHeight && my < this.height && mx > 0 && mx < this.width) {
            ModuleClientBase tab = getActiveTab();

            if (tab != null)
                tab.MouseClick(mx, my, button);
        }
        super.mouseClicked(x, y, button);
    }

    @Override
    protected void mouseReleased(int x, int y, int button) {

        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;

        this.mx = x - k;
        this.my = y - l;

        if (my > tabHeight && my < this.height && mx > 0 && mx < this.width) {
            ModuleClientBase tab = getActiveTab();
            if (tab != null)
                tab.MouseMove(mx, my);
        }
        super.mouseReleased(x, y, button);
    }

    @Override
    public void removeTab(IItab tab) {

    }

    @Override
    public void registerButton(GuiButton but) {
        buttonList.add(but);
    }

    @Override
    public void UnregisterButton(GuiButton but) {
        buttonList.remove(but);
    }

    @Override
    public ContainerServer getContainer() {
        return container;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getXsize() {
        return xSize;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getYsize() {
        return ySize;
    }

    @Override
    public Minecraft getMC() {
        return this.mc;
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        ModuleClientBase tab = getActiveTab();

        if (tab != null)
            tab.ActionPerformed(button);

        super.actionPerformed(button);
    }

    private void DrawInactiveTabBK(int index) {
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;

        if (index > 6)
            this.drawTexturedModalRect(k + ((index - 7) * (tabWidth + 2)), l + this.ySize - 1, 232, 24 + 2,
                    this.tabWidth + 3, this.tabHeight - 1);
        else
            this.drawTexturedModalRect(k + (index * (tabWidth + 2)), l, 232, 0, this.tabWidth + 2, this.tabHeight);
    }

    private void DrawActiveTabBK(int index) {
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;

        if (index > 6)
            this.drawTexturedModalRect(k + ((index - 7) * (tabWidth + 2)), l + this.ySize - 3, 232, 24,
                    this.tabWidth + 3, this.tabHeight + 3);
        else
            this.drawTexturedModalRect(k + (index * (tabWidth + 2)), l, 232, 0, this.tabWidth + 2, this.tabHeight + 3);
    }

    private void DrawTabIcon(int index) {

        if (index >= container.modules.getCount())
            return;
        ModuleClientBase tab = container.modules.getModuleByIndex(index);

        if (index > 6)
            this.drawItemStack2(tab.GetItem(), 1 + ((index - 7) * (tabWidth + 2)) + 3, this.ySize - 1);
        else
            this.drawItemStack2(tab.GetItem(), 1 + (index * (tabWidth + 2)) + 3, 3);
    }

    public void DrawTooltips(Minecraft mc, String text) {
        this.drawHoveringText(Arrays.asList(text), this.mx, this.my, mc.fontRenderer);
    }

    @Override
    protected void drawHoveringText(List p_146283_1_, int p_146283_2_, int p_146283_3_, FontRenderer font) {
        //super.drawHoveringText(p_146283_1_, p_146283_2_, p_146283_3_, font);

        if (!p_146283_1_.isEmpty()) {
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_DEPTH_TEST);

            int k = 0;
            Iterator iterator = p_146283_1_.iterator();

            while (iterator.hasNext()) {
                String s = (String) iterator.next();
                int l = font.getStringWidth(s);

                if (l > k) {
                    k = l;
                }
            }

            int j2 = p_146283_2_ + 12;
            int k2 = p_146283_3_ - 12;
            int i1 = 8;

            if (p_146283_1_.size() > 1) {
                i1 += 2 + (p_146283_1_.size() - 1) * 10;
            }

            this.zLevel = 200.0F;
            itemRender.zLevel = 200.0F;
            int j1 = -267386864;
            this.drawGradientRect(j2 - 3, k2 - 4, j2 + k + 3, k2 - 3, j1, j1);
            this.drawGradientRect(j2 - 3, k2 + i1 + 3, j2 + k + 3, k2 + i1 + 4, j1, j1);
            this.drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 + i1 + 3, j1, j1);
            this.drawGradientRect(j2 - 4, k2 - 3, j2 - 3, k2 + i1 + 3, j1, j1);
            this.drawGradientRect(j2 + k + 3, k2 - 3, j2 + k + 4, k2 + i1 + 3, j1, j1);
            int k1 = 1347420415;
            int l1 = (k1 & 16711422) >> 1 | k1 & -16777216;
            this.drawGradientRect(j2 - 3, k2 - 3 + 1, j2 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
            this.drawGradientRect(j2 + k + 2, k2 - 3 + 1, j2 + k + 3, k2 + i1 + 3 - 1, k1, l1);
            this.drawGradientRect(j2 - 3, k2 - 3, j2 + k + 3, k2 - 3 + 1, k1, k1);
            this.drawGradientRect(j2 - 3, k2 + i1 + 2, j2 + k + 3, k2 + i1 + 3, l1, l1);

            for (int i2 = 0; i2 < p_146283_1_.size(); ++i2) {
                String s1 = (String) p_146283_1_.get(i2);
                font.drawStringWithShadow(s1, j2, k2, -1);

                if (i2 == 0) {
                    k2 += 2;
                }

                k2 += 10;
            }

            this.zLevel = 0.0F;
            itemRender.zLevel = 0.0F;
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            RenderHelper.enableStandardItemLighting();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        }
    }

    protected void drawItemStack2(ItemStack stack, int x, int y) {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 0.0F, 32.0F);

        itemRender.zLevel = 50.0F;
        FontRenderer font = null;
        if (stack != null)
            font = stack.getItem().getFontRenderer(stack);
        if (font == null)
            font = fontRenderer;

        RenderHelper.enableGUIStandardItemLighting();
        itemRender.renderItemAndEffectIntoGUI(stack, x, y);

        this.zLevel = 0.0F;
        itemRender.zLevel = 0.0F;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glPopMatrix();
    }

    public void drawItemStack2(ItemStack stack, int x, int y, String altText) {
        GlStateManager.translate(0.0F, 0.0F, 32.0F);
        this.zLevel = 200.0F;
        this.itemRender.zLevel = 200.0F;
        net.minecraft.client.gui.FontRenderer font = null;
        if (stack != null)
            font = stack.getItem().getFontRenderer(stack);
        if (font == null)
            font = fontRenderer;
        this.itemRender.renderItemAndEffectIntoGUI(stack, x, y);
        this.itemRender.renderItemOverlayIntoGUI(font, stack, x, y, altText);
        this.zLevel = 0.0F;
        this.itemRender.zLevel = 0.0F;
        GlStateManager.color(1, 1, 1);
    }

    @Override
    public void drawEnergyBar(int x, int y, int width, int height, int maxEnergy, int storedEnergy) {

        if (storedEnergy > 0) {
            float per = (float) storedEnergy / (float) maxEnergy;
            int si = (int) (height * per);
            drawFilledRect(x, y + (height - si), width, height - (height - si), new Color(150, 0, 0).getRGB(),
                    new Color(150, 0, 0).getRGB());
        }
        // draw3DBorder(9, 27, 12, 52, false, false);
        draw3DBorder(x, y, width, height, true, false);
        GlStateManager.color(1, 1, 1);
    }

    @Override
    public void drawTooltipText(List<String> asList, int mouseX, int mouseY,
                                FontRenderer fontRendererObj) {

        drawHoveringText(asList, mouseX, mouseY, fontRendererObj);


    }

}

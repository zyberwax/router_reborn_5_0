package com.tomevoll.routerreborn.lib.gui.slot;

import com.tomevoll.routerreborn.lib.gui.iface.IGuiModule;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class GuiSlotGhost extends GuiSlot {

    public GuiSlotGhost(IGuiModule module, int index, int xPosition, int yPosition) {
        super(module, index, xPosition, yPosition);
    }

    public boolean canTakeStack(EntityPlayer playerIn) {
        this.putStack(null);
        return false; //prevent picking up ghost items
    }

    public int getSlotStackLimit() {
        return 64;
    }

    public void putStack(ItemStack stack) {
        ItemStack itm;
        if (stack == ItemStack.EMPTY)
            itm = ItemStack.EMPTY;
        else {
            itm = stack.copy();
            itm.setCount(1);
        }
        super.putStack(itm);
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack stack) {
        putStack(stack.copy());
        return false; //prevent eating items
    }


}

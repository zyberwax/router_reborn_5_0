package com.tomevoll.routerreborn.lib.gui.modules.EightenSlot;

import net.minecraft.item.ItemStack;

public interface IEightenSlotTile {

    void UpdateProcessing(EightenSlotServerModule eightenSlotServerModule);

    ItemStack getGuiSlot(int slotIndex);

    void setGuiSlot(int slotIndex, ItemStack stack);

}

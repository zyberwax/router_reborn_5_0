package com.tomevoll.routerreborn.lib.gui.iface;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;

public interface IClientGuiModule extends IGuiModule {

    void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2);

    void drawScreen(int mouseX, int mouseY, float partialTicks);
}

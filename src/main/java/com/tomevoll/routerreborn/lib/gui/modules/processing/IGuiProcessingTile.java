package com.tomevoll.routerreborn.lib.gui.modules.processing;

import net.minecraft.item.ItemStack;

public interface IGuiProcessingTile {
    ItemStack getProcessingSlot(int slot);

    void setProcessingSlot(int slot, ItemStack stack);

    float getProcessingXP(ItemStack stack);

    void UpdateProcessing(ProcessingServerModule handler);

}

package com.tomevoll.routerreborn.lib.gui.modules.inoutside;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.GuiButtonFlat;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import org.lwjgl.opengl.GL11;

public class InOutSideClientModule extends ModuleClientBase {


    public GuiButtonFlat upBtn;
    public GuiButtonFlat downBtn;
    public GuiButtonFlat westBtn;
    public GuiButtonFlat eastBtn;
    public GuiButtonFlat northBtn;
    public GuiButtonFlat southBtn;
    public GuiButtonFlat inoutBtn;
    public GuiButtonFlat slotmodeBtn;
    public GuiButtonFlat slotaddBtn;
    public GuiButtonFlat slotremBtn;
    IInOutSideTile tile;
    EnumFacing side;
    int InOutMode = 1;
    boolean slotmode = false;
    int slot = 0;

    public InOutSideClientModule(ItemStack icon, IInOutSideTile tile) {
        super(icon);
        this.tile = tile;
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        switch (cmd) {
            case 1:
                side = EnumFacing.VALUES[val2];
                break;
            case 2:
                InOutMode = val2;
                break;
            case 3:
                slotmode = val2 == 1;
                break;
            case 4:
                slot = val2;
            default:
                break;
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getModuleID() {
        return "inoutside";
    }

    @Override
    public void ActionPerformed(GuiButton btn) {
        int id = btn.id;

        if (id >= downBtn.id && id <= eastBtn.id) {
            container.sendToServer(getModuleID(), 1, id - downBtn.id);
        }
        if (id == inoutBtn.id) {
            if (InOutMode == 1)
                InOutMode = 0;
            else
                InOutMode = 1;
            container.sendToServer(getModuleID(), 2, InOutMode);
        }
        if (id == slotmodeBtn.id) {
            slotmode = !slotmode;
            container.sendToServer(getModuleID(), 3, slotmode ? 1 : 0);
        }
        if (id == slotaddBtn.id) {
            int i = 1;
            if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_RSHIFT) || org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_LSHIFT)) {
                i = 10;
            }
            slot += i;
            container.sendToServer(getModuleID(), 4, slot);

        }

        if (id == slotremBtn.id) {
            int i = 1;
            if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_RSHIFT) || org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_LSHIFT)) {
                i = 10;
            }
            slot -= i;
            if (slot < 0)
                slot = 0;

            container.sendToServer(getModuleID(), 4, slot);
        }

    }


    @Override
    protected void AddControls(IGuiController gui) {
        int x = Left;
        int y = Top;

        upBtn = new GuiButtonFlat(3, x + 130, 3 + y + 24 + 2, 15, 15, 2, 0);
        downBtn = new GuiButtonFlat(2, x + 130, y + 62 - 3 + 2, 15, 15, 2, 0);
        southBtn = new GuiButtonFlat(4, x + 130, y + 43 + 2, 15, 15, 2, 0);
        northBtn = new GuiButtonFlat(5, x + 149 - 3, y + 62 - 3 + 2, 15, 15, 2, 0);
        westBtn = new GuiButtonFlat(6, 3 + x + 111, y + 43 + 2, 15, 15, 2, 0);
        eastBtn = new GuiButtonFlat(7, x + 149 - 3, y + 43 + 2, 15, 15, 2, 0);

        inoutBtn = new GuiButtonFlat(8, x + 12, y + 29, 15, 15, 6, 0);

        slotmodeBtn = new GuiButtonFlat(9, 3 + x + 111 - 14 - 4, y + 43 + 2, 15, 15, 3, 0);
        slotaddBtn = new GuiButtonFlat(10, 3 + x + 111 - 14 - 4 - 14 - 4, y + 43 + 2 - 16, 15, 15, 4, 0);
        slotremBtn = new GuiButtonFlat(11, 3 + x + 111 - 14 - 4 - 14 - 4, y + 43 + 2 + 16, 15, 15, 5, 0);


        AddButton(downBtn);
        AddButton(upBtn);
        AddButton(northBtn);
        AddButton(southBtn);
        AddButton(westBtn);
        AddButton(eastBtn);
        AddButton(inoutBtn);
        AddButton(slotmodeBtn);
        AddButton(slotaddBtn);
        AddButton(slotremBtn);

        northBtn.ir = 0.2f;
        northBtn.ig = 0.6f;
        northBtn.ib = 0.2f;

        southBtn.ir = 0.2f;
        southBtn.ib = 0.6f;
        southBtn.ig = 0.2f;

        westBtn.ir = 0.6f;
        westBtn.ig = 0.2f;
        westBtn.ib = 0.2f;

        eastBtn.ir = 0.6f;
        eastBtn.ig = 0.6f;
        eastBtn.ib = 0.2f;

        inoutBtn.ir = 1f;
        inoutBtn.ig = 1f;
        inoutBtn.ib = 1f;
    }


    @Override
    public void DrawBG() {

        northBtn.br = 0.3f;
        northBtn.bg = 0.7f;
        northBtn.bb = 0.3f;
        northBtn.ba = 1f;

        southBtn.br = 0.3f;
        southBtn.bg = 0.7f;
        southBtn.bb = 0.3f;
        southBtn.ba = 1f;

        eastBtn.br = 0.3f;
        eastBtn.bg = 0.7f;
        eastBtn.bb = 0.3f;
        eastBtn.ba = 1f;

        westBtn.br = 0.3f;
        westBtn.bg = 0.7f;
        westBtn.bb = 0.3f;
        westBtn.ba = 1f;

        upBtn.br = 0.3f;
        upBtn.bg = 0.7f;
        upBtn.bb = 0.3f;
        upBtn.ba = 1f;

        downBtn.br = 0.3f;
        downBtn.bg = 0.7f;
        downBtn.bb = 0.3f;
        downBtn.ba = 1f;

        inoutBtn.br = 0.3f;
        inoutBtn.bg = 0.7f;
        inoutBtn.bb = 0.3f;
        inoutBtn.ba = 1f;

        northBtn.displayString = "North";
        southBtn.displayString = "South";
        westBtn.displayString = "West";
        eastBtn.displayString = "East";
        upBtn.displayString = "Top";
        downBtn.displayString = "Bottom";
        inoutBtn.displayString = InOutMode == 1 ? "Extract" : "Output";

        inoutBtn.setImageIndexes(InOutMode, 0);

        slotmodeBtn.displayString = "Slot/Side mode";
        slotaddBtn.displayString = "+1, Hold shift for +10";
        slotremBtn.displayString = "-1, Hold shift for -10";


        northBtn.selected = false;
        southBtn.selected = false;
        westBtn.selected = false;
        eastBtn.selected = false;
        upBtn.selected = false;
        downBtn.selected = false;
        inoutBtn.selected = false;


        if (!slotmode) {
            northBtn.enabled = true;
            southBtn.enabled = true;
            westBtn.enabled = true;
            eastBtn.enabled = true;
            upBtn.enabled = true;
            downBtn.enabled = true;

            slotaddBtn.enabled = false;
            slotremBtn.enabled = false;
        } else {
            northBtn.enabled = false;
            southBtn.enabled = false;
            westBtn.enabled = false;
            eastBtn.enabled = false;
            upBtn.enabled = false;
            downBtn.enabled = false;

            slotaddBtn.enabled = true;
            slotremBtn.enabled = slot > 0;

        }


        inoutBtn.enabled = true;

        if (side != null && !slotmode) {
            switch (side) {

                case UP:
                    upBtn.selected = true;
                    break;
                case DOWN:
                    downBtn.selected = true;
                    break;
                case NORTH:
                    northBtn.selected = true;
                    break;
                case SOUTH:
                    southBtn.selected = true;
                    break;
                case WEST:
                    westBtn.selected = true;
                    break;
                case EAST:
                    eastBtn.selected = true;
                    break;
                default:
                    //disBtn.selected = true;
                    break;
            }


        }


        // gui.drawRect(x1, y1, x2, y2, 0xff7d8c9d);
        // gui.drawRect(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 0xffa6a6a6);

        int x1 = Left + 75;
        int x2 = x1 + 89;
        int y1 = Top + 27;
        int y2 = y1 + 51;

        Gui.drawRect(x1, y1, x2, y2, 0xff7d8c9d);
        Gui.drawRect(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 0xffa6a6a6);

        x1 = Left + 8;
        x2 = x1 + 24;
        y2 = y1 + 39;
        Gui.drawRect(x1, y1, x2, y2, 0xff7d8c9d);
        Gui.drawRect(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 0xffa6a6a6);


        int slotX = 3 + Left + 111 - 14 - 4 - 14;
        int slotY = Top + 43 + 5;

        Minecraft.getMinecraft().fontRenderer.drawStringWithShadow("" + slot, slotX, slotY, slotmode ? 0xffffffff : 0xffa6a6a6);

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        super.DrawBG();
        // gui.getMC().fontRendererObj.drawString("Autoeject", 8, 25, 4210752);

    }

    @Override
    protected void registerSlots() {
        addSlot(new GuiSlot(this, 0, Left + 12, Top + 29 + 18));

    }

    @Override
    public ItemStack getStack(int slotIndex) {
        if (slotIndex == 0)
            return tile.getInOutSlotStack();
        else
            return super.getStack(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        if (slotIndex == 0)
            tile.setInOutSlotStack(stack);
        else
            super.setSlotContents(slotIndex, stack);
    }


    public boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        boolean flag = false;
        int i = startIndex;
        //reverseDirection = false;
        if (reverseDirection) {
            i = slots.size() - 1;
        }
        if (stack.getCount() > 0) {
            for (i = (reverseDirection ? slots.size() - 1 : 0); (reverseDirection ? i > -1
                    : i < slots.size()); i = (reverseDirection ? --i : ++i)) {
                Slot slot = slots.get(i).getSlot();
                ItemStack itemstack = slot.getStack();

                if (itemstack.isEmpty() && !stack.isEmpty()) // Forge:
                {
                    ItemStack st = stack.copy();
                    // st.setCount(1);
                    slot.putStack(st);
                    slot.onSlotChanged();
                    stack.setCount(0);
                    setSlotContents(0, st);
                    // stack.stackSize = 0;
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }


}

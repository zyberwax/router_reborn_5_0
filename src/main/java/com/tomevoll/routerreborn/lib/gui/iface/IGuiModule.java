package com.tomevoll.routerreborn.lib.gui.iface;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import net.minecraft.item.ItemStack;

public interface IGuiModule {
    String getModuleID();

    ItemStack getStack(int slotIndex);

    void setSlotContents(int slotIndex, ItemStack stack);

    int getStackLimit();

    void addSlots(ContainerServer c);

    float getSmeltingExperience(ItemStack stack);

    boolean mergeItemStack(ItemStack stackInSlot, int startIndex, int size, boolean reverse);

    enum CMD_TYPE {
        BUTTON_CLICK(1);

        public final int Value; // in meters

        CMD_TYPE(int val) {
            this.Value = val;
        }
    }


}

package com.tomevoll.routerreborn.lib.gui;


import com.tomevoll.routerreborn.lib.RouterRebornLib;
import com.tomevoll.routerreborn.lib.books.client.gui.GuiManual;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.network.C01_ContainerToClient;
import com.tomevoll.routerreborn.lib.gui.network.S01_ContainerToServer;
import com.tomevoll.routerreborn.lib.proxy.ClientProxy;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GuiRegistry implements IGuiHandler {
    public static GuiRegistry INSTANCE;
    private SimpleNetworkWrapper network;

    public GuiRegistry(SimpleNetworkWrapper network) {
        INSTANCE = this;
        this.network = network;

        network.registerMessage(S01_ContainerToServer.Handler.class, S01_ContainerToServer.class, 200, Side.SERVER);
        network.registerMessage(C01_ContainerToClient.Handler.class, C01_ContainerToClient.class, 201, Side.CLIENT);
        NetworkRegistry.INSTANCE.registerGuiHandler(RouterRebornLib.instance, this);
    }


    public Container createGuiContainerServer(IGuiTile t, EntityPlayer player, World world) {
        return new ContainerServer(t, player, world, network);
    }

    @SideOnly(Side.CLIENT)
    public GuiContainer createGuiContainerClient(IGuiTile t, EntityPlayer player, World world) {
        return new GuiClient(t, player, world, network);
    }


    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world,
                                      int x, int y, int z) {
        TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));
        if (entity instanceof IGuiTile)
            return GuiRegistry.INSTANCE.createGuiContainerServer((IGuiTile) entity, player, world);

        return null;
    }


    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world,
                                      int x, int y, int z) {
        TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));

        if (entity instanceof IGuiTile)
            return GuiRegistry.INSTANCE.createGuiContainerClient((IGuiTile) entity, player, world);

        if (ID == RouterRebornLib.manualGuiID) {
            ItemStack stack = player.getHeldItemMainhand();
            if (stack == null)
                System.out.println("Null stack in book.");
            if (stack != null && stack.getItem() == null)
                System.out.println("Null item in book.");
            if (stack != null && stack.getItem() != null && stack.getItem().getUnlocalizedName() == null)
                System.out.println("Null unlocalized name in book.");
            else {
                return new GuiManual(stack, ClientProxy.getBookDataFromStack(stack));
            }
        }


        return null;
    }


}

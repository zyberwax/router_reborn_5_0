package com.tomevoll.routerreborn.lib.gui.modules.inoutside;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleServerBase;
import com.tomevoll.routerreborn.lib.gui.slot.GuiSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;

public class InOutSideServerModule extends ModuleServerBase {

    IInOutSideTile tile;
    EnumFacing side;
    int inoutmode = -1;
    boolean slotMode = false;
    int slot = -1;

    public InOutSideServerModule(IInOutSideTile tile) {
        this.tile = tile;

    }

    @Override
    public void handleMessageFromClient(ContainerServer container, IGuiTile t, int cmd, String val1, int val2) {

        switch (cmd) {
            case 1:
                tile.setInOutSide(val2);
                break;
            case 2:
                tile.setInOutMode(val2);
                break;
            case 3:
                tile.setIsSlotMode(val2 == 1);
                break;
            case 4:
                tile.setInOutSlot(val2);
                break;
            default:
                break;
        }

    }

    @Override
    public void detectAndSendChanges(ContainerServer container, IGuiTile t) {
        EnumFacing s = EnumFacing.VALUES[tile.getInOutSide()];
        if (s != side) {
            container.sendToClient(getModuleID(), 1, s.ordinal());
            side = s;
        }
        int i = tile.getInOutMode();
        if (inoutmode != i) {
            container.sendToClient(getModuleID(), 2, i);
            inoutmode = i;
        }

        boolean isSlot = tile.getIsSlotMode();
        if (slotMode != isSlot) {
            slotMode = isSlot;
            container.sendToClient(getModuleID(), 3, slotMode ? 1 : 0);
        }

        int sl = tile.getInOutSlot();
        if (sl != slot) {
            slot = sl;
            container.sendToClient(getModuleID(), 4, slot);
        }
    }

    @Override
    public String getModuleID() {
        return "inoutside";
    }

    @Override
    protected void registerSlots() {
        addSlot(new GuiSlot(this, 0, 0, 0));
    }

    @Override
    public ItemStack getStack(int slotIndex) {
        if (slotIndex == 0)
            return tile.getInOutSlotStack();
        else
            return super.getStack(slotIndex);
    }

    @Override
    public void setSlotContents(int slotIndex, ItemStack stack) {
        if (slotIndex == 0)
            tile.setInOutSlotStack(stack);
        else
            super.setSlotContents(slotIndex, stack);
    }


}
package com.tomevoll.routerreborn.lib.gui.modules.eject;

import net.minecraft.util.EnumFacing;

public interface IEjectTile {

    EnumFacing getEjectVal();

    void setEjectValue(EnumFacing val);

}

package com.tomevoll.routerreborn.lib.gui.modules.redstone;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IGuiRedstoneTile {

    int getRedstoneMode();

    void setRedstoneMode(int mode);

    @SideOnly(Side.CLIENT)
    void RegisterRedstoneModes(RedstoneClientModule handler);

}

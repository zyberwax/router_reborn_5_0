package com.tomevoll.routerreborn.lib.gui.modules.eject;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.GuiButtonFlat;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import org.lwjgl.opengl.GL11;

public class EjectClientModule extends ModuleClientBase {

    public GuiButtonFlat upBtn;
    public GuiButtonFlat downBtn;
    public GuiButtonFlat westBtn;
    public GuiButtonFlat eastBtn;
    public GuiButtonFlat northBtn;
    public GuiButtonFlat southBtn;
    public GuiButtonFlat disBtn;
    IEjectTile tile;
    EnumFacing ejectSide;


    public EjectClientModule(ItemStack icon, IEjectTile tile) {
        super(icon);
        this.tile = tile;
    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        if (cmd == 1) {
            if (val2 == -1) {
                ejectSide = null;
            } else {
                ejectSide = EnumFacing.VALUES[val2];
            }
        }
    }

    @Override
    public String getModuleID() {
        return "eject";
    }

    @Override
    public void ActionPerformed(GuiButton btn) {
        int id = btn.id;

        if (id >= downBtn.id && id <= eastBtn.id) {
            container.sendToServer(getModuleID(), 1, id - downBtn.id);
        }
        if (id == disBtn.id) {
            container.sendToServer(getModuleID(), 1, -1);
        }

    }


    @Override
    protected void AddControls(IGuiController gui) {
        int x = Left;
        int y = Top;

        upBtn = new GuiButtonFlat(3, x + 130, 3 + y + 24 + 2, 15, 15, 2, 0);
        downBtn = new GuiButtonFlat(2, x + 130, y + 62 - 3 + 2, 15, 15, 2, 0);
        southBtn = new GuiButtonFlat(4, x + 130, y + 43 + 2, 15, 15, 2, 0);
        northBtn = new GuiButtonFlat(5, x + 149 - 3, y + 62 - 3 + 2, 15, 15, 2, 0);
        westBtn = new GuiButtonFlat(6, 3 + x + 111, y + 43 + 2, 15, 15, 2, 0);
        eastBtn = new GuiButtonFlat(7, x + 149 - 3, y + 43 + 2, 15, 15, 2, 0);
        disBtn = new GuiButtonFlat(8, x + 80, y + 45, 15, 15, 6, 0);

        AddButton(downBtn);
        AddButton(upBtn);
        AddButton(northBtn);
        AddButton(southBtn);
        AddButton(westBtn);
        AddButton(eastBtn);
        AddButton(disBtn);

        southBtn.ir = 0.2f;
        southBtn.ib = 0.6f;
        southBtn.ig = 0.2f;

        northBtn.ir = 0.2f;
        northBtn.ig = 0.6f;
        northBtn.ib = 0.2f;

        westBtn.ir = 0.6f;
        westBtn.ig = 0.2f;
        westBtn.ib = 0.2f;

        eastBtn.ir = 0.6f;
        eastBtn.ig = 0.6f;
        eastBtn.ib = 0.2f;

        disBtn.ir = 0.6f;
        disBtn.ig = 0.2f;
        disBtn.ib = 0.2f;


    }

    @Override
    protected void registerSlots() {
        // TODO Auto-generated method stub

    }


    @Override
    public void DrawBG() {
        super.DrawBG();
        northBtn.br = 0.3f;
        northBtn.bg = 0.7f;
        northBtn.bb = 0.3f;
        northBtn.ba = 1f;

        southBtn.br = 0.3f;
        southBtn.bg = 0.7f;
        southBtn.bb = 0.3f;
        southBtn.ba = 1f;

        eastBtn.br = 0.3f;
        eastBtn.bg = 0.7f;
        eastBtn.bb = 0.3f;
        eastBtn.ba = 1f;

        westBtn.br = 0.3f;
        westBtn.bg = 0.7f;
        westBtn.bb = 0.3f;
        westBtn.ba = 1f;

        upBtn.br = 0.3f;
        upBtn.bg = 0.7f;
        upBtn.bb = 0.3f;
        upBtn.ba = 1f;

        downBtn.br = 0.3f;
        downBtn.bg = 0.7f;
        downBtn.bb = 0.3f;
        downBtn.ba = 1f;

        disBtn.br = 0.3f;
        disBtn.bg = 0.7f;
        disBtn.bb = 0.3f;
        disBtn.ba = 1f;

        northBtn.displayString = "North";
        southBtn.displayString = "South";
        westBtn.displayString = "West";
        eastBtn.displayString = "East";
        upBtn.displayString = "Top";
        downBtn.displayString = "Bottom";
        disBtn.displayString = "Disable Eject";

        northBtn.selected = false;
        southBtn.selected = false;
        westBtn.selected = false;
        eastBtn.selected = false;
        upBtn.selected = false;
        downBtn.selected = false;
        disBtn.selected = false;

        northBtn.enabled = true;
        southBtn.enabled = true;
        westBtn.enabled = true;
        eastBtn.enabled = true;
        upBtn.enabled = true;
        downBtn.enabled = true;
        disBtn.enabled = true;

        if (ejectSide != null)
            switch (ejectSide) {
                case UP:
                    upBtn.selected = true;
                    break;
                case DOWN:
                    downBtn.selected = true;
                    break;
                case NORTH:
                    northBtn.selected = true;
                    break;
                case SOUTH:
                    southBtn.selected = true;
                    break;
                case WEST:
                    westBtn.selected = true;
                    break;
                case EAST:
                    eastBtn.selected = true;
                    break;
                default:
                    disBtn.selected = true;
                    break;
            }
        else
            disBtn.selected = true;

        // gui.drawRect(x1, y1, x2, y2, 0xff7d8c9d);
        // gui.drawRect(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 0xffa6a6a6);

        int x1 = Left + 75;
        int x2 = x1 + 89;
        int y1 = Top + 27;
        int y2 = y1 + 51;

        Gui.drawRect(x1, y1, x2, y2, 0xff7d8c9d);
        Gui.drawRect(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 0xffa6a6a6);

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);


        // gui.getMC().fontRendererObj.drawString("Autoeject", 8, 25, 4210752);
    }

    @Override
    public String GetTooltip() {
        return "Auto Eject";
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }

}

package com.tomevoll.routerreborn.lib.gui.network;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import io.netty.buffer.ByteBuf;
import net.minecraft.inventory.Container;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class S01_ContainerToServer implements IMessage {


    public String module;
    public int cmd;
    public String val1;
    public int val2;

    public S01_ContainerToServer() {

    }

    public S01_ContainerToServer(String module, int cmd, String val1, int val2) {
        this.module = module;
        this.cmd = cmd;
        this.val1 = val1;
        this.val2 = val2;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        module = ByteBufUtils.readUTF8String(buf);
        cmd = buf.readInt();
        val1 = ByteBufUtils.readUTF8String(buf);
        val2 = buf.readInt();

    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, module);
        buf.writeInt(cmd);
        ByteBufUtils.writeUTF8String(buf, val1);
        buf.writeInt(val2);
    }

    public static class Handler implements IMessageHandler<S01_ContainerToServer, IMessage> {

        @Override
        public IMessage onMessage(S01_ContainerToServer message, MessageContext ctx) {


            Container c = ctx.getServerHandler().player.openContainer;
            if (c instanceof ContainerServer) {
                ((ContainerServer) c).onMessageFromClient(message.module, message.cmd, message.val1, message.val2);
            }
            //ctx.getServerHandler().playerEntity.openContainer
            return null;
        }

    }


}

package com.tomevoll.routerreborn.lib.gui.modules.machinefilter;

import com.tomevoll.routerreborn.lib.gui.ContainerServer;
import com.tomevoll.routerreborn.lib.gui.control.GuiButtonFlat;
import com.tomevoll.routerreborn.lib.gui.control.GuiMultiList;
import com.tomevoll.routerreborn.lib.gui.control.GuiMultiList.ListItem;
import com.tomevoll.routerreborn.lib.gui.iface.IGuiController;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import com.tomevoll.routerreborn.lib.gui.modules.ModuleClientBase;
import com.tomevoll.routerreborn.lib.util.I18n;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;


/**
 * Created by Zyber on 17.12.2016.
 */
@SuppressWarnings({"deprecation"})
public class MachineFilterClientModule extends ModuleClientBase {
    GuiMultiList lst;
    boolean blacklist = false;
    boolean visitnear = false;
    private GuiButtonFlat btnup;
    private GuiButtonFlat btndown;
    private GuiButtonFlat btnwhitelist;
    private GuiButtonFlat btnvisit;

    public MachineFilterClientModule(ItemStack icon) {
        super(icon);
    }

    @Override
    public String getModuleID() {
        return "machinefilter";
    }

    @Override
    public String GetTooltip() {
        return "Machine Filter";
    }

    @Override
    protected void AddControls(IGuiController gui) {

        int k = Left;
        int l = (Top);

        this.lst = new GuiMultiList(1, Left + 10, Top + 27, 120, 50, "");

        btnup = new GuiButtonFlat(2, k + 131, l + 27, 10, 10, 4, 0);
        btndown = new GuiButtonFlat(3, k + 131, l + 67, 10, 10, 5, 0);
        btnwhitelist = new GuiButtonFlat(4, k + 153, l + 61, 17, 17, 7, 0);
        btnvisit = new GuiButtonFlat(5, k + 153, l + 41, 17, 17, 9, 0);

        AddButton(lst);
        AddButton(btnup);
        AddButton(btndown);
        AddButton(btnwhitelist);
        AddButton(btnvisit);
    }

    @Override
    protected void registerSlots() {

    }

    @Override
    public void handleMessageFromServer(ContainerServer containerServer, IGuiTile t, int cmd, String val1, int val2) {
        if (cmd == 1) {
            lst.lst.clear();
            String[] l = val1.split(":");

            for (String rName : l) {
                String name = rName.replace('|', ':');

                if (!name.endsWith(".name"))
                    name += ".name";
                @SuppressWarnings({"deprecation"})
                String lName = I18n.translateToLocal(name);
                if (lName.equalsIgnoreCase("") || lName == null)
                    lName = "NaN name";

                if (!rName.equalsIgnoreCase(""))
                    lst.lst.add(lst.new ListItem(lName, rName));
            }
        }

        if (cmd == 2) {
            String[] l = val1.split(":");
            lst.unselectAll();
            for (String rName : l) {
                lst.selectFromText(rName);
            }

        }
        if (cmd == 3) {
            visitnear = val2 == 1;
        }
        if (cmd == 4) {
            blacklist = val2 == 1;
        }
    }

    @Override
    public void DrawBG() {
        super.DrawBG();

        btnwhitelist.setImageIndexes(blacklist ? 8 : 7, 0);
        btnwhitelist.displayString = blacklist ? "Blacklist" : "Whitelist";

        btnvisit.setImageIndexes(visitnear ? 9 : 0, visitnear ? 0 : 1);
        btnvisit.displayString = visitnear ? "Visit Near" : "Visit All";

        btnup.enabled = lst.canScollUp;
        btndown.enabled = lst.canScrollDown;
    }

    @Override
    public void ActionPerformed(GuiButton btn) {

        if (btn.id == lst.id) {
            boolean changed = lst.Clicked(gui.getMC());

            if (changed) {
                String filt = "";
                for (ListItem itm : lst.getselectedindex()) {
                    filt += itm.realname + ":";
                }

                container.sendToServer(getModuleID(), 1, filt);
            }
        }
        if (btn.id == btnwhitelist.id) {
            blacklist = !blacklist;
            container.sendToServer(getModuleID(), 2, blacklist ? 1 : 0);
        }
        if (btn.id == btnvisit.id) {
            visitnear = !visitnear;
            container.sendToServer(getModuleID(), 3, visitnear ? 1 : 0);
        }

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // TODO Auto-generated method stub

    }
}

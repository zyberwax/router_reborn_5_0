package com.tomevoll.routerreborn.lib.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;


public class CraftingItem extends Item {
    public String modTexPrefix;
    public String[] textureNames;
    public String[] unlocalizedNames;
    public String folder;
    // public IIcon[] icons;

    public CraftingItem(String[] names, String[] tex, String folder, String modTexturePrefix, CreativeTabs tab) {
        super();
        this.modTexPrefix = modTexturePrefix;
        if (tab != null) {
            this.setCreativeTab(tab);
        }
        //this., damage), damage)(0);
        this.setHasSubtypes(false);
        this.textureNames = tex;
        this.unlocalizedNames = names;
        this.folder = folder;
    }

    public void updateData(String[] names, String[] tex, String folder, String modTexturePrefix) {
        this.modTexPrefix = modTexturePrefix;
        this.textureNames = tex;
        this.unlocalizedNames = names;
        this.folder = folder;
    }


    @Override
    public String getUnlocalizedName(ItemStack stack) {
        int arr = MathHelper.clamp(stack.getMetadata(), 0, unlocalizedNames.length);
        return getUnlocalizedName() + "." + unlocalizedNames[arr];
    }


    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> list) {
        for (int i = 0; i < unlocalizedNames.length; i++)
            if (!(textureNames[i].equals("")))
                list.add(new ItemStack(this, 1, i));
    }

}

package com.tomevoll.routerreborn.lib.item;

import com.tomevoll.routerreborn.lib.RouterRebornLib;
import com.tomevoll.routerreborn.lib.books.BookDataStore;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
@SuppressWarnings({"deprecation"})
public class Manual extends CraftingItem {

    static String[] name = new String[]{"beginner"};
    static String[] textureName = new String[]{"tinkerbook_diary", "tinkerbook_toolstation", "tinkerbook_smeltery", "tinkerbook_blue"};
    public final String modID;

    public Manual() {
        super(name, textureName, "", "routerrebornlib", null);
        modID = RouterRebornLib.MODID;
    }

    public Manual(String MODID) {
        super(name, textureName, "", "routerrebornlib", null);
        modID = MODID;
    }


    public Manual(String[] name, String[] textureName, String folder, String modTexturePrefix, CreativeTabs tab, String modID) {
        super(name, textureName, "", modTexturePrefix, tab);
        this.modID = modID;
    }

    public void updateManual() {
        updateData(new String[]{"test"}, new String[]{"mantlebook_blue"}, "", "mantle");
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        // TODO Auto-generated method stub
        player.openGui(RouterRebornLib.instance, RouterRebornLib.manualGuiID, world, 0, 0, 0);
        // ManualOpenEvent.Post postOpenEvent = new ManualOpenEvent.Post(stack, player);
        // MinecraftForge.EVENT_BUS.post(postOpenEvent);

        ItemStack r = player.getHeldItem(hand);
        return ActionResult.newResult(EnumActionResult.SUCCESS, r);
    }


    @Override
    @SideOnly(Side.CLIENT)

    @SuppressWarnings({"unchecked", "rawtypes"})

    public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        // TODO Auto-generated method stub

        tooltip.add("\u00a7o" + net.minecraft.util.text.translation.I18n.translateToLocal(BookDataStore.getBookfromID(stack.getMetadata()).toolTip));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }


    @Override
    public String getUnlocalizedName() {
        return modID + ":" + super.getUnlocalizedName();
    }

    @Override
    public String getUnlocalizedName(ItemStack par1ItemStack) {
        return super.getUnlocalizedName(par1ItemStack);
    }
}

package com.tomevoll.routerreborn.lib;


import com.tomevoll.routerreborn.lib.books.external.ZipLoader;
import com.tomevoll.routerreborn.lib.gui.GuiRegistry;
import com.tomevoll.routerreborn.lib.proxy.IProxy;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

@Mod(modid = RouterRebornLib.MODID, version = RouterRebornLib.VERSION, name = "RouterReborn Lib", acceptedMinecraftVersions = "1.12")
public class RouterRebornLib {
    public static final String MODID = "routerrebornlib";
    public static final String VERSION = "@VERSION@";
    public static SimpleNetworkWrapper network;
    public static Item mantleBook;

    @Instance(MODID)
    public static RouterRebornLib instance;


    @SidedProxy(clientSide = "com.tomevoll.routerreborn.lib.proxy.ClientProxy", serverSide = "com.tomevoll.routerreborn.lib.proxy.ServerProxy")
    public static IProxy proxy;
    public static int manualGuiID = 100;
    public static File config;

    public static void loadBookLocations() {
        for (File f : config.listFiles()) {
            if (f.isFile() && FilenameUtils.getExtension(f.getAbsolutePath()).equalsIgnoreCase("zip"))
                ZipLoader.loadZip(f);
        }
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.RegisterEvents();

        config = event.getModConfigurationDirectory();

        network = NetworkRegistry.INSTANCE.newSimpleChannel("RouterRebornLib");
        new GuiRegistry(network);


        loadBookLocations();

	         /*
             mantleBook = new Manual().setUnlocalizedName("manuals").setRegistryName(MODID + ":manuals.test");
	         ((Manual)mantleBook).updateManual();

	         ForgeRegistries.ITEMS.register(mantleBook);


	         BookData data = new BookData();
	         data.doc =  ManualReader.readManual("assets/routerrebornlib/manuals/test.xml");
	         data.unlocalizedName = "item.manuals.test";
	         data.modID = MODID;

	         BookDataStore.addBook(data);

			ForgeRegistries.ITEMS.register(mantleBook);
	    	 */
    }
}

package com.tomevoll.routerreborn.lib.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.util.List;

public class ItemFilter {

    private ItemStack[] slots = new ItemStack[9];

    private boolean useOreDict = true;
    private boolean whiteList = true;
    private boolean useMeta = true;
    private boolean isKeepStock = false;

    public ItemFilter(boolean useOreDict, boolean useMeta, boolean whiteList) {

        this.setUseOreDict(useOreDict);
        this.setUseMeta(useMeta);
        this.setWhiteList(whiteList);

        if (this.getUseOreDict())
            this.setUseMeta(false);

        for (int i = 0; i < 9; i++)
            slots[i] = ItemStack.EMPTY;

    }

    public NBTTagCompound writeToNBT(NBTTagCompound tag) {

        NBTTagList list = new NBTTagList();
        for (int i = 0; i < slots.length; i++) {
            NBTTagCompound t = new NBTTagCompound();
            ItemStack stack = this.slots[i];

            t.setByte("Slot", (byte) i);
            stack.writeToNBT(t);
            list.appendTag(t);
        }

        tag.setTag("itemfilter", list);

        tag.setBoolean("useOreDict", this.useOreDict);
        tag.setBoolean("whiteList", this.whiteList);
        tag.setBoolean("useMeta", this.useMeta);

        return tag;
    }

    public void readFromNBT(NBTTagCompound tagCompound) {
        int t = 0;
        NBTTagList tagList = tagCompound.getTagList("itemfilter", 10);
        for (int i = 0; i < tagList.tagCount(); i++) {
            NBTTagCompound tag = tagList.getCompoundTagAt(i);
            byte slot = tag.getByte("Slot");

            this.slots[slot] = new ItemStack(tag);
        }

        this.useOreDict = tagCompound.getBoolean("useOreDict");
        this.whiteList = tagCompound.getBoolean("whiteList");
        this.useMeta = tagCompound.getBoolean("useMeta");
    }

    public boolean getUseOreDict() {
        return useOreDict;
    }

    public void setUseOreDict(boolean useOreDict) {
        this.useOreDict = useOreDict;
        if (this.useOreDict)
            this.setUseMeta(false);
    }

    public boolean getWhiteList() {
        return whiteList;
    }

    public void setWhiteList(boolean whiteList) {
        this.whiteList = whiteList;
    }

    public boolean getUseMeta() {
        return useMeta;
    }

    public void setUseMeta(boolean useMeta) {
        this.useMeta = useMeta;
        if (this.useMeta)
            this.useOreDict = false;
    }

    public int getSlotCount() {
        return slots.length;
    }

    public ItemStack getSlot(int index) {
        return slots[index];
    }

    public void setSlot(int slot, ItemStack stack) {
        this.slots[slot] = stack;
    }

    public int CheckKeepStock(ItemStack item, List<ItemStack> itemsOrg) {
        if (item.isEmpty())
            return 0;

        List<ItemStack> items = TransferHelper.combineItemStacks(itemsOrg);

        for (int i = 0; i < 9; i++) {
            ItemStack it = slots[i];

            if (TransferHelper.isStacksEqual(it, item, true)) {
                for (ItemStack stack : items) {
                    if (TransferHelper.isStacksEqual(it, stack, true)) {
                        if (it.getCount() > stack.getCount())
                            return it.getCount() - stack.getCount();
                        else
                            return 0;
                    }
                }
                return it.getCount();
            }
        }
        return 0;
    }

    public boolean CheckFilter(ItemStack item) {

        if (item.isEmpty())
            return false;
        // if (con.type == ConnectionType.EXTRACT) filter.keepStock = false;

        for (int i = 0; i < 9; i++) {
            ItemStack it = slots[i];
            if (!it.isEmpty()) {
                if (useOreDict) {
                    if (TransferHelper.oreDictMatch(it, item) == true)
                        return whiteList;
                }

                if (it.getItem() == item.getItem()) {
                    if (!useMeta)
                        return whiteList;
                    else {
                        if (it.getItemDamage() == item.getItemDamage()) {
                            if (it.getTagCompound() == item.getTagCompound())
                                return whiteList;
                        }
                    }
                }
            }
        }
        return !whiteList;
    }

    public boolean getKeepStock() {
        return isKeepStock;
    }

    public void setKeepStock(boolean isKeepStock) {
        this.isKeepStock = isKeepStock;
    }

}

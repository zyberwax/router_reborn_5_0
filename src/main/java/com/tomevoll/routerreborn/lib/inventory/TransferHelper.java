package com.tomevoll.routerreborn.lib.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;
import java.util.List;

public class TransferHelper {

    public static boolean oreDictMatch(ItemStack stack, ItemStack stack2) {
        if (stack.isEmpty()) return false;
        if (stack2.isEmpty()) return false;

        int[] ore1 = OreDictionary.getOreIDs(stack);
        int[] ore2 = OreDictionary.getOreIDs(stack2);
        if (ore1 != null && ore2 != null && ore1.length != 0 && ore2.length != 0) {

            for (int s = 0; s < ore1.length; s++) {
                for (int t = 0; t < ore2.length; t++)
                    if (ore1[s] == ore2[t])
                        return true;
            }
        }
        return false;
    }


    public static List<ItemStack> getContentOfInventory(IItemHandler inventory) {
        List<ItemStack> lst = new ArrayList<ItemStack>();
        for (int s = 0; s < inventory.getSlots(); s++)
            lst.add(inventory.getStackInSlot(s).isEmpty() ? null : inventory.getStackInSlot(s).copy());

        return lst;
    }


    public static List<ItemStack> combineItemStacks(List<ItemStack> items) {
        List<Integer> markedIndexes = new ArrayList<Integer>();

        for (int i = 0; i < items.size(); ++i) {
            if (markedIndexes.contains(i)) {
                continue;
            }

            ItemStack item = items.get(i);

            for (int j = i + 1; j < items.size(); ++j) {
                if (markedIndexes.contains(j)) {
                    continue;
                }

                ItemStack other = items.get(j);

                if (TransferHelper.canCombine(item, other, false)) {
                    item.setCount(item.getCount() + other.getCount());

                    markedIndexes.add(j);
                }
            }
        }

        List<ItemStack> markedItems = new ArrayList<ItemStack>();

        for (int i : markedIndexes) {
            markedItems.add(items.get(i));
        }

        items.removeAll(markedItems);
        return items;
    }


    public static boolean canCombine(ItemStack target, ItemStack source) {
        return target.getItem() == source.getItem() && (target.getMetadata() == source.getMetadata() && (target.getCount() < target.getMaxStackSize() && ItemStack.areItemStackTagsEqual(target, source)));
    }

    public static boolean canCombine(ItemStack target, ItemStack source, boolean t) {

        if (target.isEmpty() || source.isEmpty()) return false;
        return target.getItem() == source.getItem() && (target.getMetadata() == source.getMetadata() && ItemStack.areItemStackTagsEqual(target, source));
    }

    public static boolean isStacksEqual(ItemStack stackIn1, ItemStack stackIn2, boolean ignoreSize) {
        if (stackIn1.isEmpty() && stackIn2.isEmpty()) return true;
        if (stackIn1.isEmpty() || stackIn2.isEmpty()) return false;
        if (ignoreSize)
            return ItemStack.areItemStacksEqual(stackIn1.copy().splitStack(1), stackIn2.copy().splitStack(1));
        else
            return ItemStack.areItemStacksEqual(stackIn1.copy(), stackIn2.copy());

    }

    //REWRITE

    public static boolean SlotHasTypeAndCanAcceptStack(InfoTile tile, EnumFacing facing, ItemStack stack, int slot) {
        IItemHandler handler = tile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, facing);
        if (handler != null) {
            boolean canAccept = false;
            boolean hasItem = false;

            if (handler.insertItem(slot, stack.copy(), true).getCount() < stack.getCount()) {
                canAccept = true;
            }

            if (!canAccept)
                return false;

            for (int x = 0; x < handler.getSlots(); x++) {
                if (handler.getStackInSlot(x).isItemEqual(stack)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean HasTypeAndCanAcceptStack(InfoTile tile, EnumFacing facing, ItemStack stack) {
        IItemHandler handler = tile.tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, facing);
        if (handler != null) {
            boolean canAccept = false;
            boolean hasItem = false;
            for (int x = 0; x < handler.getSlots(); x++) {
                if (handler.getStackInSlot(x).isItemEqual(stack)) {
                    if (handler.insertItem(x, stack.copy(), true).getCount() < stack.getCount()) {
                        return true;
                    }
                    hasItem = true;
                    if (canAccept)
                        return true;
                }
                if (handler.insertItem(x, stack.copy(), true).getCount() < stack.getCount()) {
                    canAccept = true;
                    if (hasItem)
                        return true;
                }

            }

        }
        return false;
    }

    public static int GetFirstHasAndCanAcceptStack(List<InfoTile> tiles, ItemStack stack, EnumFacing facing) {
        for (int i = 0; i < tiles.size(); i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                    facing);
            if (handler != null) {
                boolean canAccept = false;
                boolean hasItem = false;
                for (int x = 0; x < handler.getSlots(); x++) {
                    if (handler.getStackInSlot(x).isItemEqual(stack)) {
                        if (handler.insertItem(x, stack.copy(), true).getCount() < stack.getCount()) {
                            return i;
                        }
                        hasItem = true;
                        if (canAccept)
                            return i;
                    }
                    if (handler.insertItem(x, stack.copy(), true).getCount() < stack.getCount()) {
                        canAccept = true;
                        if (hasItem)
                            return i;
                    }

                }

            }
        }
        return -1;
    }

    public static int GetFirstCanAcceptStack(List<InfoTile> tiles, ItemStack stack, EnumFacing facing, int startIndex) {

        //Go from start index to end
        for (int i = startIndex; i < tiles.size(); i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                    facing);
            if (handler != null) {
                boolean canAccept = false;
                for (int x = 0; x < handler.getSlots(); x++) {
                    if (handler.insertItem(x, stack.copy(), true).getCount() < stack.getCount()) {
                        canAccept = true;
                        return i;
                    }
                }
            }
        }


        //from 0 to startIndex
        for (int i = 0; i < startIndex; i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,
                    facing);
            if (handler != null) {
                boolean canAccept = false;
                for (int x = 0; x < handler.getSlots(); x++) {
                    if (handler.insertItem(x, stack.copy(), true).getCount() < stack.getCount()) {
                        canAccept = true;
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public static int GetFirstSlotHasAndCanAcceptStack(List<InfoTile> tiles, ItemStack stack, int slot) {
        for (int i = 0; i < tiles.size(); i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            if (handler != null) {
                boolean canAccept = handler.insertItem(slot, stack.copy(), true).getCount() < stack.getCount();
                boolean hasItem = false;

                if (canAccept)
                    for (int x = 0; x < handler.getSlots(); x++) {
                        if (handler.getStackInSlot(x).isItemEqual(stack)) {
                            return i;
                        }
                    }

            }
        }
        return -1;
    }


    public static int GetFirstCanExtractFrom(List<InfoTile> tiles, ItemStack stack, int slot, EnumFacing side, ItemFilter filter, int startIndex) {
        for (int i = startIndex; i < tiles.size(); i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            if (handler != null) {
                if (CanExtractFrom(handler, stack, slot, filter))
                    return i;
            }
        }
        for (int i = 0; i < startIndex; i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, side);
            if (handler != null) {
                if (CanExtractFrom(handler, stack, slot, filter))
                    return i;
            }
        }
        return -1;
    }


    public static boolean CanExtractFrom(IItemHandler handler, ItemStack stackToMatch, int slot, ItemFilter filter) {

        if (!stackToMatch.isEmpty() && stackToMatch.getCount() >= stackToMatch.getMaxStackSize())
            return false;

        if (slot > -1) {
            if (slot >= handler.getSlots()) return false;
            ItemStack ret = handler.extractItem(slot, 64, true);
            if (!ret.isEmpty()) {
                if (filter != null && !filter.CheckFilter(ret)) return false;
                if (stackToMatch.isEmpty()) {
                    return true;
                }
                return ItemStack.areItemsEqual(ret, stackToMatch);
            } else
                return false;
        } else {
            for (int x = 0; x < handler.getSlots(); x++) {
                ItemStack ret = handler.extractItem(x, 64, true);
                if (!ret.isEmpty()) {

                    if (stackToMatch.isEmpty()) {
                        if ((filter != null && filter.CheckFilter(ret)) || filter == null) {
                            return true;
                        }
                    } else {
                        if (ItemStack.areItemsEqual(ret, stackToMatch)) {
                            if ((filter != null && filter.CheckFilter(ret)) || filter == null) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static ItemStack ExtractFrom(IItemHandler handler, ItemStack target, int slot, ItemFilter filter, int maxExtract) {


        int limit = target.isEmpty() ? 64 : target.getMaxStackSize() - target.getCount();

        limit = Math.min(limit, maxExtract);

        if (limit <= 0)
            return ItemStack.EMPTY;

        if (!target.isEmpty() && filter != null && !filter.CheckFilter(target)) return ItemStack.EMPTY;


        if (slot > -1) {
            if (slot >= handler.getSlots()) return ItemStack.EMPTY;
            ItemStack ret = handler.extractItem(slot, 64, true);
            if (!ret.isEmpty()) {
                if (filter != null && !filter.CheckFilter(ret)) return ItemStack.EMPTY;
                if (target.isEmpty()) {
                    return handler.extractItem(slot, limit, false);
                }
                if (ItemStack.areItemsEqual(ret, target)) {
                    return handler.extractItem(slot, limit, false);
                } else
                    return ItemStack.EMPTY;
            } else
                return ItemStack.EMPTY;
        } else {
            for (int x = 0; x < handler.getSlots(); x++) {
                ItemStack ret = handler.extractItem(x, 64, true);
                if (!ret.isEmpty()) {

                    if (target.isEmpty()) {
                        if ((filter != null && filter.CheckFilter(ret)) || filter == null) {
                            return handler.extractItem(x, limit, false);
                        }
                    } else {
                        if (ItemStack.areItemsEqual(ret, target)) {
                            if ((filter != null && filter.CheckFilter(ret)) || filter == null) {
                                return handler.extractItem(x, limit, false);
                            }
                        }
                    }
                }
            }
        }
        return ItemStack.EMPTY;
    }

    public static int GetFirstSlotCanAcceptStack(List<InfoTile> tiles, ItemStack stack, int slot, int startIndex) {
        //from startIndex to end
        for (int i = startIndex; i < tiles.size(); i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            if (handler != null) {
                boolean canAccept = handler.insertItem(slot, stack.copy(), true).getCount() < stack.getCount();
                if (canAccept)
                    return i;
            }
        }

        for (int i = 0; i < startIndex; i++) {
            IItemHandler handler = tiles.get(i).tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            if (handler != null) {
                boolean canAccept = handler.insertItem(slot, stack.copy(), true).getCount() < stack.getCount();
                if (canAccept)
                    return i;
            }
        }
        return -1;
    }

    public static ItemStack insertStackToSlot(IItemHandler handler, ItemStack stack, int maxCount, int slot) {
        int count = maxCount;
        ItemStack toTransfer = stack.copy();
        toTransfer.setCount(Math.min(count, stack.getCount()));
        count = toTransfer.getCount();

        toTransfer = handler.insertItem(slot, toTransfer, false);

        count = count - toTransfer.getCount();
        toTransfer = stack.copy();
        toTransfer.setCount(stack.getCount() - count);
        if (toTransfer.getCount() <= 0)
            return ItemStack.EMPTY;
        else
            return toTransfer.copy();

    }

    public static ItemStack insertStackToAny(IItemHandler handler, ItemStack stack, int maxCount) {
        int count = maxCount;
        ItemStack toTransfer = stack.copy();
        toTransfer.setCount(Math.min(count, stack.getCount()));
        count = toTransfer.getCount();

        for (int i = 0; i < handler.getSlots(); i++) {
            toTransfer = handler.insertItem(i, toTransfer, false);
            if (toTransfer.isEmpty()) {
                break;
            }
        }

        count = count - toTransfer.getCount();
        toTransfer = stack.copy();
        toTransfer.setCount(stack.getCount() - count);
        if (toTransfer.getCount() <= 0)
            return ItemStack.EMPTY;
        else
            return toTransfer.copy();

    }


}

package com.tomevoll.routerreborn.lib.inventory;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

import java.util.ArrayList;
import java.util.List;

public class TileFilter {

    public List<String> filterList = new ArrayList<String>();
    public boolean BlackList = false;
    public boolean VisitNear = false;
    List<Class<?>> passThrew;
    private boolean isDummy = false;

    public TileFilter() {
        isDummy = true;
    }


    public TileFilter(List<String> tileNames, List<Class<?>> passThrew, boolean isBlackList) {
        this.filterList = tileNames;
        this.passThrew = passThrew;
        this.BlackList = isBlackList;
    }

    public TileFilter Copy() {
        TileFilter t = new TileFilter(new ArrayList<String>(), new ArrayList<Class<?>>(), BlackList);

        t.filterList.addAll(filterList);
        t.passThrew.addAll(passThrew);
        t.BlackList = BlackList;
        t.VisitNear = VisitNear;

        t.isDummy = isDummy;
        return t;
    }

    public NBTTagCompound writeToNBT(NBTTagCompound tag) {
        NBTTagCompound c = new NBTTagCompound();
        c.setBoolean("blacklist", BlackList);
        c.setBoolean("visitnear", VisitNear);


        NBTTagList list = new NBTTagList();
        for (int i = 0; i < filterList.size(); i++) {
            NBTTagCompound t = new NBTTagCompound();
            t.setString("value", filterList.get(i));

            list.appendTag(t);
        }

        c.setTag("filterlist", list);
        tag.setTag("machinefilter", c);

        return tag;
    }

    public void readFromNBT(NBTTagCompound tag) {
        NBTTagCompound c = tag.getCompoundTag("machinefilter");
        BlackList = c.getBoolean("blacklist");
        VisitNear = c.getBoolean("visitnear");
        filterList.clear();
        NBTTagList list = c.getTagList("filterlist", 10);
        for (int i = 0; i < list.tagCount(); i++) {
            NBTTagCompound t = list.getCompoundTagAt(i);
            filterList.add(t.getString("value"));

        }
    }

    public boolean isAllowed(InfoTile tile) {
        if (isDummy) return true;

        if (BlackList)
            return !isInFilter(tile.invName);
        else
            return isInFilter(tile.invName);
    }


    public boolean isPassThrew(TileEntity tile) {
        if (isDummy) return false;
        return passThrew.stream().filter(t -> t.isInstance(tile)).findAny().isPresent();
    }

    private boolean isInFilter(String tileName) {
        return filterList.stream().filter(t -> t.equalsIgnoreCase(tileName)).findAny().isPresent();
    }

}

package com.tomevoll.routerreborn.lib.inventory;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class InfoTile {
    public String invName = "";
    public TileEntity tile;
    public BlockPos pos;
    public boolean isPassthrew = false;

    public InfoTile(TileEntity tile, World worldObj, BlockPos position, boolean isPassthrew) {
        this.tile = tile;
        this.invName = getTileName(tile, worldObj);
        this.pos = position;
        this.isPassthrew = isPassthrew;
    }

    public String getTileName(TileEntity tile, World worldObj) {

        if (tile == null)
            return "NULL";

        Block blc = tile.getBlockType();

        try {
            IBlockState state = worldObj.getBlockState(tile.getPos());

            int meta = blc.getMetaFromState(state);

            Item it = blc.getItemDropped(state, null, 0);

            ItemStack stack = new ItemStack(it, 1, meta);

            // blc.getSubBlocks(Item.getItemFromBlock(blc), null, ls);
            if (!stack.isEmpty() && stack.getItem() != null) {
                // int meta = tile.getBlockMetadata();

                String invname = stack.getUnlocalizedName();
                return invname.replace(':', '|');
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return blc.getUnlocalizedName();
    }
}

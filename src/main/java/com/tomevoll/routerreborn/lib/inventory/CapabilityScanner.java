package com.tomevoll.routerreborn.lib.inventory;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static java.util.stream.Collectors.toList;

public class CapabilityScanner {
    private final List<EnumFacing> face = Arrays.asList(EnumFacing.VALUES);
    public TileFilter filter;
    Capability<?> cap;
    private List<InfoTile> scanned;
    private Thread thread;
    // private boolean shedulenewRun = false;
    private BlockPos myPos;
    private int limitscan = 0;
    private int scannedCount = 0;
    private World w;
    private Stack<shedule> shedules = new Stack<shedule>();

    /**
     * Background thread scanner, use this for normal scans, for gui list use {@link #scanForConnectedTilesSTA}
     *
     * @param types        List of classes we will scan for, if you have passthrew in the filter, they must be included here or they will be skipped
     * @param world
     * @param position     Initial position, normally the tile intializing the scan meaning you
     * @param limitScanner Number of Blocks to scan each time the scanner is ticked, will scan all 6 sides every tick with a value of 1, 0 for no limit
     * @param filter       Filter to be used or null for no filtering
     * @return {@link InfoTile} list containing TileEntity and the Localized name
     */
    public void scanForCapability(Capability<?> cap, World world, BlockPos position, int limitScanner, TileFilter filter) {


        //System.out.println("CALLED SCAN FROM THREAD: " + Thread.currentThread().getName());

        if (filter == null)
            filter = new TileFilter();

        if (thread != null && thread.getState() != Thread.State.TERMINATED) {
            shedules.clear(); //TODO: never more then 1 for now, if i find that we need this remove this line, if not remove the list and use just the class
            shedule s = new shedule();
            s.limitscan = limitScanner;
            s.myPos = position;
            s.cap = cap;
            s.SheduleWorld = world;
            s.filter = filter;

            shedules.push(s);
            return;
        }

        this.filter = filter;
        this.cap = cap;
        w = world;
        limitscan = limitScanner;
        scanned = new ArrayList<InfoTile>();
        myPos = position;
        thread = new Thread(() -> getAllConnectedWithCapability(cap, world, position, this.filter));
        thread.setDaemon(false);
        thread.start();
    }

    private List<InfoTile> getAllConnectedWithCapability(Capability<?> cap2, World world, BlockPos position,
                                                         TileFilter filter2) {


        List<InfoTile> mSet = face.stream()
                .map(f -> getTile(position, world, f.getDirectionVec()))
                .filter(t -> t != null)
                .filter(t -> hasCapability(cap2, t) || filter.isPassThrew(t))
                .filter(t -> !Contains(scanned, t))
                .map(t -> new InfoTile(t, world, t.getPos(), filter.isPassThrew(t)))
                .filter(t -> CheckFilter(t, filter))
                .collect(toList());

        scanned.addAll(mSet);

        // ################################################
        // ** THeREADING WAIT/SLEEP
        // ################################################
        if (limitscan > 0) {
            scannedCount++;

            if (scannedCount >= limitscan) {
                scannedCount = 0;
                try {
                    synchronized (Thread.currentThread()) {
                        Thread.currentThread().wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IllegalMonitorStateException e) {
                    e.printStackTrace();
                }
            }
        }
        // ##############################################

        List<InfoTile> mSet2 = mSet.stream()
                .map(t -> getAllConnectedWithCapability(cap2, world, t.tile.getPos(), filter))
                .reduce((List<InfoTile> t, List<InfoTile> r) -> { // Converting
                    // List<List<ScannedTiles>>()
                    // to
                    // List<ScannedTiles>
                    if (r != null && t != null)
                        r.addAll(t);
                    // System.out.println("T is: " + t);
                    // System.out.println("R is: " + r);
                    return r;
                }).orElse(new ArrayList<InfoTile>());

        mSet.addAll(mSet2);
        return mSet;
    }

    private boolean hasCapability(Capability<?> cap2, TileEntity t) {

        if (t == null) return false;

        for (int i = 0; i < 6; i++) {
            if (t.hasCapability(cap2, EnumFacing.VALUES[i]))
                return true;
        }

        return false;

    }

    /**
     * Should only be used for gui scans, this will lag the server for a sec in big setups<br> DO NOT USE FOR NOTMAL SCANS<br>
     * For normal scans use {@link #scanForConnectedTiles}
     *
     * @param types    List of classes we will scan for
     * @param world
     * @param position
     * @return {@link InfoTile} list containing TileEntity and the Localized name
     */
    public List<InfoTile> scanForConnectedCapabilitySTA(Capability<?> cap, World world, BlockPos position, List<Class<?>> PassThrew) {

        //System.out.println("CALLED SCAN STA FROM THREAD: " + Thread.currentThread().getName());

        //TODO: Do we really want to que more then 1, is there even a point in doing so ?
        if (thread != null && thread.getState() != Thread.State.TERMINATED) {

            //System.out.println("Scan Thread suspended and qued..");
            shedule s = new shedule();
            s.limitscan = this.limitscan;
            s.myPos = this.myPos;
            s.cap = this.cap;
            s.SheduleWorld = this.w;
            s.filter = this.filter;
            shedules.push(s);

            thread.interrupt();

            thread = null;
        }

        this.filter = new TileFilter(new ArrayList<String>(), PassThrew, true);
        this.cap = cap;
        w = world;
        limitscan = 0;
        scanned = new ArrayList<InfoTile>();
        myPos = position;
        //System.out.println("Inventory single thread scan started..");
        getAllConnectedWithCapability(cap, world, position, this.filter);
        //	System.out.println("Inventory single thread scan ended..");
        return getResult();
    }

    private boolean isPassrhrew(InfoTile t) {
        return t.isPassthrew;
    }

    private boolean isAllowedInFilter(InfoTile t) {
        return filter.isAllowed(t);
    }

    public List<InfoTile> getResult() {
        if (thread == null || (thread != null && thread.getState() == Thread.State.TERMINATED)) {

            if (thread != null) {
                thread.interrupt();

            }

            thread = null;
            removeMe(myPos);

            List<InfoTile> retList = new ArrayList<InfoTile>();

            for (InfoTile infoTile : scanned) {
                if (!isPassrhrew(infoTile)) {
                    if (isAllowedInFilter(infoTile)) {
                        retList.add(infoTile);
                    }

                }
            }
            scanned.clear();
            return retList;
        }
        return null;
    }

    private void removeMe(BlockPos myPos2) {
        InfoTile st = scanned.stream().filter(t -> t.pos.equals(myPos2)).findFirst().orElse(null);
        if (st != null)
            scanned.remove(st);
    }

    public boolean isDone() {
        if (thread == null)
            return false;

        return thread.getState() == Thread.State.TERMINATED;
    }

    public void Tick() {
        if (thread == null || thread.getState() == Thread.State.TERMINATED)
            if (!shedules.isEmpty()) {
                shedule s = shedules.pop();
                //System.out.println("Qued scan thread started..");
                scanForCapability(s.cap, s.SheduleWorld, s.myPos, s.limitscan, s.filter);
                return;
            }

        if (thread != null && thread.getState() == Thread.State.WAITING) {
            synchronized (thread) {
                thread.notify();
            }
        }
    }

    protected boolean CheckFilter(InfoTile tile, TileFilter filter) {
        if (filter.VisitNear) {
            tile.isPassthrew = filter.isPassThrew(tile.tile);
            return tile.isPassthrew || filter.isAllowed(tile);
        } else {
            return true; // we filter later if not visit near or we wont scan it all
        }
    }

    private boolean Contains(List<InfoTile> scanned2, TileEntity tile) {
        return scanned.stream().filter(t -> t.tile == tile).findAny().isPresent();

    }

    private TileEntity getTile(BlockPos pos, World world, Vec3i offset) {
        return world.getTileEntity(pos.add(offset));
    }

    private class shedule {
        public World SheduleWorld;
        public Capability<?> cap;
        public int limitscan = 0;
        public BlockPos myPos;
        public TileFilter filter;
    }


}

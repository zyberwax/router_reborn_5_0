package com.tomevoll.routerreborn.lib;

import com.tomevoll.routerreborn.lib.books.client.BookClientRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import org.apache.logging.log4j.Level;

import java.util.*;

public class RecipeHandler {

    public static Map<String, ItemStack> maps = new HashMap<String, ItemStack>();
    static String[] letters = {"a", "b", "c", "d", "e", "f", "g", "h", "i"};

    public static void init() {


        if (maps.isEmpty()) {
         //   FMLLog.log("RouterReborn", Level.INFO, "Starting recipe mapping...");

            for (Item it : Item.REGISTRY) {

                if (it != null) {
                    String name = it.getUnlocalizedName();
                    if (name != null && name.length() > 5) {
                        name = name.substring(5);
                        ItemStack item = new ItemStack(it);
                        if (item != null && item.getItem() != null)
                            maps.put(name, item);
                    }
                }
            }

            for (Block it : Block.REGISTRY) {
                if (it != null) {
                    String name = it.getUnlocalizedName();
                    if (name != null && name.length() > 5) {
                        name = name.substring(5);
                        ItemStack item = new ItemStack(it);
                        if (item != null && item.getItem() != null)
                            maps.put(name, item);
                    }
                }
            }

            for (String string : OreDictionary.getOreNames()) {
                String name = string;
                List<ItemStack> item = OreDictionary.getOres(string, false);
                if (item != null && !item.isEmpty()) {

                    for (ItemStack itemStack : item) {

                        if (itemStack != null && itemStack.getItem() != null) {
                            maps.put(string, item.get(0));
                            break;
                        }
                    }


                }

            }

          //  FMLLog.log("RouterReborn", Level.INFO,
           //         "Maping done with " + maps.size() + " entries.");

        }

    }

    public static ItemStack getItemStack(String strItem) {
        String[] parts = strItem.split("@");

        String item = parts[0].trim();

        boolean hasMeta = false;
        boolean hasCount = false;

        int meta = 0;
        int count = 1;

        if (parts.length > 1) {
            hasMeta = true;
            if (parts[1].contains("#")) {
                hasCount = true;
                String[] parts2 = parts[1].split("#");
                try {
                    meta = Integer.parseInt(parts2[0]);
                } catch (NumberFormatException e) {
                    return null;
                }

                try {
                    count = Integer.parseInt(parts2[1]);
                } catch (NumberFormatException e) {
                    return null;
                }

            } else {
                try {
                    meta = Integer.parseInt(parts[1]);
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        } else {
            if (parts[0].contains("#")) {
                hasCount = true;
                String[] parts2 = parts[0].split("#");
                try {
                    count = Integer.parseInt(parts2[1]);
                } catch (NumberFormatException e) {
                    return null;
                }
                item = parts2[0].trim();
            }
        }

        //     if (!maps.containsKey(item)) {
        // System.out.println(maps);
        //        return null;
        //     }


        // Item ii = Blocks.REDSTONE_BLOCK;
        Item i = Item.REGISTRY.getObject(new ResourceLocation(item));
        if (i == null) {
            Block b = Block.REGISTRY.getObject(new ResourceLocation(item));
            if (b != null)
                i = Item.getItemFromBlock(b);

        }


        if (i == null)
            return null;


        //ItemStack itm = maps.get(item);

        ItemStack itm = new ItemStack(i, 1);


        if (hasMeta)
            itm.setItemDamage(meta);

        if (hasCount)
            itm.setCount(count);

        return itm;

    }

    public static boolean registerRecipe(String[] recipe) {
        RECIPE_TYPE rtype = getRecipeType(recipe);

        if (rtype == RECIPE_TYPE.DISABLED)
            return true;

        int letter = 0;
        // shaped, item, null, item, null, null, null, item, item, item
        List<Object> rec = new ArrayList<Object>();
        List<String> rows = new ArrayList<String>();
        List<Object> dec = new ArrayList<Object>();

        ItemStack result = getItemStack(recipe[1].trim());
        if (result == null) {
        /*    FMLLog.log(
                    "RouterReborn",
                    Level.ERROR,
                    "Error in recipe, Result item: '"
                            + recipe[1]
                            + "' is unknown or error in format, Format expected is: <disable|shaped|shapeless>, <result>@meta#count, <input>@mets#count, ... ");
            FMLLog.log("RouterReborn", Level.ERROR,
                    "Or Item has been disabled.");
*/
            return false;

        }

        int limit = 3;

        if (recipe.length - 2 == 4)
            limit = 2;

        if (rtype == RECIPE_TYPE.ERROR)
            return false;

        String cLine = "";
        for (int x = 2; x < recipe.length && x < 11; x++) {
            String cItem = recipe[x].trim();


            if (cItem.equalsIgnoreCase("null") || cItem.length() == 0) {
                cLine += " ";
                //  rec.add(null);
            } else {
                Object r = null;
                if (OreDictionary.doesOreNameExist(cItem)) {
                    r = cItem;
                } else
                    r = getItemStack(cItem);

                if (r == null) {

               /*     FMLLog.log("RouterReborn", Level.ERROR,
                            "Error in recipe for: '" + result.getDisplayName()
                                    + "' Item: '" + cItem + "' is unknown");
                 */
                    return false;
                }

                cLine += letters[letter];

                rec.add(r);

                if (rtype == RECIPE_TYPE.SHAPED) {
                    dec.add(letters[letter].charAt(0));
                    dec.add(r);
                } else
                    dec.add(r);

                letter++;
            }
            if (cLine.length() == limit) {
                rows.add(cLine);
                cLine = "";
            }

        }

        if (rtype == RECIPE_TYPE.SHAPED) {
            List<Object> f = new ArrayList<Object>();

            for (String row : rows) {
                f.add(row);
            }

            for (Object o : dec) {
                f.add(o);
            }

            GameRegistry.addShapedRecipe(new ResourceLocation("String"), new ResourceLocation("recipes"), result, f.toArray());
          //  FMLLog.log("RouterReborn", Level.INFO, "Added Shaped Recipe for: "
                    //+ result.getDisplayName());
        }
        if (rtype == RECIPE_TYPE.SHAPELESS) {
            List<Object> f = new ArrayList<Object>();
            for (Object o : dec) {
                f.add(o);
            }
            GameRegistry.addShapelessRecipe(new ResourceLocation("String"), new ResourceLocation("recipes"), result, (Ingredient[]) f.toArray());
            //   GameRegistry.addRecipe(new ShapelessOreRecipe(result, f.toArray()));
            //FMLLog.log("RouterReborn", Level.INFO,
             //       "Added Shapeless Recipe for: " + result.getDisplayName());
        }

        return true;
    }

    public static String registerBookRecipe(String[] recipe, String recipesTxt) {
        RECIPE_TYPE rtype = getRecipeType(recipe);

        if (rtype == RECIPE_TYPE.DISABLED)
            return recipesTxt;

        int letter = 0;
        // shaped, item, null, item, null, null, null, item, item, item
        List<ItemStack> rec = new ArrayList<ItemStack>();
        List<String> rows = new ArrayList<String>();
        List<Object> dec = new ArrayList<Object>();

        ItemStack result = getItemStack(recipe[1].trim());
        if (result == null) {
          //  FMLLog.log(
          //          "RouterReborn",
          //          Level.ERROR,
          //          "Error in recipe, Result item: '"
          //                  + recipe[1]
          //                  + "' is unknown or error in format, Format expected is: <disable|shaped|shapeless>, <result>@meta#count, <input>@mets#count, ... ");
          //  FMLLog.log("RouterReborn", Level.ERROR,
          //          "Or Item has been disabled.");

            return recipesTxt;

        }


        int limit = 3;

        if (recipe.length - 2 == 4)
            limit = 2;

        if (rtype == RECIPE_TYPE.ERROR)
            return recipesTxt;

        String cLine = "";
        for (int x = 2; x < recipe.length && x < 11; x++) {
            String cItem = recipe[x].trim();


            if (cItem.equalsIgnoreCase("null") || cItem.length() == 0) {
                cLine += " ";

                rec.add(null);
            } else {
                ItemStack r = null;
                if (OreDictionary.doesOreNameExist(cItem)) {
                    List<ItemStack> lst = OreDictionary.getOres(cItem);
                    for (ItemStack stack : lst) {
                        if (stack != null && stack.getItem() != null) {
                            r = new ItemStack(stack.getItem());

                            break;
                        }
                    }

                } else
                    r = getItemStack(cItem);

                if (r == null) {

                   // FMLLog.log("RouterReborn", Level.ERROR,
                   //         "Error in recipe for: '" + result.getDisplayName()
                   //                 + "' Item: '" + cItem + "' is unknown");
                    return recipesTxt;
                }

                cLine += letters[letter];

                rec.add(r);

                if (rtype == RECIPE_TYPE.SHAPED) {
                    dec.add(letters[letter].charAt(0));
                    dec.add(r);
                } else
                    dec.add(r);

                letter++;
            }
            if (cLine.length() == limit) {
                rows.add(cLine);
                cLine = "";
            }

        }

        ItemStack[] itemRecipe = new ItemStack[rec.size() + 1];
        itemRecipe[0] = result;
        for (int i = 0; i < rec.size(); i++) {

            itemRecipe[i + 1] = rec.get(i);
        }

        String recipeUUID = UUID.randomUUID().toString();
        BookClientRegistry.registerManualRecipe(recipeUUID, itemRecipe);

        recipesTxt += "<page type=\"crafting\"><indexkey>recipes</indexkey><text>" + result.getDisplayName() + "</text><recipe><name>" + recipeUUID + "</name><size>" + (limit == 2 ? "two" : "three") + "</size></recipe></page>";

       // FMLLog.log("RouterReborn", Level.INFO, "Added book recipe for '" + itemRecipe[0].getUnlocalizedName() + "'");

        return recipesTxt;
    }

    public static RECIPE_TYPE getRecipeType(String[] recipe) {
        if (recipe[0].trim() == null)
            return RECIPE_TYPE.ERROR;

        if (recipe[0].trim().equalsIgnoreCase("shaped"))
            return RECIPE_TYPE.SHAPED;

        if (recipe[0].trim().equalsIgnoreCase("shapeless"))
            return RECIPE_TYPE.SHAPELESS;
        if (recipe[0].trim().equalsIgnoreCase("disable"))
            return RECIPE_TYPE.DISABLED;

        return RECIPE_TYPE.ERROR;
    }


    public enum RECIPE_TYPE {
        SHAPED, SHAPELESS, ERROR, DISABLED
    }

}

package com.tomevoll.routerreborn.lib.tile;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface ICammo {

    void setCammo(ItemStack stack, EntityPlayer player);

    ItemStack getCammo();


}

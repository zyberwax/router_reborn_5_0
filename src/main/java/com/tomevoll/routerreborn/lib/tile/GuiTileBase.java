package com.tomevoll.routerreborn.lib.tile;

import com.tomevoll.routerreborn.lib.events.ClientEvents;
import com.tomevoll.routerreborn.lib.gui.modules.IGuiTile;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

import java.util.List;

public abstract class GuiTileBase extends TileEntity implements ITickable, IGuiTile {

    protected boolean showRange;
    protected int N = 10;
    protected int S = 10;
    protected int E = 10;
    protected int W = 10;
    protected int offsetY = 0;
    boolean isShowing = false;

    @Override
    public void onChunkUnload() {

        if (getWorld().isRemote) {
            ClientEvents.toRender.remove(this);
        }

        super.onChunkUnload();
    }

    @Override
    public void onLoad() {
        if (getWorld().isRemote) {
            ClientEvents.toRender.remove(this);
        }
        super.onLoad();
    }

    public boolean getShowRange() {
        return showRange;
    }

    public void setShowRange(boolean val) {
        // TODO Auto-generated method stub
        showRange = val;

        if (getWorld() != null && getWorld().isRemote) {
            if (showRange) {
                if (!ClientEvents.toRender.contains(this)) {
                    TileEntity r = null;
                    for (TileEntity t : ClientEvents.toRender) {
                        if (t.getPos().equals(pos)) {
                            r = t;
                            break;
                        }
                    }
                    if (r != null)
                        ClientEvents.toRender.remove(r);


                    ClientEvents.toRender.add(this);
                }
            } else {

                ClientEvents.toRender.remove(this);
                TileEntity r = null;
                for (TileEntity t : ClientEvents.toRender) {
                    if (t.getPos().equals(pos)) {
                        r = t;
                        break;
                    }
                }
                if (r != null)
                    ClientEvents.toRender.remove(r);
            }
        }
        this.markDirty();
        if (!getWorld().isRemote) UpdateClients();
    }

    @Override
    public void update() {


    }

    void UpdateClients() {
        if (!getWorld().isRemote) {


            int x = pos.getX();
            int y = pos.getY();
            int z = pos.getZ();

            int range = 120;
            List<EntityPlayerMP> list = getWorld().getEntitiesWithinAABB(EntityPlayerMP.class, new AxisAlignedBB(x - range, y - range, z - range,
                    x + range, y + range, z + range));
            NBTTagCompound syncData = new NBTTagCompound();
            writeToNBT(syncData);

            for (EntityPlayerMP player : list) {
                player.connection.sendPacket(new SPacketUpdateTileEntity(pos, 0, syncData));

            }
        }
    }

    @Override
    public AxisAlignedBB getRenderBoundingBox() {

        if (showRange)
            return new AxisAlignedBB(pos.add(-W, offsetY, -N),
                    pos.add(E, offsetY + 1, S));


        return super.getRenderBoundingBox();

    }

    public NBTTagCompound writeSyncToNBT(NBTTagCompound tagCompound) {

        NBTTagList itemList = new NBTTagList();

        tagCompound.setInteger("rangeN", N);
        tagCompound.setInteger("rangeS", S);
        tagCompound.setInteger("rangeW", W);
        tagCompound.setInteger("rangeE", E);
        tagCompound.setInteger("offsetY", offsetY);
        tagCompound.setBoolean("showrange", showRange);

        return tagCompound;
    }

    public void readSyncFromNBT(NBTTagCompound tagCompound) {


        if (tagCompound.hasKey("rangeN")) {
            this.N = tagCompound.getInteger("rangeN");
            this.S = tagCompound.getInteger("rangeS");
            this.W = tagCompound.getInteger("rangeW");
            this.E = tagCompound.getInteger("rangeE");
            this.offsetY = tagCompound.getInteger("offsetY");
            setShowRange(tagCompound.getBoolean("showrange"));
        } else {
            N = 10;
            S = 10;
            W = 10;
            E = 10;
        }

    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tagCompound) {
        // TODO Auto-generated method stub

        NBTTagList itemList = new NBTTagList();

        tagCompound.setInteger("rangeN", N);
        tagCompound.setInteger("rangeS", S);
        tagCompound.setInteger("rangeW", W);
        tagCompound.setInteger("rangeE", E);
        tagCompound.setInteger("offsetY", offsetY);
        tagCompound.setBoolean("showrange", showRange);


        return super.writeToNBT(tagCompound);
    }

    @Override
    public void readFromNBT(NBTTagCompound tagCompound) {

        super.readFromNBT(tagCompound);
        if (tagCompound.hasKey("rangeN")) {
            this.N = tagCompound.getInteger("rangeN");
            this.S = tagCompound.getInteger("rangeS");
            this.W = tagCompound.getInteger("rangeW");
            this.E = tagCompound.getInteger("rangeE");
            this.offsetY = tagCompound.getInteger("offsetY");
            this.showRange = tagCompound.getBoolean("showrange");
        } else {
            N = 10;
            S = 10;
            W = 10;
            E = 10;
        }

    }

    public int getMaxRange() {
        return 10;
    }

    public int getRange(EnumFacing facing) {

        switch (facing) {
            case NORTH:
                return N;
            case SOUTH:
                return S;
            case WEST:
                return W;
            case EAST:
                return E;

            default:
                break;
        }

        return 0;
    }

    public void setRange(EnumFacing facing, int range) {
        switch (facing) {
            case NORTH:
                N = range;
                break;
            case SOUTH:
                S = range;
                break;
            case WEST:
                W = range;
                break;
            case EAST:
                E = range;
                break;
            default:
                break;
        }

        this.markDirty();
        if (!getWorld().isRemote) UpdateClients();
    }

    public int getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(int val) {
        offsetY = val;
        this.markDirty();
        if (!getWorld().isRemote) UpdateClients();
    }

    public int getRedstonePower(BlockPos pos, EnumFacing facing) {
        IBlockState iblockstate = getWorld().getBlockState(pos);
        return iblockstate.getBlock().shouldCheckWeakPower(iblockstate, getWorld(), pos, facing) ? getWorld().getStrongPower(pos) : iblockstate.getWeakPower(getWorld(), pos, facing);
    }

    public boolean isBlockPowered(BlockPos pos) {
        return this.getRedstonePower(pos.down(), EnumFacing.DOWN) > 0 || (this.getRedstonePower(pos.up(), EnumFacing.UP) > 0 || (this.getRedstonePower(pos.north(), EnumFacing.NORTH) > 0 || (this.getRedstonePower(pos.south(), EnumFacing.SOUTH) > 0 || (this.getRedstonePower(pos.west(), EnumFacing.WEST) > 0 || this.getRedstonePower(pos.east(), EnumFacing.EAST) > 0))));
    }

    /**
     * Checks if the specified block or its neighbors are powered by a
     * neighboring block. Used by blocks like TNT and Doors.
     */
    public int isBlockIndirectlyGettingPowered(BlockPos pos) {
        int i = 0;

        for (EnumFacing enumfacing : EnumFacing.values()) {
            int j = this.getRedstonePower(pos.offset(enumfacing), enumfacing);

            if (j >= 15) {
                return 15;
            }

            if (j > i) {
                i = j;
            }
        }

        return i;
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        if (!this.isInvalid())
            readSyncFromNBT(pkt.getNbtCompound());

        HandleRangeUpdate();
    }


    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        //if(this.isInvalid()) return null;

        NBTTagCompound syncData = new NBTTagCompound();
        this.writeSyncToNBT(syncData);
        return new SPacketUpdateTileEntity(this.pos, 0, syncData);

    }

    void HandleRangeUpdate() {
        if (getWorld() != null && getWorld().isRemote) {
            if (showRange != isShowing) {
                if (!showRange) { //make sure to remove from render

                    if (!ClientEvents.toRender.contains(this)) {
                        TileEntity r = null;
                        for (TileEntity t : ClientEvents.toRender) {
                            if (t.getPos().equals(pos)) {
                                r = t;
                                break;
                            }
                        }
                        if (r != null)
                            ClientEvents.toRender.remove(r);

                    } else
                        ClientEvents.toRender.remove(this);
                } else {
                    //We should shhow, but in case we got a bad tile in there, lets double check
                    if (!ClientEvents.toRender.contains(this)) {
                        TileEntity r = null;
                        for (TileEntity t : ClientEvents.toRender) {
                            if (t.getPos().equals(pos)) {
                                r = t;
                                break;
                            }
                        }
                        if (r != null)
                            ClientEvents.toRender.remove(r);

                        ClientEvents.toRender.add(this);
                    }
                }
                isShowing = showRange;
            }
        }
    }


    //client
    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        // TODO Auto-generated method stub
        super.handleUpdateTag(tag);
        if (!this.isInvalid())
            readSyncFromNBT(tag);

        HandleRangeUpdate();


    }


    //server
    @Override
    public NBTTagCompound getUpdateTag() {
        // TODO Auto-generated method stub
        NBTTagCompound syncData = super.getUpdateTag();

        if (syncData == null) syncData = new NBTTagCompound();
        this.writeSyncToNBT(syncData);
        return syncData;
    }

    public boolean canProvideRedstone() {
        // TODO Auto-generated method stub
        return false;
    }

    public int getRedstoneOutput() {
        // TODO Auto-generated method stub
        return 0;
    }


}

package com.tomevoll.routerreborn.lib.proxy;


import com.tomevoll.routerreborn.lib.books.BookData;
import com.tomevoll.routerreborn.lib.books.BookDataStore;
import com.tomevoll.routerreborn.lib.books.client.SmallFontRenderer;
import com.tomevoll.routerreborn.lib.books.client.pages.*;
import com.tomevoll.routerreborn.lib.events.ClientEvents;
import com.tomevoll.routerreborn.lib.renderer.RenderInventoryRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;

import java.util.HashMap;
import java.util.Map;

public class ClientProxy extends CommonProxy {
    public static final String MINECRAFT_ASCII_PATH = "minecraft:textures/font/ascii.png";
    public static SmallFontRenderer smallFontRenderer;
    public static Map<String, Class<? extends BookPage>> pageClasses = new HashMap<String, Class<? extends BookPage>>();

    public static void registerManualPage(String type, Class<? extends BookPage> clazz) {
        pageClasses.put(type, clazz);
    }

    public static Class<? extends BookPage> getPageClass(String type) {
        return pageClasses.get(type);
    }

    public static BookData getBookDataFromStack(ItemStack stack) {
        if (stack == null)
            return null;
        return BookDataStore.getBookFromName(stack.getItem().getUnlocalizedName(stack));
    }

    @Override
    public void RegisterEvents() {
        MinecraftForge.EVENT_BUS.register(new RenderInventoryRegistry());
        MinecraftForge.EVENT_BUS.register(new ClientEvents());

        smallFontRenderer = new SmallFontRenderer(Minecraft.getMinecraft().gameSettings, new ResourceLocation(MINECRAFT_ASCII_PATH), Minecraft.getMinecraft().renderEngine, false);
        readManuals();


    }

    public void readManuals() {
        initManualPages();
    }

    void initManualPages() {
        registerManualPage("crafting", CraftingPage.class);
        registerManualPage("picture", PicturePage.class);
        registerManualPage("text", TextPage.class);
        registerManualPage("intro", TextPage.class);
        registerManualPage("sectionpage", SectionPage.class);
        registerManualPage("intro", TitlePage.class);
        registerManualPage("contents", ContentsTablePage.class);
        registerManualPage("furnace", FurnacePage.class);
        registerManualPage("sidebar", SidebarPage.class);
        registerManualPage("blank", BlankPage.class);
    }
}

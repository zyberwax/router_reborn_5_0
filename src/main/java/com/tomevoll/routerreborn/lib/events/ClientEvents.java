package com.tomevoll.routerreborn.lib.events;

import com.tomevoll.routerreborn.lib.gui.modules.rangeselect.IGuiRangeSelectTile;
import com.tomevoll.routerreborn.lib.renderer.TileFarmAreaRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClientEvents {

    public static Collection<TileEntity> toRender = new ArrayList<TileEntity>();

    @SubscribeEvent
    public void renderWorldLastEvent(RenderWorldLastEvent evt) {
        if (toRender.size() == 0)
            return;

        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayer player = Minecraft.getMinecraft().player;
        TileFarmAreaRenderer tile = new TileFarmAreaRenderer();
        Tessellator tessellator = Tessellator.getInstance();

        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX)
                * (double) evt.getPartialTicks();
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY)
                * (double) evt.getPartialTicks();
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ)
                * (double) evt.getPartialTicks();

        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);

        GlStateManager.disableTexture2D();
        List<TileEntity> toRemove = new ArrayList<TileEntity>();

        GlStateManager.disableCull();
        // GlStateManager.disableDepth();
        for (TileEntity tileEntity : toRender) {

            if (tileEntity.isInvalid()) {
                toRemove.add(tileEntity);
            } else {

                double doubleX = tileEntity.getPos().getX();
                double doubleY = tileEntity.getPos().getY();
                double doubleZ = tileEntity.getPos().getZ();
                IGuiRangeSelectTile t = (IGuiRangeSelectTile) tileEntity;
                AxisAlignedBB movingObjectPositionIn = new AxisAlignedBB(-t.getRange(EnumFacing.WEST),
                        t.getOffsetY(), -t.getRange(EnumFacing.NORTH), t.getRange(EnumFacing.EAST) + 1, t.getOffsetY() + 1, t.getRange(EnumFacing.SOUTH) + 1);
                // if(res != null)
                AxisAlignedBB boundingBox = movingObjectPositionIn
                        .expand(-0.0120000000949949026D,
                                -0.0120000000949949026D,
                                -0.0120000000949949026D).offset(-d0, -d1, -d2)
                        .offset(doubleX, doubleY, doubleZ);

                GlStateManager.depthMask(true);
                // GlStateManager.enableDepth();

                boolean res = TileFarmAreaRenderer.drawOutlinedBoundingBox(t, tessellator,
                        boundingBox.minX, boundingBox.minY, boundingBox.minZ,
                        boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);

                if (res) {

                }
            }
        }
        GlStateManager.color(1F, 1F, 1F, 1F);
        GlStateManager.enableDepth();
        GlStateManager.enableCull();
        GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();

        if (toRemove.size() > 0)
            for (TileEntity tileEntity : toRemove) {
                toRender.remove(tileEntity);
            }

    }

}

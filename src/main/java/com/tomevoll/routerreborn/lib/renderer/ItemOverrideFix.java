package com.tomevoll.routerreborn.lib.renderer;

import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemOverride;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.ArrayList;

public class ItemOverrideFix extends ItemOverrideList {

    public IOverrideFix model = null;

    public ItemOverrideFix(IOverrideFix mod) {
        super(new ArrayList<ItemOverride>());
        this.model = mod;
    }


    @Override
    public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world,
                                       EntityLivingBase entity) {
        return model.handleItemState(originalModel, stack, world, entity);
    }
}

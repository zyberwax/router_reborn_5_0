package com.tomevoll.routerreborn.lib.renderer;

public class ModelRenderer {
    public String res;
    public InventoryItemRenderer renderer;

    public ModelRenderer(String res, InventoryItemRenderer renderer) {
        this.res = res;
        this.renderer = renderer;
    }
}
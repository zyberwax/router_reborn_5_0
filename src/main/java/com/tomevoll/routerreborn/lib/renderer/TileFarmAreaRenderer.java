package com.tomevoll.routerreborn.lib.renderer;

import com.tomevoll.routerreborn.lib.gui.modules.rangeselect.IGuiRangeSelectTile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import org.lwjgl.opengl.GL11;

public class TileFarmAreaRenderer extends TileEntitySpecialRenderer {

    private static void drawOutlinedBottom(IGuiRangeSelectTile tile, BufferBuilder w, Tessellator t, double x0,
                                           double y0, double z0, double x1, double y1, double z1, ItemStack currentCammo) {
/*
* ItemStack tmp = null; TileEntity te =
* tile.getWorld().getTileEntity(tile.getPos().up()); if(te instanceof
* ICammo) { tmp = ((ICammo)te).getCammo(); if(tmp != null) {
* if(currentCammo.getIsItemStackEqual(tmp)) { return; }
*
* } }
*/
        w.begin(7, DefaultVertexFormats.POSITION_TEX);
        w.pos(x0, y0, z0).tex(1, 1).endVertex();
        w.pos(x1, y0, z0).tex(0, 1).endVertex();
        w.pos(x1, y0, z1).tex(0, 0).endVertex();
        w.pos(x0, y0, z1).tex(1, 0).endVertex();
        t.draw();

    }

    private static void drawOutlinedTop(IGuiRangeSelectTile tile, BufferBuilder w, Tessellator t, double x0,
                                        double y0, double z0, double x1, double y1, double z1, ItemStack currentCammo) {
/*
* ItemStack tmp = null; TileEntity te =
* tile.getWorld().getTileEntity(tile.getPos().down()); if(te instanceof
* ICammo) { tmp = ((ICammo)te).getCammo(); if(tmp != null) {
* if(currentCammo.getIsItemStackEqual(tmp)) { return; }
*
* } }
*/
        w.begin(7, DefaultVertexFormats.POSITION_TEX);
        w.pos(x0, y1, z0).tex(1, 1).endVertex();
        w.pos(x0, y1, z1).tex(0, 1).endVertex();
        w.pos(x1, y1, z1).tex(0, 0).endVertex();
        w.pos(x1, y1, z0).tex(1, 0).endVertex();
        t.draw();

    }

    private static void drawOutlinedPosX(IGuiRangeSelectTile tile, BufferBuilder w, Tessellator t, double x0,
                                         double y0, double z0, double x1, double y1, double z1, ItemStack currentCammo) {

/*
* ItemStack tmp = null; TileEntity te =
* tile.getWorld().getTileEntity(tile.getPos().add(-1,0,0)); if(te
* instanceof ICammo) { tmp = ((ICammo)te).getCammo(); if(tmp != null) {
* if(currentCammo.getIsItemStackEqual(tmp)) { return; }
*
* } }
*/
// +X face
// Minecraft.getMinecraft().renderEngine.bindTexture(north);

        w.begin(7, DefaultVertexFormats.POSITION_TEX);
        w.pos(x1, y1, z1).tex(1, 1).endVertex();
        w.pos(x1, y0, z1).tex(1, 0).endVertex();
        w.pos(x1, y0, z0).tex(0, 0).endVertex();
        w.pos(x1, y1, z0).tex(0, 1).endVertex();

        t.draw();

    }

    private static void drawOutlinedNegX(IGuiRangeSelectTile tile, BufferBuilder w, Tessellator t, double x0,
                                         double y0, double z0, double x1, double y1, double z1, ItemStack currentCammo) {
/*
* ItemStack tmp = null; TileEntity te =
* tile.getWorld().getTileEntity(tile.getPos().add(1, 0, 0)); if(te
* instanceof ICammo) { tmp = ((ICammo)te).getCammo(); if(tmp != null) {
* if(currentCammo.getIsItemStackEqual(tmp)) { return; }
*
* } }
*/
// -X face
// Minecraft.getMinecraft().renderEngine.bindTexture(north);

        w.begin(7, DefaultVertexFormats.POSITION_TEX);
        w.pos(x0, y0, z0).tex(1, 0).endVertex();
        w.pos(x0, y0, z1).tex(0, 0).endVertex();
        w.pos(x0, y1, z1).tex(0, 1).endVertex();
        w.pos(x0, y1, z0).tex(1, 1).endVertex();
        t.draw();

    }

    public static boolean drawOutlinedBoundingBox(IGuiRangeSelectTile tile, Tessellator t, double x0, double y0,
                                                  double z0, double x1, double y1, double z1) {
        BufferBuilder w = t.getBuffer();

        if (tile == null) return false;
        double range = new BlockPos(
                Minecraft.getMinecraft().player.posX,
                Minecraft.getMinecraft().player.posY,
                Minecraft.getMinecraft().player.posZ)
                .distanceSq(((TileEntity) tile).getPos());


        float d = 0.201F;
        if (range > 3000) {
            if (range > 4000)
                return false;

            double a = (double) 4000 - range;
            d = (float) ((d / 1000f) * a);
            GlStateManager.color(0, 1.0f, 0, d);
        } else
            GlStateManager.color(0.0F, 1.0F, 0.0F, d);


        GL11.glAlphaFunc(GL11.GL_LESS, 1.0f);
        drawOutlinedBottom(tile, w, t, x0, y0, z0, x1, y1, z1, null);
        drawOutlinedTop(tile, w, t, x0, y0, z0, x1, y1, z1, null);
        drawOutlinedPosX(tile, w, t, x0, y0, z0, x1, y1, z1, null);
        drawOutlinedNegX(tile, w, t, x0, y0, z0, x1, y1, z1, null);

// Tessellator tessellator = Tessellator.instance;

// Top face

// +Z face
// Minecraft.getMinecraft().renderEngine.bindTexture(north);

        w.begin(7, DefaultVertexFormats.POSITION_TEX);
        w.pos(x1, y1, z1).tex(0, 1).endVertex();
        w.pos(x0, y1, z1).tex(1, 1).endVertex();
        w.pos(x0, y0, z1).tex(1, 0).endVertex();
        w.pos(x1, y0, z1).tex(0, 0).endVertex();
        t.draw();

// -Z face
// Minecraft.getMinecraft().renderEngine.bindTexture(north);

        w.begin(7, DefaultVertexFormats.POSITION_TEX);
        w.pos(x1, y1, z0).tex(1, 1).endVertex();
        w.pos(x1, y0, z0).tex(1, 0).endVertex();
        w.pos(x0, y0, z0).tex(0, 0).endVertex();
        w.pos(x0, y1, z0).tex(0, 1).endVertex();
        t.draw();


        GlStateManager.color(0.0F, 0.4F, 0.0F, d * 2);
        GL11.glLineWidth(2.0F);
// GlStateManager.disableDepth();
        drawSelectionBoundingBox(x0, y0, z0, x1, y1, z1, 0, 0.5f, 0);

        GlStateManager.color(1F, 1F, 1F, 1f);
        GL11.glAlphaFunc(GL11.GL_GEQUAL, 0.1f);

        return true;
    }


    public static void drawSelectionBoundingBox(double minX, double minY,
                                                double minZ, double maxX, double maxY, double maxZ, float r, float g, float b) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        vertexbuffer.begin(3, DefaultVertexFormats.POSITION);
        // vertexbuffer.color(r, g, b, 1f);
        vertexbuffer.pos(minX, minY, minZ).endVertex();
        vertexbuffer.pos(maxX, minY, minZ).endVertex();
        vertexbuffer.pos(maxX, minY, maxZ).endVertex();
        vertexbuffer.pos(minX, minY, maxZ).endVertex();
        vertexbuffer.pos(minX, minY, minZ).endVertex();
        tessellator.draw();
        vertexbuffer.begin(3, DefaultVertexFormats.POSITION);
        // vertexbuffer.color(r, g, b, 1f);
        vertexbuffer.pos(minX, maxY, minZ).endVertex();
        vertexbuffer.pos(maxX, maxY, minZ).endVertex();
        vertexbuffer.pos(maxX, maxY, maxZ).endVertex();
        vertexbuffer.pos(minX, maxY, maxZ).endVertex();
        vertexbuffer.pos(minX, maxY, minZ).endVertex();
        tessellator.draw();
        vertexbuffer.begin(1, DefaultVertexFormats.POSITION);
        //vertexbuffer.color(r, g, b, 1f);
        vertexbuffer.pos(minX, minY, minZ).endVertex();
        vertexbuffer.pos(minX, maxY, minZ).endVertex();
        vertexbuffer.pos(maxX, minY, minZ).endVertex();
        vertexbuffer.pos(maxX, maxY, minZ).endVertex();
        vertexbuffer.pos(maxX, minY, maxZ).endVertex();
        vertexbuffer.pos(maxX, maxY, maxZ).endVertex();
        vertexbuffer.pos(minX, minY, maxZ).endVertex();
        vertexbuffer.pos(minX, maxY, maxZ).endVertex();
        tessellator.draw();
    }

    @Override
    public void render(TileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GL11.glPushMatrix();
        GL11.glTranslated(x, y, z);

        doRender((IGuiRangeSelectTile) te);

        GL11.glPopMatrix();
    }


    @Override
    public boolean isGlobalRenderer(TileEntity te) {
        // TODO Auto-generated method stub
        if (te instanceof IGuiRangeSelectTile) {
            if (((IGuiRangeSelectTile) te).getShowRange())
                return true;

        }

        return super.isGlobalRenderer(te);
    }

    public void doRender(IGuiRangeSelectTile tile) {

        if (!tile.getShowRange()) return;


        //GL11.glTranslated(-0.5, 0.0, -0.5);

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(0.0F, 1.0F, 0.0F, 0.4F);
        GL11.glColor4f(0f, 254f, 0f, 0.4f);
        GL11.glLineWidth(1.0F);
        GlStateManager.disableTexture2D();
        GlStateManager.depthMask(false);
        GL11.glColor4f(0f, 254f, 0f, 0.4f);
        //GlStateManager.disableCull();
        drawOutlinedBoundingBox(tile, Tessellator.getInstance(), -tile.getRange(EnumFacing.WEST), tile.getRange(EnumFacing.UP), -tile.getRange(EnumFacing.NORTH), tile.getRange(EnumFacing.EAST), tile.getRange(EnumFacing.UP) + 1, tile.getRange(EnumFacing.SOUTH));
        GL11.glColor4f(0f, 254f, 0f, 1f);
        drawSelectionBoundingBox(-tile.getRange(EnumFacing.WEST), tile.getRange(EnumFacing.UP), -tile.getRange(EnumFacing.NORTH), tile.getRange(EnumFacing.EAST), tile.getRange(EnumFacing.UP) + 1, tile.getRange(EnumFacing.SOUTH), 0f, 1f, 0f);

        GlStateManager.enableCull();
        GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();


        GL11.glPopAttrib();
    }

}





package com.tomevoll.routerreborn.lib.renderer;

import net.minecraft.nbt.NBTTagCompound;

public abstract class InventoryItemRenderer {

    public abstract void RenderInventoryItem(NBTTagCompound itemTag);
}

package com.tomevoll.routerreborn.lib.renderer;

import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;
import java.util.List;

public class RenderInventoryRegistry {

    private static List<ModelRenderer> registered = new ArrayList<ModelRenderer>();

    public static void register(String modelResourcelocation, InventoryItemRenderer renderer) {
        if (!contains(modelResourcelocation))
            registered.add(new ModelRenderer(modelResourcelocation, renderer));
    }

    private static boolean contains(String res) {
        return registered.stream().filter(t -> t.res.equalsIgnoreCase(res)).findAny().isPresent();
    }

    @SubscribeEvent
    public void onModelBakeEvent(ModelBakeEvent event) {

        // Replace the mapping with our ISmartBlockModel, using the existing mapped model as the base for the smart model.

        for (ModelRenderer modelRenderer : registered) {

            Object object = event.getModelRegistry().getObject(new ModelResourceLocation(modelRenderer.res, "inventory"));
            if (object instanceof IBakedModel) {
                IBakedModel existingModel = (IBakedModel) object;
                InventoryRenderer customModel = new InventoryRenderer(existingModel, new ModelResourceLocation(modelRenderer.res, "inventory"), modelRenderer.renderer);
                event.getModelRegistry().putObject(customModel.res, customModel);
            }
        }


    }

}

package com.tomevoll.routerreborn.lib.renderer;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class InventoryRenderer implements IBakedModel, IOverrideFix {
    private static List<BakedQuad> dummyList = Collections.emptyList();
    public Stack<ItemStack> items = new Stack<ItemStack>();
    public ModelResourceLocation res;
    public InventoryItemRenderer renderer;
    private ItemStack item;
    private IBakedModel baseModel;
    private int numberOfChessPieces;

    public InventoryRenderer(IBakedModel model, ModelResourceLocation res, InventoryItemRenderer renderer) {
        super();
        this.baseModel = model;
        this.res = res;
        this.renderer = renderer;
    }

    @Override
    public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
        if (items.empty()) return dummyList;

        if (side != null) return dummyList;
        if (rand != 0) return dummyList;

        ItemStack it = items.pop();
        handleItemState(it);

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();

        //finish current building state (started just before getQuads is called, and is for each model,
        //canceling is fine)
        tessellator.draw();

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        GlStateManager.pushAttrib();
        GlStateManager.pushMatrix();
        //
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.rotate(180, 0, 0, 1);
        GlStateManager.translate(-1D, -1D, 0D);
        GlStateManager.pushMatrix();
        GlStateManager.disableLighting(); // disable light

        //RENDER HERE
        renderer.RenderInventoryItem(item.getTagCompound());

        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
        GlStateManager.popMatrix();

        GlStateManager.enableRescaleNormal();
        GlStateManager.enableAlpha();
        GlStateManager.alphaFunc(516, 0.1F);
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

        GlStateManager.popAttrib();
        GL11.glPopAttrib();

        //Restore state
        worldrenderer.begin(7, DefaultVertexFormats.ITEM);
        Minecraft.getMinecraft().renderEngine.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
        return dummyList;
    }

    @Override
    public boolean isAmbientOcclusion() {
        return true;
    }

    @Override
    public boolean isGui3d() {
        return true;
    }

    @Override
    public boolean isBuiltInRenderer() {
        return false;
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return baseModel.getParticleTexture();
    }

    @SuppressWarnings("deprecation")
    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return baseModel.getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return new ItemOverrideFix(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world,
                                       EntityLivingBase entity) {
        items.push(stack.copy());
        return this;
    }

    public IBakedModel handleItemState(ItemStack stack) {
        this.item = stack.copy();
        return this;
    }

}

package com.tomevoll.routerreborn.lib.block;

import com.tomevoll.routerreborn.lib.RouterRebornLib;
import com.tomevoll.routerreborn.lib.tile.GuiTileBase;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public abstract class GuiBlock extends BlockContainer {

    public GuiBlock(Material mat) {
        super(mat);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return createGuiTileEntity(worldIn, meta);
    }

    public abstract TileEntity createGuiTileEntity(World worldIn, int meta);

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
                                    EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {

        if (worldIn.isRemote) {
            return true;
        } else {
            net.minecraftforge.fml.common.network.internal.FMLNetworkHandler.openGui(playerIn, RouterRebornLib.instance, 999, worldIn, pos.getX(), pos.getY(), pos.getZ());
            return true;
        }

    }


    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof IInventory) {
            InventoryHelper.dropInventoryItems(worldIn, pos, (IInventory) tileentity);
            worldIn.updateComparatorOutputLevel(pos, this);
        }
        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(this);
    }


    public Item getItem(World worldIn, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.SOLID;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state,
                                                World worldIn, BlockPos pos) {
        return super.getSelectedBoundingBox(state, worldIn, pos);
    }

    @SuppressWarnings({"deprecation"})
    @Deprecated
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        if (blockAccess != null) {
            TileEntity tile = blockAccess.getTileEntity(pos);

            if (tile != null && tile instanceof GuiTileBase) {
                if (((GuiTileBase) tile).canProvideRedstone())
                    return ((GuiTileBase) tile).getRedstoneOutput();
            }
        }
        return 0;
    }

    /**
     * Can this block provide power. Only wire currently seems to have this change based on its state.
     */
    @SuppressWarnings({"deprecation"})
    public boolean canProvidePower(IBlockState state) {

        return true;
    }

    /**
     * Called When an Entity Collided with the Block
     */
    @SuppressWarnings({"deprecation"})
    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return getWeakPower(blockState, blockAccess, pos, side);
    }

    @SideOnly(Side.CLIENT)
    public void RegisterRenderer() {
        net.minecraft.client.Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

}

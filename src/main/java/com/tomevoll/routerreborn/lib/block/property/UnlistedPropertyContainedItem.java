package com.tomevoll.routerreborn.lib.block.property;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.property.IUnlistedProperty;

public class UnlistedPropertyContainedItem implements IUnlistedProperty<ItemStack> {
    @Override
    public String getName() {
        return "UnlistedPropertyContainedItem";
    }

    @Override
    public boolean isValid(ItemStack value) {
        return true;
    }

    @Override
    public Class<ItemStack> getType() {
        return ItemStack.class;
    }

    @Override
    public String valueToString(ItemStack value) {
        return value.toString();
    }
}
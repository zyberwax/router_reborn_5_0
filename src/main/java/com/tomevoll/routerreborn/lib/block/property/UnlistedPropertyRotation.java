package com.tomevoll.routerreborn.lib.block.property;

import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.property.IUnlistedProperty;

public class UnlistedPropertyRotation implements IUnlistedProperty<EnumFacing> {
    @Override
    public String getName() {
        return "UnlistedPropertyRotation";
    }

    @Override
    public boolean isValid(EnumFacing value) {
        return true;
    }

    @Override
    public Class<EnumFacing> getType() {
        return EnumFacing.class;
    }

    @Override
    public String valueToString(EnumFacing value) {
        return value.toString();
    }
}
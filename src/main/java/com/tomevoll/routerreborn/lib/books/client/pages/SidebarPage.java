package com.tomevoll.routerreborn.lib.books.client.pages;

import com.tomevoll.routerreborn.lib.books.client.BookClientRegistry;
import com.tomevoll.routerreborn.lib.util.I18n;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SidebarPage extends BookPage {
    String text;
    String[] iconText;
    ItemStack[] icons;
    String[] link;
    int hover = -1;
    int clickX = -1;
    int clickY = -1;
    int moveX = -1;
    int moveY = -1;

    @Override
    public void readPageFromXML(Element element) {
        NodeList nodes = element.getElementsByTagName("text");
        if (nodes != null)
            text = nodes.item(0).getTextContent();

        nodes = element.getElementsByTagName("item");
        iconText = new String[nodes.getLength()];
        icons = new ItemStack[nodes.getLength()];
        link = new String[nodes.getLength()];
        for (int i = 0; i < nodes.getLength(); i++) {
            NodeList children = nodes.item(i).getChildNodes();
            iconText[i] = children.item(1).getTextContent();
            icons[i] = BookClientRegistry.getManualIcon(children.item(3).getTextContent());
            if (children.item(5) != null) link[i] = children.item(5).getTextContent();
        }
    }

    @Override
    public void renderContentLayer(int localWidth, int localHeight, boolean isTranslatable) {
        if (isTranslatable) {
            text = I18n.translateToLocal(text);
        }


        manual.fonts.drawSplitString(text, localWidth, localHeight, 178, 0);
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        RenderHelper.enableGUIStandardItemLighting();
        manual.renderitem.zLevel = 100;
        hover = -1;
        int offset = text.length() / 4 + 10;
        for (int i = 0; i < icons.length; i++) {
            if (isTranslatable) {
                iconText[i] = I18n.translateToLocal(iconText[i]);
            }

            int color = 0;
            if (link[i] != null)
                if (moveY > (localHeight) + (18 * i) + offset) {
                    if (moveY < (localHeight) + (18 * i) + (offset + 18))
                        if (moveX < localWidth + 140)
                            if (moveX > localWidth) {
                                color = 0x0000FF;
                                hover = i;
                            }

                }

            manual.renderitem.renderItemIntoGUI(icons[i], localWidth + 8, localHeight + 18 * i + offset);
            int yOffset = 39;
            if (iconText[i].length() > 40)
                yOffset = 34;

            manual.fonts.drawSplitString(iconText[i], localWidth + 30, localHeight + 18 * i + offset, 140, color);
        }
        manual.renderitem.zLevel = 0;
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
    }

    @Override
    public void mouseClickMove(int i, int j, Boolean isTranslatable,
                               int mouseX, int mouseY, int mouseButton, long timeSinceLastClick) {

        moveX = mouseX;
        moveY = mouseY;
        // TODO Auto-generated method stub
        super.mouseClickMove(i, j, isTranslatable, mouseX, mouseY, mouseButton,
                timeSinceLastClick);
    }

    @Override
    public void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                           int mouseY, int mouseButton) {
        clickX = mouseX;
        clickY = mouseY;
        if (hover != -1) {
            if (link[hover] != null) {
                Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
                manual.gotoPage(link[hover]);
            }
        }
        // TODO Auto-generated method stub

    }
}

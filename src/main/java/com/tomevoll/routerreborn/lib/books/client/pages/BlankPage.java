package com.tomevoll.routerreborn.lib.books.client.pages;

import org.w3c.dom.Element;

public class BlankPage extends BookPage {

    @Override
    public void readPageFromXML(Element element) {
    }

    @Override
    public void renderContentLayer(int localwidth, int localheight, boolean isTranslatable) {
    }

    @Override
    public void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                           int mouseY, int mouseButton) {

    }

}

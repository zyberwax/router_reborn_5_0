package com.tomevoll.routerreborn.lib.books.client.pages;


import com.tomevoll.routerreborn.lib.util.I18n;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SectionPage extends BookPage {
    String title;
    String body;

    @Override
    public void readPageFromXML(Element element) {
        NodeList nodes = element.getElementsByTagName("title");
        if (nodes != null)
            title = nodes.item(0).getTextContent();

        nodes = element.getElementsByTagName("text");
        if (nodes != null)
            body = nodes.item(0).getTextContent();
    }

    @Override
    public void renderContentLayer(int localWidth, int localHeight, boolean isTranslatable) {
        if (isTranslatable) {
            title = I18n.translateToLocal(title);
            body = I18n.translateToLocal(body);
        }
        manual.fonts.drawSplitString("\u00a7n" + title, localWidth + 70, localHeight + 4, 178, 0);
        manual.fonts.drawSplitString(body, localWidth, localHeight + 16, 190, 0);
    }

    @Override
    public void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                           int mouseY, int mouseButton) {
        // TODO Auto-generated method stub

    }
}

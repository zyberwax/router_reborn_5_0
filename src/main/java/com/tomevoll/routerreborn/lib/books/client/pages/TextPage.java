package com.tomevoll.routerreborn.lib.books.client.pages;


import com.tomevoll.routerreborn.lib.books.client.BookClientRegistry;
import com.tomevoll.routerreborn.lib.util.I18n;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.item.ItemStack;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TextPage extends BookPage {
    String text;
    ItemStack icon;

    @Override
    public void readPageFromXML(Element element) {
        NodeList nodes = element.getElementsByTagName("text");
        if (nodes != null)
            text = nodes.item(0).getTextContent();
        NodeList nodes2 = element.getElementsByTagName("icon");
        if (nodes2 != null)
            icon = BookClientRegistry.getManualIcon(nodes2.item(0).getTextContent());

    }

    @Override
    public void renderContentLayer(int localWidth, int localHeight, boolean IsTranslatable) {
        if (IsTranslatable)
            text = I18n.translateToLocal(text);

        if (icon != null) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
                    240, 240);

            manual.renderitem.renderItemAndEffectIntoGUI(icon, (localWidth), (localHeight));

        }

        manual.fonts.drawSplitString(text, localWidth, localHeight + (icon == null ? 0 : 16), 178, 0);
    }

    @Override
    public void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                           int mouseY, int mouseButton) {
        // TODO Auto-generated method stub

    }
}

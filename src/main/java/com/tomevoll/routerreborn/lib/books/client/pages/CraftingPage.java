package com.tomevoll.routerreborn.lib.books.client.pages;

import com.tomevoll.routerreborn.lib.books.client.BookClientRegistry;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class CraftingPage extends BookPage {
    private static final ResourceLocation background = new ResourceLocation("routerrebornlib", "textures/gui/bookcrafting.png");
    String text;
    String size;
    ItemStack[] icons;

    @Override
    public void readPageFromXML(Element element) {
        NodeList nodes = element.getElementsByTagName("text");
        if (nodes != null)
            text = nodes.item(0).getTextContent();

        nodes = element.getElementsByTagName("name");
        if (nodes != null)
            icons = BookClientRegistry.getRecipeIcons(nodes.item(0).getTextContent());

        nodes = element.getElementsByTagName("size");
        if (nodes != null)
            size = nodes.item(0).getTextContent();
    }
    @SuppressWarnings({"deprecation"})
    @Override
    public void renderContentLayer(int localWidth, int localHeight, boolean isTranslatable) {
        if (isTranslatable)
            text = net.minecraft.util.text.translation.I18n.translateToLocal(text);
        if (size.equals("two"))
            drawCraftingPage(text, icons, 2, localWidth, localHeight + 12);
        if (size.equals("three"))
            drawCraftingPage(text, icons, 3, localWidth + (side != 1 ? 6 : 0), localHeight + 12);
    }

    public void drawCraftingPage(String info, ItemStack[] icons, int recipeSize, int localWidth, int localHeight) {
        if (info != null)
            manual.fonts.drawString("\u00a7n" + info, localWidth + 50, localHeight + 4, 0);

        GL11.glScalef(2f, 2f, 2f);
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        //RenderHelper.enableStandardItemLighting();
        RenderHelper.enableGUIStandardItemLighting();
        manual.renderitem.zLevel = 100;

        if (recipeSize == 2 && icons != null) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
                    240, 240);

            manual.renderitem.renderItemAndEffectIntoGUI(icons[0], (localWidth + 126) / 2, (localHeight + 68) / 2);
            if (icons[0].getCount() > 1)
                manual.renderitem.renderItemOverlayIntoGUI(manual.getMC().fontRenderer, icons[0], (localWidth + 126) / 2, (localHeight + 68) / 2, String.valueOf(icons[0].getCount()));
            for (int i = 0; i < icons.length - 1; i++) {
                OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
                        240, 240);

                if (icons[i + 1] != null)
                    manual.renderitem.renderItemAndEffectIntoGUI(icons[i + 1], (localWidth + 14 + 36 * (i % 2)) / 2, (localHeight + 36 * (i / 2) + 52) / 2);
            }
        }

        if (recipeSize == 3 && icons != null) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
                    240, 240);

            manual.renderitem.renderItemAndEffectIntoGUI(icons[0], (localWidth + 138) / 2, (localHeight + 70) / 2);
            if (icons[0].getCount() > 1)
                manual.renderitem.renderItemOverlayIntoGUI(manual.getMC().fontRenderer, icons[0], (localWidth + 126) / 2, (localHeight + 68) / 2, String.valueOf(icons[0].getCount()));
            for (int i = 0; i < icons.length - 1; i++) {
                OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
                        240, 240);

                if (icons[i + 1] != null)
                    manual.renderitem.renderItemAndEffectIntoGUI(icons[i + 1], (localWidth - 2 + 36 * (i % 3)) / 2, (localHeight + 36 * (i / 3) + 34) / 2);
            }
        }

        manual.renderitem.zLevel = 0;
        GL11.glScalef(0.5F, 0.5F, 0.5F);
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
    }

    @Override
    public void renderBackgroundLayer(int localwidth, int localheight) {
        if (size.equals("two"))
            drawBackground(2, localwidth, localheight + 12);

        if (size.equals("three"))
            drawBackground(3, localwidth + (side != 1 ? 6 : 0), localheight + 12);
    }

    public void drawBackground(int size, int localWidth, int localHeight) {
        manual.getMC().getTextureManager().bindTexture(background);
        if (size == 2)
            manual.drawTexturedModalRect(localWidth + 8, localHeight + 46, 0, 116, 154, 78);
        if (size == 3)
            manual.drawTexturedModalRect(localWidth - 8, localHeight + 28, 0, 0, 183, 114);
    }

    @Override
    public void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                           int mouseY, int mouseButton) {
        // TODO Auto-generated method stub

    }

}

package com.tomevoll.routerreborn.lib.books.client.pages;


import com.tomevoll.routerreborn.lib.util.I18n;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TitlePage extends BookPage {
    String text;

    @Override
    public void readPageFromXML(Element element) {
        NodeList nodes = element.getElementsByTagName("text");
        if (nodes != null)
            text = nodes.item(0).getTextContent();
    }

    @Override
    public void renderContentLayer(int localWidth, int localHeight, boolean isTranslatable) {
        if (isTranslatable)
            text = I18n.translateToLocal(text);
        manual.fonts.drawSplitString(text, localWidth, localHeight, 178, 0);
    }

    @Override
    public void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                           int mouseY, int mouseButton) {
        // TODO Auto-generated method stub

    }
}

package com.tomevoll.routerreborn.lib.books;

//import static mantle.lib.CoreRepo.logger;

import com.tomevoll.routerreborn.lib.RouterRebornLib;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.nio.charset.StandardCharsets;

public class ManualReader {
    @SuppressWarnings({"deprecation"})
    public static InputStream inputStreamFromString(String value) {
        return new StringBufferInputStream(value);

    }


    public static String getString(String location) {
        try {

            InputStream stream = RouterRebornLib.class.getResourceAsStream("/" + location);
            String result = IOUtils.toString(stream, StandardCharsets.UTF_8);
            return result;
        } catch (Exception e) {
            return "";
        }

    }

    public static Document readManual(String location) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        try {
            //logger.info("Loading Manual XML from: " + location);
            InputStream stream = RouterRebornLib.class.getResourceAsStream("/" + location);

            return readManual(stream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Document readManual(InputStream is) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (Exception e) {
            //  logger.error("Failed to Load Manual XML from: " + filenameOrLocation);
            e.printStackTrace();
            return null;
        }
    }
}

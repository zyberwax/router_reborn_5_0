package com.tomevoll.routerreborn.lib.books.client.gui;


import com.tomevoll.routerreborn.lib.books.BookData;
import com.tomevoll.routerreborn.lib.books.client.SmallFontRenderer;
import com.tomevoll.routerreborn.lib.books.client.pages.BookPage;
import com.tomevoll.routerreborn.lib.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;


@SideOnly(Side.CLIENT)
public class GuiManual extends GuiScreen {


    private static ResourceLocation bookRight;// = new ResourceLocation("mantle", "textures/gui/bookright.png");
    private static ResourceLocation bookLeft;// = new ResourceLocation("mantle", "textures/gui/bookleft.png");
    public RenderItem renderitem = Minecraft.getMinecraft().getRenderItem();
    public SmallFontRenderer fonts = ClientProxy.smallFontRenderer;
    ItemStack itemstackBook;
    Document manual;
    int bookImageWidth = 206;
    int bookImageHeight = 200;
    int bookTotalPages = 1;
    int currentPage;
    int maxPages;
    BookData bData;
    BookPage pageLeft;
    BookPage pageRight;
    private TurnPageButton buttonNextPage;
    private TurnPageButton buttonPreviousPage;


    public GuiManual(ItemStack stack, BookData data) {
        if (data == null) return;

        this.mc = Minecraft.getMinecraft();
        this.itemstackBook = stack;
        currentPage = 0; //Stack page
        manual = data.getDoc();
        if (data.font != null)
            this.fonts = data.font;
        bookLeft = data.leftImage;
        bookRight = data.rightImage;
        this.bData = data;

        //renderitem.renderInFrame = true;
    }

    @Override
    public void handleMouseInput() throws IOException {
        int i = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int j = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
        int k = Mouse.getEventButton();

        int localWidth = (this.width / 2);
        byte localHeight = 8;

        if (pageLeft != null)
            pageLeft.mouseClickMove(localWidth + 16, localHeight + 12, bData.isTranslatable, i, j, k, 0);
        if (pageRight != null)
            pageRight.mouseClickMove(localWidth + 220, localHeight + 12, bData.isTranslatable, i, j, k, 0);


        super.handleMouseInput();
    }

    public void setCurrentPage(int page) {
        this.currentPage = page;
        updateText();
    }
    
    /*@Override
    public void setWorldAndResolution (Minecraft minecraft, int w, int h)
    {
        this.guiParticles = new GuiParticle(minecraft);
        this.mc = minecraft;
        this.width = w;
        this.height = h;
        this.buttonList.clear();
        this.initGui();
    }*/

    @Override
    @SuppressWarnings("unchecked")
    public void initGui() {
        maxPages = manual.getElementsByTagName("page").getLength();
        updateText();
        int xPos = (this.width) / 2; //TODO Width?
        //TODO buttonList
        this.buttonList.add(this.buttonNextPage = new TurnPageButton(1, xPos + bookImageWidth - 50, 180, true, bData));
        this.buttonList.add(this.buttonPreviousPage = new TurnPageButton(2, xPos - bookImageWidth + 24, 180, false, bData));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.enabled) {
            if (button.id == 1)
                currentPage += 2;
            if (button.id == 2)
                currentPage -= 2;

            updateText();
        }
    }


    public void gotoPage(String key) {
        int page = -1;

        NodeList nList = manual.getElementsByTagName("page");


        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                String cKey = "";
                Element element = (Element) node;
                NodeList nodes = element.getElementsByTagName("indexkey");
                if (nodes.item(0) != null)
                    cKey = nodes.item(0).getTextContent();

                if (cKey.equalsIgnoreCase(key)) {
                    page = i;
                    break;
                }

            }
        }

        if (page > -1) {
            currentPage = page;
            updateText();
        }


    }


    void updateText() {
        if (maxPages % 2 == 1) {
            if (currentPage > maxPages)
                currentPage = maxPages;
        } else {
            if (currentPage >= maxPages)
                currentPage = maxPages - 2;
        }
        if (currentPage % 2 == 1)
            currentPage--;
        if (currentPage < 0)
            currentPage = 0;

        NodeList nList = manual.getElementsByTagName("page");

        Node node = nList.item(currentPage);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            Class<? extends BookPage> clazz = ClientProxy.getPageClass(element.getAttribute("type"));
            if (clazz != null) {
                try {
                    pageLeft = clazz.newInstance();
                    pageLeft.init(this, 0);
                    pageLeft.readPageFromXML(element);
                } catch (Exception e) {
                }
            } else {
                pageLeft = null;
            }
        }

        node = nList.item(currentPage + 1);
        if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            Class<? extends BookPage> clazz = ClientProxy.getPageClass(element.getAttribute("type"));
            if (clazz != null) {
                try {
                    pageRight = clazz.newInstance();
                    pageRight.init(this, 1);
                    pageRight.readPageFromXML(element);
                } catch (Exception e) {
                }
            } else {
                pageLeft = null;
            }
        } else {
            pageRight = null;
        }
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(bookRight);
        int localWidth = (this.width / 2);
        byte localHeight = 8;
        this.drawTexturedModalRect(localWidth, localHeight, 0, 0, this.bookImageWidth, this.bookImageHeight);

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(bookLeft);
        localWidth = localWidth - this.bookImageWidth;
        this.drawTexturedModalRect(localWidth, localHeight, 256 - this.bookImageWidth, 0, this.bookImageWidth, this.bookImageHeight);

        super.drawScreen(par1, par2, par3); //16, 12, 220, 12

        if (pageLeft != null)
            pageLeft.renderBackgroundLayer(localWidth + 16, localHeight + 12);
        if (pageRight != null)
            pageRight.renderBackgroundLayer(localWidth + 220, localHeight + 12);
        if (pageLeft != null)
            pageLeft.renderContentLayer(localWidth + 16, localHeight + 12, bData.isTranslatable);
        if (pageRight != null)
            pageRight.renderContentLayer(localWidth + 220, localHeight + 12, bData.isTranslatable);

    }

    @Override
    protected void mouseClickMove(int mouseX, int mouseY,
                                  int mouseButton, long timeSinceLastClick) {

        super.mouseClickMove(mouseX, mouseY, mouseButton, timeSinceLastClick);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton)
            throws IOException {
        int localWidth = (this.width / 2);
        byte localHeight = 8;

        if (pageLeft != null)
            pageLeft.mouseClick(localWidth + 16, localHeight + 12, bData.isTranslatable, mouseX, mouseY, mouseButton);
        if (pageRight != null)
            pageRight.mouseClick(localWidth + 220, localHeight + 12, bData.isTranslatable, mouseX, mouseY, mouseButton);

        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    public Minecraft getMC() {
        return mc;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}

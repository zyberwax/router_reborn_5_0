package com.tomevoll.routerreborn.lib.books.client;

import com.google.common.collect.Maps;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class BookClientRegistry {
    public static Map<String, ItemStack> manualIcons = Maps.newHashMap();
    public static Map<String, ItemStack[]> recipeIcons = Maps.newHashMap();
    public static HashMap<String, BookImage> imageCache = Maps.newHashMap();

    public static ItemStack defaultStack = new ItemStack(Items.IRON_INGOT);

    public static BookImage getBookImageFromCache(String s) {
        return imageCache.get(s);
    }

    public static void registerManualIcon(String name, ItemStack stack) {
        manualIcons.put(name, stack);
    }

    public static ItemStack getManualIcon(String textContent) {
        ItemStack stack = manualIcons.get(textContent);
        if (stack != null)
            return stack;
        return defaultStack;
    }

    public static void registerManualRecipe(String name, ItemStack[] recipeItem) {

        //  ItemStack[] recipe = new ItemStack[5];
        //  recipe[0] = recipeItem[0];
        // System.arraycopy(recipeItem, 1, recipe, 1, recipe.length-1);
        recipeIcons.put(name, recipeItem);
    }


    public static void registerManualSmallRecipe(String name, ItemStack output, ItemStack... stacks) {
        ItemStack[] recipe = new ItemStack[5];
        recipe[0] = output;
        System.arraycopy(stacks, 0, recipe, 1, 4);
        recipeIcons.put(name, recipe);
    }

    public static void registerManualLargeRecipe(String name, ItemStack output, ItemStack... stacks) {
        ItemStack[] recipe = new ItemStack[10];
        recipe[0] = output;
        System.arraycopy(stacks, 0, recipe, 1, 9);
        recipeIcons.put(name, recipe);
    }

    public static void registerManualFurnaceRecipe(String name, ItemStack output, ItemStack input) {
        ItemStack[] recipe = new ItemStack[2];
        recipe[0] = output;
        recipe[1] = input;
        recipeIcons.put(name, recipe);
    }

    public static ItemStack[] getRecipeIcons(String recipeName) {
        return recipeIcons.get(recipeName);
    }
}
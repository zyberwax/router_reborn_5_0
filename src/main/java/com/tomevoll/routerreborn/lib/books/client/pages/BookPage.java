package com.tomevoll.routerreborn.lib.books.client.pages;

import com.tomevoll.routerreborn.lib.books.client.gui.GuiManual;
import org.w3c.dom.Element;

public abstract class BookPage {
    protected GuiManual manual;
    protected int side;

    public void init(GuiManual manual, int side) {
        this.manual = manual;
        this.side = side;
    }

    public abstract void readPageFromXML(Element element);

    public void renderBackgroundLayer(int localwidth, int localheight) {
    }

    public abstract void renderContentLayer(int localwidth, int localheight, boolean isTranslatable);

    public abstract void mouseClick(int i, int j, Boolean isTranslatable, int mouseX,
                                    int mouseY, int mouseButton);

    public void mouseClickMove(int i, int j, Boolean isTranslatable,
                               int mouseX, int mouseY, int mouseButton, long timeSinceLastClick) {
        // TODO Auto-generated method stub

    }
}

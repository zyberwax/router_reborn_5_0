package com.tomevoll.routerreborn.lib.books;

import com.tomevoll.routerreborn.lib.RouterRebornLib;
import com.tomevoll.routerreborn.lib.books.client.SmallFontRenderer;
import net.minecraft.util.ResourceLocation;
import org.w3c.dom.Document;

public class BookData {
    public String unlocalizedName = new String();
    public String toolTip = new String();
    public String modID = new String();
    public ResourceLocation leftImage = new ResourceLocation(RouterRebornLib.MODID, "textures/gui/bookleft.png");
    public ResourceLocation rightImage = new ResourceLocation(RouterRebornLib.MODID, "textures/gui/bookright.png");
    public ResourceLocation itemImage = new ResourceLocation(RouterRebornLib.MODID, "textures/items/mantlebook_blue.png");
    public Document doc; // = ManualReader.readManual("assets/routerrebornlib/manuals/test.xml");
    //font can be left null if so, the default from mantle will be used
    public SmallFontRenderer font;
    public Boolean isTranslatable = false;
    public boolean isFromZip = false;

    public Document getDoc() {
        return this.doc;
    }

    public String getFullUnlocalizedName() {
        return this.modID + ":" + this.unlocalizedName;
    }

}

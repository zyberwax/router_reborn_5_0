package com.tomevoll.routerreborn.lib.sound;


import net.minecraft.client.audio.PositionedSound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Generic ISound class with lots of constructor functionality. Required because - of course - Mojang has no generic that lets you specify *any* arguments for
 * this.
 *
 * @author skyboy
 */
@SideOnly(Side.CLIENT)
public class SoundBase extends PositionedSound {

    public SoundBase(SoundEvent soundIn, SoundCategory categoryIn) {
        super(soundIn, categoryIn);
        // TODO Auto-generated constructor stub
    }

    public SoundBase(SoundEvent soundIn, SoundCategory categoryIn, BlockPos pos) {
        super(soundIn, categoryIn);
        // TODO Auto-generated constructor stub
        this.xPosF = pos.getX();
        this.yPosF = pos.getY();
        this.zPosF = pos.getZ();
        this.repeat = true;
        this.repeatDelay = 0;
    }

    public SoundBase(ResourceLocation sound, float volume, float pitch,
                     boolean repeat, int repeatDelay, double x, double y, double z,
                     AttenuationType attenuation) {
        // TODO Auto-generated constructor stub
        super(sound, SoundCategory.BLOCKS);
        this.xPosF = (float) x;
        this.yPosF = (float) y;
        this.zPosF = (float) z;
        this.repeat = repeat;

        this.repeatDelay = repeatDelay;
        this.pitch = pitch;
        this.attenuationType = attenuation;
    }


}
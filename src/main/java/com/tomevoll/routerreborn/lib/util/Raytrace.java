/**
 *
 */
package com.tomevoll.routerreborn.lib.util;

/**
 * @author zyberwax
 */

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.Collection;

public final class Raytrace {

    private Raytrace() {

    }

    public static Vec3d getStart(EntityPlayer player) {

        return new Vec3d(player.posX, player.posY + player.getEyeHeight(), player.posZ);
    }

    public static Vec3d getEnd(EntityPlayer player) {

        double reachDistance = (player.capabilities.isCreativeMode ? 5.0D : 4.5D);
        Vec3d lookVec = player.getLookVec();
        Vec3d start = getStart(player);
        return start.addVector(lookVec.x * reachDistance, lookVec.y * reachDistance, lookVec.z * reachDistance);
    }

    public static RayTraceResult collisionRayTrace(World world, BlockPos pos, EntityPlayer player, Collection<AxisAlignedBB> boxes) {

        return collisionRayTrace(world, pos, getStart(player), getEnd(player), boxes);
    }

    public static RayTraceResult collisionRayTrace(World world, BlockPos pos, EntityPlayer player, AxisAlignedBB aabb, int subHit, Object hitInfo) {

        return collisionRayTrace(pos, getStart(player), getEnd(player), aabb, subHit, hitInfo);
    }

    public static RayTraceResult collisionRayTrace(World world, BlockPos pos, Vec3d start, Vec3d end, Collection<AxisAlignedBB> boxes) {

        double minDistance = Double.POSITIVE_INFINITY;
        RayTraceResult hit = null;

        int i = -1;
        for (AxisAlignedBB aabb : boxes) {
            RayTraceResult result = aabb == null ? null : collisionRayTrace(pos, start, end, aabb, i, null);
            if (result != null) {
                double d = result.squareDistanceTo(start);
                if (d < minDistance) {
                    minDistance = d;
                    hit = result;
                }
            }
            i++;
        }

        return hit;
    }

    public static RayTraceResult collisionRayTrace(BlockPos pos, Vec3d start, Vec3d end, AxisAlignedBB bounds, int subHit, Object hitInfo) {

        start = start.addVector((-pos.getX()), (-pos.getY()), (-pos.getZ()));
        end = end.addVector((-pos.getX()), (-pos.getY()), (-pos.getZ()));

        Vec3d vec3 = start.getIntermediateWithXValue(end, bounds.minX);
        Vec3d vec31 = start.getIntermediateWithXValue(end, bounds.maxX);
        Vec3d vec32 = start.getIntermediateWithYValue(end, bounds.minY);
        Vec3d vec33 = start.getIntermediateWithYValue(end, bounds.maxY);
        Vec3d vec34 = start.getIntermediateWithZValue(end, bounds.minZ);
        Vec3d vec35 = start.getIntermediateWithZValue(end, bounds.maxZ);

        if (!isVecInsideYZBounds(bounds, vec3))
            vec3 = null;
        if (!isVecInsideYZBounds(bounds, vec31))
            vec31 = null;
        if (!isVecInsideXZBounds(bounds, vec32))
            vec32 = null;
        if (!isVecInsideXZBounds(bounds, vec33))
            vec33 = null;
        if (!isVecInsideXYBounds(bounds, vec34))
            vec34 = null;
        if (!isVecInsideXYBounds(bounds, vec35))
            vec35 = null;

        Vec3d vec36 = null;

        if (vec3 != null && (vec36 == null || start.squareDistanceTo(vec3) < start.squareDistanceTo(vec36)))
            vec36 = vec3;
        if (vec31 != null && (vec36 == null || start.squareDistanceTo(vec31) < start.squareDistanceTo(vec36)))
            vec36 = vec31;
        if (vec32 != null && (vec36 == null || start.squareDistanceTo(vec32) < start.squareDistanceTo(vec36)))
            vec36 = vec32;
        if (vec33 != null && (vec36 == null || start.squareDistanceTo(vec33) < start.squareDistanceTo(vec36)))
            vec36 = vec33;
        if (vec34 != null && (vec36 == null || start.squareDistanceTo(vec34) < start.squareDistanceTo(vec36)))
            vec36 = vec34;
        if (vec35 != null && (vec36 == null || start.squareDistanceTo(vec35) < start.squareDistanceTo(vec36)))
            vec36 = vec35;

        if (vec36 == null) {
            return null;
        } else {
            EnumFacing enumfacing = null;

            if (vec36 == vec3)
                enumfacing = EnumFacing.WEST;
            if (vec36 == vec31)
                enumfacing = EnumFacing.EAST;
            if (vec36 == vec32)
                enumfacing = EnumFacing.DOWN;
            if (vec36 == vec33)
                enumfacing = EnumFacing.UP;
            if (vec36 == vec34)
                enumfacing = EnumFacing.NORTH;
            if (vec36 == vec35)
                enumfacing = EnumFacing.SOUTH;
            //new net.minecraft.util.math.RayTraceResult(hitVecIn, sideHitIn, blockPosIn)
            net.minecraft.util.math.RayTraceResult mop = new net.minecraft.util.math.RayTraceResult(vec36.addVector(pos.getX(), pos.getY(), pos.getZ()), enumfacing, pos);
            mop.subHit = subHit;
            mop.hitInfo = hitInfo;
            return new RayTraceResult(mop, bounds);
        }
    }

    private static boolean isVecInsideYZBounds(AxisAlignedBB bounds, Vec3d vec) {

        return vec != null && (vec.y >= bounds.minY && vec.y <= bounds.maxY && vec.z >= bounds.minZ && vec.z <= bounds.maxZ);
    }

    private static boolean isVecInsideXZBounds(AxisAlignedBB bounds, Vec3d vec) {

        return vec != null && (vec.x >= bounds.minX && vec.x <= bounds.maxX && vec.z >= bounds.minZ && vec.z <= bounds.maxZ);
    }

    private static boolean isVecInsideXYBounds(AxisAlignedBB bounds, Vec3d vec) {

        return vec != null && (vec.x >= bounds.minX && vec.x <= bounds.maxX && vec.y >= bounds.minY && vec.y <= bounds.maxY);
    }

    private static class RayTraceResultBase<T extends RayTraceResult> {


        public final AxisAlignedBB bounds;
        public final net.minecraft.util.math.RayTraceResult hit;

        public RayTraceResultBase(net.minecraft.util.math.RayTraceResult mop, AxisAlignedBB bounds) {

            this.hit = mop;
            this.bounds = bounds;
        }

        public boolean valid() {

            return hit != null && bounds != null;
        }

        /*	public void setBounds(World world, BlockPos pos)
            {

                world.getBlockState(pos).getBlock().setBlockBounds((float) bounds.minX, (float) bounds.minY, (float) bounds.minZ, (float) bounds.maxX, (float) bounds.maxY, (float) bounds.maxZ);
            }
            */
        public double squareDistanceTo(Vec3d vec) {

            return hit.hitVec.squareDistanceTo(vec);
        }
    }

    public static class RayTraceResult extends RayTraceResultBase<RayTraceResult> {

        public RayTraceResult(net.minecraft.util.math.RayTraceResult mop, AxisAlignedBB bounds) {

            super(mop, bounds);
        }
    }
}
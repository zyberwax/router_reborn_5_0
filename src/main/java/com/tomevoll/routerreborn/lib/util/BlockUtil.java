package com.tomevoll.routerreborn.lib.util;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class BlockUtil {
/*	public static void notifyBlockUpdate(World world, BlockPos pos)
    {
		IBlockState state = world.getBlockState(pos);
		world.notifyBlockUpdate(pos, state, state, 3);
	}
	*/

    public static void markBlockForUpdate(World world, BlockPos pos) {
        if (world.isRemote) return;
        ((WorldServer) world).getPlayerChunkMap().markBlockForUpdate(pos);
    }

}

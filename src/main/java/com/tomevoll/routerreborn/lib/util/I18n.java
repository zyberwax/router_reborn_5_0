package com.tomevoll.routerreborn.lib.util;

import net.minecraft.util.text.translation.LanguageMap;

public class I18n {

    @SuppressWarnings({"deprecation"})
    public static String translateToLocal(String key) {
        return net.minecraft.util.text.translation.I18n.translateToLocal(key);
    }


}